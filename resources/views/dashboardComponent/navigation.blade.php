<header>
    <div class="webNav hidden-xs hidden-sm">
        <div class="navMini">
            <div class="container-fluid padding-none">
                <div class="row margin-none">
                    <div class="col-xs-6 col-sm-6 col-md-4 text-left">
                        <div class="toll-free"> Call 1660-01-22-4444 for help</div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-8 text-right">
                        <!-- Admin link start -->

                       
                        <!-- Login info start -->

                        <!--message-->
                        <span href="#" class="message-blocks"  style="text-decoration: none">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <a href="#" class="messages" title="messages">message</a>
                            <ul class="messages-lists">
                               <span class="notify-header">
                                    <h3>3 New messages</h3>
                                </span>
                                <li><a href="#">
                                      <span class="mini-card">
                                          <span class="message">This is methis is methis is me</span><br>
                                          <span class="time">18min</span>
                                      </span>
                                    </a>
                                </li>
                                <li><a href="#">
                                        <span class="mini-card">
                                          <span class="message">This is methis is methis is me</span><br>
                                          <span class="time">18min</span>
                                      </span>
                                    </a>
                                </li>
                                <li><a href="#">
                                        <span class="mini-card">
                                          <span class="message">This is methis is methis is me</span><br>
                                          <span class="time">18min</span>
                                      </span>
                                    </a>
                                </li>
                            </ul>
                        </span>
                        <!--notification-->
                        <span href="#" class="notify-blocks"  style="text-decoration: none">
                            <i class="fa fa-bell-o" aria-hidden="true"></i>
                            <a href="#" class="notifys" title="notifys">notification</a>
                            <ul class="notify-lists">
                                <span class="notify-header">
                                    <h3>3 New notification</h3>
                                </span>
                                <li><a href="#">
                                      <span class="mini-card">
                                          <span class="message">This is methis is methis is me</span><br>
                                          <span class="time">18min</span>
                                      </span>
                                    </a>
                                </li>
                                <li><a href="#">
                                        <span class="mini-card">
                                          <span class="message">This is methis is methis is me</span><br>
                                          <span class="time">18min</span>
                                      </span>
                                    </a>
                                </li>
                                <li><a href="#">
                                        <span class="mini-card">
                                          <span class="message">This is methis is methis is me</span><br>
                                          <span class="time">18min</span>
                                      </span>
                                    </a>
                                </li>
                            </ul>
                        </span>

                        <!--settings-->
                        <span href="#" class="setting-blocks"  style="text-decoration: none">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <a href="#" class="settings" title="Settings">Hi!&nbsp<span>{{ ucfirst(Auth::user()->username) }}</span></a>
                            <ul class="setting-lists">
                                <li><a href="#">settings</a></li>
                                <li><a href="{{ route('admin/changepasword') }}">Change Password</a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out pull-right"></i>Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </li>
                            </ul>
                        </span>

                        <!-- Login info over -->
                    </div>
                </div>
            </div>
        </div>

        <nav class="navbar mart-nav margin-none header-bar affix-top" role="navigation" data-spy="affix" data-offset-top="60" data-offset-bottom="200">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('dashboard') }}"><img src="{{ asset('frontend/design/assets/images/logos.png') }}" alt="" style="height:100%">
                    <img src="{{ asset('frontend/design/assets/images/swasthya-vector-logo.png') }}" alt="" style="height:100%;margin-left:10px"></a>
            </div>

            <!-- search start-->

            <ul class="nav navbar-nav nav-search">
                <form name="productSearchFrm" id="productSearchFrm" >
                    <input type="hidden" value="A" id="productType" name="searchCategory">
                    <input type="hidden" name="quantity" class="qty" value="" ondrop="return false" id="quantity">
                    <input type="hidden" name="productId" value="" id="searchProductId">
                    <input type="hidden" value="" id="searchType">
                    <div class="form-group margin-none">
                        <div class="input-group productsearch">
								<span class="input-group-btn">
								<button class="btn btnMenu" type="button" id="MartSearchCategories" data-toggle="dropdown">All Product<span class="caret"></span></button>
									<ul class="dropdown-menu categories-drpdwn" aria-labelledby="MartSearchCategories">
										<li><a href="#" id="general" onclick="selectSearchType(this,'G');" class=""><i class="fa fa-check-circle"></i> General Store</a></li>
										<li><a href="#" id="pharmacy" onclick="selectSearchType(this,'P');"><i class="fa fa-check-circle"></i> Pharmacy</a></li>
										<li><a href="#" id="allProducts" onclick="selectSearchType(this,'A');" class="active"><i class="fa fa-check-circle"></i> All Products</a></li>
									</ul>
								</span>
                            <span class="twitter-typeahead srchdropdwn container-fluid padding-none">
                                <input class="tt-hint hide" type="text" autocomplete="off" spellcheck="off" disabled="" style="position: absolute; top: 0px; left: 0px; border-color: transparent; box-shadow: none; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255);">
                                <input id="enterval" name="enterval" data-provide="typeahead" type="text"
                                       class="form-control navtxtSearch typeahead tw-typeahead tt-query"
                                       placeholder="Search for... general or pharma products"
                                       onfocus="$('.alert-error,.alert-info').not('#noItemsInCartMsg').parent().fadeOut();"
                                       autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: rgb(255, 255, 255); width: 100%;"
                                       data-placeholder="Search for... general or pharma products">
                                <span style="position: absolute; left: -9999px; visibility: hidden; white-space: nowrap;
                                 font-family: open_sansregular, Helvetica Neue, Helvetica, Arial, sans-serif; font-size: 14px;
                                 font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal;
                                 font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px;
                                 text-rendering: auto; text-transform: none;">
                                </span>
                                <span class="tt-dropdown-menu dropdown-menu"
                                      style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none; width: 100%;">
                                </span>
                            </span>
                            <i class="fa fa-refresh fa-spin fa-fw loadproducts hide"></i>
                            <span class="input-group-btn">
									<button class="btn btnDark" type="button" style="background-color: #000000;color:#ffffff">SEARCH</button>
                            </span>
                        </div>

                    </div>
                </form>
            </ul>
        </nav>

     

        <!--WebNav start -->

        <!-- webNav ends -->

    </div>

    <!--mobile navigation start -->
    <nav class="navbar mart-nav navbar-fixed-top margin-none visible-xs visible-sm" role="navigation">
        <div >
            <a class="navbar-brand" href="{{route('dashboard') }}">
<!--                <img src="design/assets/images/logos.png" alt="">-->
                <img src="{{ asset('frontend/design/assets/images/logos.png') }}" alt="" style="height:100%">
                    <img src="{{ asset('frontend/design/assets/images/swasthya-vector-logo.png') }}" alt="" style="height:100%;margin-left:10px">
            </a>
        </div>
        
    </nav>
    <!--mobile navigation over-->

    


  

</header>

