@extends('frontend.master')

@section('extra-header')

    <link type="text/css" href="{{ asset('frontend/css/_asideBar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/added-items/added-items.css') }}">
    <link href="{{ asset('frontend/css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">


@endsection

@section('main-content')

    @include('frontend/added-items-page/_order-summaries')

@endsection

@section('extra-footer')
//    checkbox set to true
<script >
    $("#checky").prop( "checked", true );
</script>
@endsection