@extends('frontend.master')

@section('extra-header')

    
<link rel="stylesheet" href="{{ asset('frontend/css/userprofile/userProfile.css') }}">
<style>
  #accInfo h4,#personalInfo h4, #billingInfo h4, #deliveryInfo h4{
  color: #121212;
  }
</style>

@endsection

@section('main-content')

  <!-- /.banner-->
<div class="wrapper" id="site-wrapper" >
	<div class="content-wrapper" style="margin-left:0px;">

    <!--  /.userProfile     -->

        @include('frontend.userprofile._userProfile')

   </div>
<!--/.wrapper-->

@endsection


@section('extra-footer')
<script src="{{ asset('frontend/source/js/userprofile.js') }}"></script>
@endsection
