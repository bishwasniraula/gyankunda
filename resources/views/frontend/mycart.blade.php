@extends('frontend.master')

@section('extra-header')

    <link type="text/css" href="{{ asset('frontend/css/_asideBar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/added-items/added-items.css') }}">
    <link href="{{ asset('frontend/css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('main-content')

	<div class="Cart__container___5UudH custom-top-margin">
		<div id="cart_section" >
			<div class="CartStepper__margin___rPxU6"><div>
				<div class="Step__step___3K1ts Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 10 -->STEP <!-- /react-text --><!-- react-text: 11 -->1<!-- /react-text --><!-- react-text: 12 -->: <!-- /react-text --><!-- react-text: 13 -->My Cart<!-- /react-text --></div></div><div class="Step__step___3K1ts Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 16 -->STEP <!-- /react-text --><!-- react-text: 17 -->2<!-- /react-text --><!-- react-text: 18 -->: <!-- /react-text --><!-- react-text: 19 -->Order Summary<!-- /react-text --></div></div><div class="Step__step___3K1ts" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 22 -->STEP <!-- /react-text --><!-- react-text: 23 -->3<!-- /react-text --><!-- react-text: 24 -->: <!-- /react-text --><!-- react-text: 25 -->Payment<!-- /react-text --></div></div></div></div>

			<div class="row CartPayment__cart-items___KuzeB">
				<div class="custom-snackbar" style="position: fixed; left: 0px; display: flex; right: 0px; bottom: 0px; z-index: 2900; visibility: hidden; transform: translate3d(0px, 48px, 0px); transition: transform 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, visibility 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;">
                <div width="3" style="background-color: rgb(255, 243, 193); padding: 0px 24px; height: 48px; line-height: 48px; border-radius: 2px; max-width: 568px; min-width: 288px; flex-grow: 0; margin: auto; color: rgba(33, 33, 33, 0.87);">
                    <div style="font-size: 14px; color: rgb(255, 255, 255); opacity: 0; transition: opacity 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;">
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="CartPayment__mobile-cashback___azElx">
                <div class="ApplyCashback__outer-container___3ICor">
                    <div class="row ApplyCashback__inner-container___1mlzT">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-7">
            	<div class="row">
                    <div class="col-xs-12 CartPayment__select-payment-title___3Qzwq">My Items</div>
                </div>
                <div class="CartLineItems__item-list___2p3SB">
                	 @if(Session::has('cart'))

                	 	@foreach($products as $product)

                	 		<div class="CartLineItem__container___1w1IL"><div class="row"><div class="col-xs-7"><div class="CartLineItem__name___3JS5V">{{ $product['item']['title'] }}</div><div class="CartLineItem__pack-size-label___xORIi">Quantity: {{ $product['qty'] }}</div></div><div class="col-xs-5 text-right"><div class="CartLineItem__price___1j-lm"><!-- react-text: 124 -->Rs.<!-- /react-text --><!-- react-text: 125 -->{{ $product['price'] }}<!-- /react-text --></div><div class="CartLineItem__actual-price___1Juw7"><!-- react-text: 127 -->Rs.<!-- /react-text --><!-- react-text: 128 -->{{ $product['item']['selling_price'] }}<!-- /react-text --></div><!-- react-text: 129 --><!-- /react-text --></div></div>

                	 		<div class="row"><a class="button-text">
                	 			<div class="col-xs-6">

                	 		<!-- <img src="/images/delete_icon.svg" alt="delete icon" class="CartLineItem__delete-medicine___1oaAq"><span class="CartLineItem__txt-delete-medicine___2PC18"> -->
							

                	 		</span></div></a></div></div>
           @endforeach
           	@else
      			<h3 style="color: #d81010;text-align: center;margin-top: 26px;">You have no items in your shopping cart</h3>
  			@endif
                  
                  </div>
              </div>

              <div class="col-xs-12 col-md-5 ">
              	<div class="CartSummary__outerContainer___1mopE">
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="col-xs-6">MRP Total</span>
                            <span class="col-xs-6 text-right">Rs. {{ $totalPrice }}</span>
                        </div>
                        <div class="col-xs-12">
                            <span class="col-xs-6">Discount</span>
                            <span class="col-xs-6 text-right">Rs. 0.00</span>
                        </div>
                        <div class="col-xs-12">
                            <span class="col-xs-6">Shipping Charges</span>
                            <span class="col-xs-6 text-right">As per delivery address</span>
                        </div>

                        <div class="col-xs-12" style="margin-top: 10px;">
                            <h5 class="col-xs-6"><strong>To be paid</strong></h5>
                            <span class="col-xs-6 text-right"><strong>Rs. {{ $totalPrice }}</strong></span>
                        </div>

                        <div class="col-xs-12 CartSummary__cart-summary-footer___24W5I">
                    <span class="col-xs-6 CartSummary__cart-summary-footer-text___211r2">Total Saving

                    </span>
                    <span class="col-xs-6 CartSummary__cart-summary-footer-text___211r2 text-right" style="color: #1aab2a;font-weight: 600;">Rs. 0.00

                    </span>
                        </div>
                     </div>

                    </div>
                    <a class="t-cart-address-continue CartDeliveryDetails__desktop-button___29znG button-text" href="/checkout" style="background-color: rgb(11, 197, 216); color: rgb(255, 255, 255); margin-top: 15px;
               height: 40px;width:100%; padding: 0px 16px; text-decoration: none; display: inline-flex; font-weight: 500; font-size: 16px;
               border-radius: 4px; z-index: 100; cursor: pointer; align-items: center; justify-content: center; border: 1px solid rgb(11, 197, 216);">
                        <span style="display: inline-block;">PROCEED TO PAYMENT</span>
                    </a>
                </div>


              </div>
	
                  <div class="row">
        <div class="col-xs-12 col-md-5 col-md-push-7">
            <div class="Cart__disclaimer___N20CK">
                <div class="Cart__coupon-superscript-star___1fuIB">*</div>
                Swasthya Nepal will be credited 7 days after your complete order is delivered in case of Products and in case of
                Lab Services Swasthya Nepal will be credited within 24 hours from the time of generation of test report.
                Swasthya Nepal will not be credited in case a return request is initiated for the order.<br>
                <!--                <div class="Cart__coupon-superscript-star___1fuIB">**</div>Coupon Discount value may change if the total order value changes.-->
            </div>
        </div>
        <div class="col-xs-12 col-md-7 col-md-pull-5">
            <div class="Cart__disclaimer-small___1ptS3">
                Swasthya Nepal is a technology platform to facilitate transaction of business.
                The products and services are offered for sale by the sellers.
                The user authorizes the delivery personel to be his agent for delivery of the goods. For details read
                <a href="#">terms and conditions.</a>
            </div>
        </div>
    </div>  
                        
                </div>
            </div>
		</div>
 
             

            
</div>
</div>
@endsection

