@extends('frontend.master')

@section('extra-header')

    <link type="text/css" href="{{ asset('frontend/css/_asideBar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/added-items/added-items.css') }}">
    <link href="{{ asset('frontend/css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('main-content')
    <div class="container" id="cartEmpty">
        <div id="cart_section">
             @if(Session::has('cart'))
            <div class="CartStepper__margin___rPxU6">
            <div>
                <div class="Step__step___3K1ts Step__step-active___H-r3g Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 10 -->STEP <!-- /react-text --><!-- react-text: 11 -->1<!-- /react-text --><!-- react-text: 12 -->: <!-- /react-text --><!-- react-text: 13 -->My Cart<!-- /react-text --></div></div><div class="Step__step___3K1ts" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 16 -->STEP <!-- /react-text --><!-- react-text: 17 -->2<!-- /react-text --><!-- react-text: 18 -->: <!-- /react-text --><!-- react-text: 19 -->Order Summary<!-- /react-text --></div></div><div class="Step__step___3K1ts" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 22 -->STEP <!-- /react-text --><!-- react-text: 23 -->3<!-- /react-text --><!-- react-text: 24 -->: <!-- /react-text --><!-- react-text: 25 -->Payment<!-- /react-text --></div></div></div></div>
                        <input type="hidden" id="is_zero">
                <div class="col-xs-12 col-md-7">
                    <div class="row">
                        <div id="count" class="col-xs-12 CartPayment__select-payment-title___3Qzwq">Items NOT Requiring Prescription (<span class="Iitems-count" id="countC">{{ $cart_qty }}</span>)</div>
                    </div>
                    <div class="CartLineItems__item-list___2p3SB">
                        @foreach($products as $product)
                            <div id="item-block-{{ $product['item']['id'] }}" class="CartLineItem__container___1w1IL">
                                <div class="row">
                                    <div class="col-xs-12">
                                    <div class="row">
                                        
                                <div class="col-xs-7"><div class="CartLineItem__name___3JS5V">{{ $product['item']['title'] }}</div></div>
                                <div class="col-xs-5 text-right"><div id="total-price-{{ $product['item']['id'] }}" class="CartLineItem__price___1j-lm">Rs. {{ $product['price'] }}</div></div>
                                <div class="col-xs-7"><div class="CartLineItem__pack-size-label___xORIi" id="sp-{{ $product['item']['id'] }}">Per piece- Rs. {{ $product['item']['price'] }}</div></div>
                                <div class="col-xs-5 text-right"><div class="CartLineItem__actual-price___1Juw7" id="mp-{{ $product['item']['id'] }}">Rs. {{ $product['item']['selling_price']  }}</div></div>
                                <div class="col-xs-7"><div class="CartLineItem__pack-size-label___xORIi">
                            <a class="removeItem" item-id="remove-{{ $product['item']['id'] }}"><i class="fa fa-trash" style="padding:10px;font-size:20px;color:red;"></i></a>
                                    </div></div>
    <div class="col-xs-5 text-right"><div class="CartLineItem__pack-size-label___xORIi">
        <span style="font-size: 20px;">
    <a class="minus-qty" item-id="reduce-qty-{{ $product['item']['id'] }}" style="padding: 0px 2px;cursor: pointer;"><i class="fa fa-minus" aria-hidden="true"></i></a><strong style="padding:10px;color: #f16c20;" id="t-itm-{{ $product['item']['id'] }}">{{ $product['qty'] }}</strong> 
    <a class="plus-qty" item-id="{{ $product['item']['id'] }}" style="padding: 0px 2px;cursor: pointer;"><i class="fa fa-plus" aria-hidden="true"></i></a></span>
                                    
</div></div>
                                    </div>
                                </div>
                                </div>


                            </div>
                        @endforeach
                   </div>     
                </div>
                <div class="col-xs-12 col-md-5 ">
                      <div class="CartSummary__outerContainer___1mopE">
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="col-xs-6">MRP Total</span>
                            <span class="col-xs-6 text-right" id="mrp-total">Rs. {{ $totalMarketprice }}</span>
                        </div>
                        <div class="col-xs-12">
                            <span class="col-xs-6">Price Discount</span>
                            <span class="col-xs-6 text-right" id="discountPrice">Rs. {{ $discountedPrice }}</span>
                        </div>
                        <div class="col-xs-12">
                            <span class="col-xs-6">Shipping Charges</span>
                            <span class="col-xs-6 text-right">As per delivery address</span>
                        </div>
                        <div class="col-xs-12" style="margin-top: 10px;">
                            <h5 class="col-xs-6"><strong>To be paid</strong></h5>
                            <span class="col-xs-6 text-right">
                                <strong id="to-be-paid">Rs. {{ $totalPrice }}</strong>
                            </span>
                        </div>
                        <div class="col-xs-12 CartSummary__cart-summary-footer___24W5I">
                    <span class="col-xs-6 CartSummary__cart-summary-footer-text___211r2">Total Saving

                    </span>
                    <span class="col-xs-6 CartSummary__cart-summary-footer-text___211r2 text-right" id="saving" style="color: #1aab2a;font-weight: 600;">Rs. {{ $discountedPrice }}

                    </span>
                        </div>
                    </div>
                </div>
                <a class="t-cart-address-continue CartDeliveryDetails__desktop-button___29znG button-text" href="{{ URL::to('/order-summary') }}" style="background-color: rgb(11, 197, 216); color: rgb(255, 255, 255); margin-top: 15px;
               height: 40px;width:100%; padding: 0px 16px; text-decoration: none; display: inline-flex; font-weight: 500; font-size: 16px;
               border-radius: 4px; z-index: 100; cursor: pointer; align-items: center; justify-content: center; border: 1px solid rgb(11, 197, 216);">
                        <span style="display: inline-block;">CHECKOUT</span>
                    </a>
            </div>
            <div class="col-xs-12" style="margin-bottom: 50px;">

                
                    
        <div class="col-xs-12 col-md-5 col-md-push-7">
            <div class="Cart__disclaimer___N20CK">
                <div class="Cart__coupon-superscript-star___1fuIB">*</div>
                Swasthya Nepal will be credited 7 days after your complete order is delivered in case of Products and in case of
                Lab Services Swasthya Nepal will be credited within 24 hours from the time of generation of test report.
                Swasthya Nepal will not be credited in case a return request is initiated for the order.<br>
                <!--                <div class="Cart__coupon-superscript-star___1fuIB">**</div>Coupon Discount value may change if the total order value changes.-->
            </div>
        </div>
        <div class="col-xs-12 col-md-7 col-md-pull-5">
            <div class="Cart__disclaimer-small___1ptS3">
                Swasthya Nepal is a technology platform to facilitate transaction of business.
                The products and services are offered for sale by the sellers.
                The user authorizes the delivery personel to be his agent for delivery of the goods. For details read
                <a href="#">terms and conditions.</a>
            </div>
        </div>
        </div>

         @else
      <div class="row"><div class="col-md-12 text-center" style="height: 500px;"><div style="top: 40%;position: relative"><i class="fa fa-cart-arrow-down" style="font-size: 50px;color: #f16c20;"></i><h3 style="color: #a8a8a8;">Oops! It's look like your cart is empty.</h3><a href="{{ URL::to('/') }}" class="btn btn-primary" style="background: #04668C !important;border-color: #04668C;color: #ffffff;border-radius:0px;">Continue Shopping</a></div></div></div>
 @endif
    </div>
</div>

@endsection

@section('script')
    <script type="text/javascript">
        
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
             });
            $('document').ready(function(){

                $('.plus-qty').click(function(){

                var id = $(this).attr('item-id');
                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                    url:"{{ URL::to('/add') }}"+"/"+id,
                    type:"GET",
                    data:{_method:'PATCH'},
                    datatType : 'json',
                    success:function(data){
                        //console.log(data);
                        $('#t-itm-'+id).text(data.countItem['qty']);
                        $('#total-price-'+id).text('Rs. '+(data.countItem['price']).toFixed(2));
                        $('#mrp-total').text('Rs. '+(data.totalMarketprice).toFixed(2));
                        $('#discountPrice').text('Rs. '+(data.discountedPrice).toFixed(2));
                        $('#saving').text('Rs. '+(data.discountedPrice).toFixed(2));
                        $('#to-be-paid').text('Rs. '+(data.totalAmount).toFixed(2));
                    }
                });
            });


                $('.minus-qty').click(function(){

                var mix_id = $(this).attr('item-id');
                var id = mix_id.split('reduce-qty-').join('');
                var i = $('#t-itm-'+id).text();

                $.get("/cart-count",function(count){

                $('#is_zero').val((count.totalCartCount)); 
                });
                
                if(i==1){
                    $('#item-block-'+id).remove();
                    $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                    url:"{{ URL::to('/reduce') }}"+"/"+id,
                    type:"GET",
                    data:{_method:'PATCH'},
                    datatType : 'json',

                    success:function(data){
                        //console.log(data);
                        if($('#is_zero').val()==0){
                            $('.Iitems-count').text(0);
                            $('div#cart_section').empty().append('<div class="row"><div class="col-md-12 text-center" style="height: 500px;"><div style="top: 40%;position: relative"><i class="fa fa-cart-arrow-down" style="font-size: 50px;color: #f16c20 !important;"></i><h3 tyle="color: #a8a8a8;">Oops ! It\'s look like Your cart is empty.</h3><a href="{{ URL::to('/') }}" class="btn btn-primary" style="background: #04668C !important;border-color: #04668C;color: #ffffff;border-radius:0px">Continue Shopping</a></div></div></div>');
                        }else{
                            $('.Iitems-count').text(data.uCartCount);
                            $('#mrp-total').text('Rs. '+(data.totalMarketprice).toFixed(2));
                            $('#discountPrice').text('Rs. '+(data.discountedPrice).toFixed(2));
                            $('#saving').text('Rs. '+(data.discountedPrice).toFixed(2));
                            $('#to-be-paid').text('Rs. '+(data.totalAmount).toFixed(2));
                        }            
                        }
                    });

                }else{
                    $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                    url:"{{ URL::to('/reduce') }}"+"/"+id,
                    type:"GET",
                    data:{_method:'PATCH'},
                    datatType : 'json',
                    success:function(data){
                        $('.Iitems-count').text(data.cartCount);
                        $('#t-itm-'+id).text(data.countItem['qty']);

                    $('#total-price-'+id).text('Rs. '+(data.countItem['price']).toFixed(2));
                        $('#mrp-total').text('Rs. '+(data.totalMarketprice).toFixed(2));
                        $('#discountPrice').text('Rs. '+(data.discountedPrice).toFixed(2));
                        $('#saving').text('Rs. '+(data.discountedPrice).toFixed(2));
                        $('#to-be-paid').text('Rs. '+(data.totalAmount).toFixed(2));            
                        }
                    });
                }
                });
               


               $('.removeItem').click(function(){

                var iId = $(this).attr('item-id');
                var itemId = iId.split('remove-').join('');

                var c = $('div#count').find('span').text();
                 

                $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                    url:"{{ URL::to('/remove') }}"+"/"+itemId,
                    type:"GET",
                    data:{_method:'PATCH'},
                    success:function(data){
                        if(data=="success"){
                        $('#item-block-'+itemId).remove();
                             $.get("{{ URL::to('/afterremove-cart') }}",function(data){
                                
                                if(c==1){
                                    $('div#cart_section').empty().append('<div class="row"><div class="col-md-12 text-center" style="height: 500px;"><div style="top: 40%;position: relative"><i class="fa fa-cart-arrow-down" style="font-size: 50px;color: #f16c20;"></i><h3 tyle="color: #a8a8a8;">Oops ! It\'s look like Your cart is empty.</h3><a href="{{ URL::to('/') }}" class="btn btn-primary" style="background: #04668C !important;border-color: #04668C;color: #ffffff;border-radius:0px">Continue Shopping</a></div></div></div>');
                                    $('.Iitems-count').text(0);
                                     console.log('cart empty');
                                     
                                }else{

                                    $('.Iitems-count').text(data.cartCount);
                                    $('#mrp-total').text('Rs. '+(data.totalMarketprice).toFixed(2));
                                    $('#discountPrice').text('Rs. '+(data.discountedPrice).toFixed(2));
                                    $('#saving').text('Rs. '+(data.discountedPrice).toFixed(2));
                                    $('#to-be-paid').text('Rs. '+(data.totalPrice).toFixed(2));
                                }
                                

                             })
                        }
                    
                    }
                   
               }); 
            });
        });
    </script>
@endsection    