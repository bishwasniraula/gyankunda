@extends('frontend.master')

@section('extra-header')
    
    <link href="{{ asset('frontend/css/hospital/hospital.css') }}" rel="stylesheet">
    
@endsection

@section('main-content')

        @include('frontend/pages/hospital/_hospital')

@endsection

@section('extra-footer')
<script>
    $(document).ready(function () {
    	$('#fadeinButton').click(function() {
        $('#fadein').toggle();

    	});
    });
</script>
@endsection