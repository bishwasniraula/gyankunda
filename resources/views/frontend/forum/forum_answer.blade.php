@extends('frontend.forum.layout.template')

@section('content')
    <ul class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Home</a></li>
        <li><a href="{{ route('forum-answer') }}">Answer</a></li>
    </ul>
    <div class="container content-wrapper">
        <div class="row">
            <!--main forum page left side-->
                @include('frontend/forum/includes/forum-main_leftside')
            <!--main forum page middle-->
                @include('frontend/forum/includes/forum-answer_middle')
            
        </div>

    </div>
@endsection