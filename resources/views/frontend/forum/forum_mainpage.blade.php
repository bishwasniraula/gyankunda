@extends('frontend.forum.layout.template')

@section('content')
    <ul class="breadcrumb">
        <li><a href="{{ URL::to('/') }}">Home</a></li>
    </ul>
    <div class="container content-wrapper">
        <div class="row">
            <!--main forum page left side-->
                @include('frontend/forum/includes/forum-main_leftside')
            <!--main forum page middle-->
                @include('frontend/forum/includes/forum-main_middle')
            <!--main forum page middle-->
                @include('frontend/forum/includes/forum-main_right')
        </div>

    </div>
@endsection