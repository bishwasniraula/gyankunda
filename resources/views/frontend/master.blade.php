<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<!--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('frontend/images/favicon.png') }}">
    <title>Swasthya Nepal</title>

    <link type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">

    <link type="text/css" href="{{ asset('frontend/design/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/design/vendor/font-awesome/css/font-awesome.min.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/design/vendor/font-awesome/css/font-awesome-animation.min.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/design/vendor/OwlCarousel2-2.2.1/dist/assets/owl.carousel.min.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/design/vendor/slick-slider/slick/slick.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/design/vendor/slick-slider/slick/slick-theme.css') }}">

    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <!-- <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css"> -->


    <link type="text/css" href="{{ asset('frontend/css/navigation/navigation.css') }}" rel="stylesheet">

    <link type="text/css" href="{{ asset('frontend/css/_asideBar.css') }}" rel="stylesheet">

    <link type="text/css" href="{{ asset('frontend/css/_slider.css') }}" rel="stylesheet">

    <link type="text/css" href="{{ asset('frontend/css/_offers.css') }}" rel="stylesheet">

    <link type="text/css" href="{{ asset('frontend/css/modal.css') }}" rel="stylesheet">

    <link type="text/css" href="{{ asset('frontend/css/_testimonials.css') }}" rel="stylesheet">

    <link type="text/css" href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/_appDisplay.css') }}" >

    <link type="text/css" href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">

    @section('extra-header')
        @show
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
</head>

<body style="overflow-x: hidden">

<div class="wrapper" id="site-wrapper">

    @include('frontend.layouts._navigation')

    @include('frontend.layouts._asideBar')

       <div class="content-wrapper">

           @yield('main-content')

        @include('frontend.layouts._footers')

       </div>

</div>





@include('frontend.layouts.footer')

@section('extra-footer')
    @show
<script type="text/javascript">

    $( window ).on( "load", function() { 
      var currentURL = location.href;
      if(currentURL==='http://127.0.0.1:8000/'){
        
        $('body').css('background', '#f1f1f1');
        //console.log(currentURL);
      }else{
        //console.log('Milan');
      }
    })
</script>
</body>
</html>