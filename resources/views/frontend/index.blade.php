@extends('frontend.master')

@section('main-content')

       @include('frontend/layouts/_slider')
       <!-- ./main-slider-wrapper -->


       @include('frontend/layouts/_featuredMedicine')
       <!-- ./featured-medicine-wrapper -->

        @include('frontend/layouts/_featuredHospital')
            <!-- ./featured-hospital-wrapper -->

       @include('frontend/layouts/_offers')
       <!-- ./featured-offers-wrapper -->

       @include('frontend/layouts/_testimonials')
       <!-- ./Testimonials-wrapper -->

       @include('frontend/layouts/_healthPatner')
       <!-- ./Health Patner-wrapper -->


       <!--ios and android display banner-->
       @include('frontend/layouts/_footer-text-container')
       @include('frontend/layouts/_app-display')
       <!--footer text container-->

       <!--<footer class="footer">-->


@endsection

@section('extra-footer')
<script>
    $(document).ready(function () {
        $(document).on('click', '.navbar-toggle', function (event) {
            event.preventDefault();
            $('body').toggleClass('open');
        });

    });

</script>
@endsection