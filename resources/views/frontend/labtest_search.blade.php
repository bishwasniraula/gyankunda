@extends('frontend.master')

@section('extra-header')


     <link href="{{ asset('frontend/css/category/category.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/category/exclusive.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/labtest/labTest.css') }}" rel="stylesheet">
    
@endsection

@section('main-content')

        @include('frontend/pages/labtest/_labtest_search')

@endsection

@section('extra-footer')
<script>
    $(document).ready(function () {
    		$('.collapsible-container').on('click',function(){
            $('.style_collapsible-footer').slideToggle(500);

    	});
    });
</script>
@endsection