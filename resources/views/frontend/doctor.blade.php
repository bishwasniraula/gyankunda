@extends('frontend.master')

@section('extra-header')

	 <link href="{{ asset('frontend/css/doctor/doctor-page.css') }}" rel="stylesheet">
     <link href="{{ asset('frontend/css/category/category.css') }}" rel="stylesheet">
   
    
    
@endsection

@section('main-content')

        @include('frontend/pages/doctor/_doctor-page')

@endsection