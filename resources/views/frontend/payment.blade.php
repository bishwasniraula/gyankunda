@extends('frontend.master')

@section('extra-header')

    <link type="text/css" href="{{ asset('frontend/css/_asideBar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/added-items/added-items.css') }}">
    <link href="{{ asset('frontend/css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">


@endsection



@section('main-content')
<div class="row">
  <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-4">
    <h2>Checkout</h2>
    <h4>Your Total: $ {{ $total }}</h4>
    <div id="charge-error" class="alert alert-danger {{ Session::has('cart') ? 'hidden' : '' }}">
      {{ Session::get('error') }}

    </div>
    <form action="{{ route('checkout') }}" method="post" id="checkout-form">
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" required>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" id="address" name="address" required>
          </div>
        </div>

      <div class="col-xs-12">
          <div class="form-group">
            <label for="Card Holder">Active Phone</label>
            <input type="text" class="form-control" name="phone" required>
          </div>
      </div>
            {{ csrf_field() }}
       <div class="col-xs-12">
          <div class="form-group">
           <button class="btn btn-success">Place Order</button>
        </div>
      </div>
      </div>
    </form>
  </div>
</div>

@endsection

@section('extra-footer')

@endsection