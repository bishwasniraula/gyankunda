@extends('frontend.master')

@section('extra-header')
    <link href="{{ asset('frontend/css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/category/category.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/category/exclusive.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/category/allopathy-product-display.css') }}" rel="stylesheet">
    <style>
        body{
            background:#f1f1f1 !important;
        }
    </style>
@endsection

@section('main-content')
  

   @include('frontend/pages/category/exclusive/_exclusive');

@endsection


@section('extra-footer')

<script src="{{ asset('frontend/design/vendor/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('frontend/design/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/source/js/cart.js') }}"></script>
<!--<script src="design/assets/js/cart-counter.js"></script>-->
<script src="{{ asset('frontend/source/js/scripts.js') }}"></script>
<script src="{{ asset('frontend/source/js/maincartsystem.js') }}"></script>

@endsection