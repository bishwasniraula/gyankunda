<div class="Cart__container___5UudH custom-top-margin">

    <!--    ajax section-->


    <div id="cart_section" >

        <div class="CartStepper__margin___rPxU6"><div><div class="Step__step___3K1ts Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 10 -->STEP <!-- /react-text --><!-- react-text: 11 -->1<!-- /react-text --><!-- react-text: 12 -->: <!-- /react-text --><!-- react-text: 13 -->My Cart<!-- /react-text --></div></div><div class="Step__step___3K1ts Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 16 -->STEP <!-- /react-text --><!-- react-text: 17 -->2<!-- /react-text --><!-- react-text: 18 -->: <!-- /react-text --><!-- react-text: 19 -->Order Summary<!-- /react-text --></div></div><div class="Step__step___3K1ts Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 22 -->STEP <!-- /react-text --><!-- react-text: 23 -->3<!-- /react-text --><!-- react-text: 24 -->: <!-- /react-text --><!-- react-text: 25 -->Payment<!-- /react-text --></div></div></div></div>
        <div class="row CartPayment__cart-items___KuzeB">
            <div class="custom-snackbar" style="position: fixed; left: 0px; display: flex; right: 0px; bottom: 0px; z-index: 2900; visibility: hidden; transform: translate3d(0px, 48px, 0px); transition: transform 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, visibility 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;">
                <div width="3" style="background-color: rgb(255, 243, 193); padding: 0px 24px; height: 48px; line-height: 48px; border-radius: 2px; max-width: 568px; min-width: 288px; flex-grow: 0; margin: auto; color: rgba(33, 33, 33, 0.87);">
                    <div style="font-size: 14px; color: rgb(255, 255, 255); opacity: 0; transition: opacity 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;">
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="CartPayment__mobile-cashback___azElx">
                <div class="ApplyCashback__outer-container___3ICor">
                    <div class="row ApplyCashback__inner-container___1mlzT">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-7">
                <div class="row">
                    <div class="col-xs-12 CartPayment__select-payment-title___3Qzwq">Select Payment Mode</div>
                </div>
                <div class="CartPayment__inner-container___3gc5E">

                    <a class="t-PAYTM">

                        <div class="PaymentModes__card___27Vom">
                            <form action="">
                                <input type="checkbox" name="paymentMethod" value="e-Sewa(Paytm Wallet, Debit/Credit Card, Internet Banking)" />
                            </form>
                            <div class="PaymentModes__card-left-padding___1ab7O">
                                <div class="PaymentModes__payment-title___1aWC2">e-Sewa<span class="pull-right">
                                <img src="https://techlekh.com/wp-content/uploads/2017/06/esewa-logo.png" alt="payment-logo" class="PaymentModes__payment-logo___3FZ_3"></span>
                                </div>
                                <div class="PaymentModes__text___12ges">
                                    <div class="PaymentModes__message___1Mlo5">Paytm Wallet, Debit/Credit Card, Internet Banking</div>
                                    <div>
                                        <div class="PaymentModes__promotional-message___3dW7k">Extra 10% Paytm Cashback* | Must Use Coupon: 1MG20 | For New Users Only*</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a class="t-RAZORPAY">

                        <div class="PaymentModes__card___27Vom">

                            <form action="">
                                <input type="checkbox" name="paymentMethod" value="Debit/Credit Card, Internet Banking"/>
                            </form>
                            <div class="PaymentModes__card-left-padding___1ab7O">
                                <div class="PaymentModes__payment-title___1aWC2">ONLINE PAYMENT

                                    <span class="pull-right">
                                        <img src="http://res.cloudinary.com/du8msdgbj/image/upload/v1496311436/marketing/nyc2nn6sbuqelpvkaxmk.png" alt="payment-logo" class="PaymentModes__payment-logo___3FZ_3">
                                    </span>
                                </div>
                                <div class="PaymentModes__text___12ges">
                                    <div class="PaymentModes__message___1Mlo5">Debit/Credit Card, Internet Banking</div>

                                    <div>

                                        <div class="PaymentModes__promotional-message___3dW7k">
                                            15% Cashback on first Freecharge payment on 1mg* (max. Rs. 50);
                                            10% MobiKwik Supercash* (max. Rs. 100) | Once per user
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a class="t-COD">
                        <div class="PaymentModes__card___27Vom">
                            <form action="">
                                <input type="checkbox" checked="checked" name="paymentMethod" value="Cash on Delivery"/>
                            </form>
                            <div class="PaymentModes__card-left-padding___1ab7O">
                                <div class="PaymentModes__payment-title___1aWC2">CASH ON DELIVERY</div>
                                <div class="PaymentModes__text___12ges">

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-md-5 CartPayment__top___2AEV_">

                <div class="CartSummary__outerContainer___1mopE">
                    <div class="row CartSummary__innerContainer___fqXQi">
                        <div class="col-xs-12 CartSummary__text___1vrNO">
                            <span class="col-xs-6">Amount</span>
                            <span class="col-xs-6 text-right total-cart">Rs. {{ $totalPrice }}</span>
                        </div>
                        
                        <div class="col-xs-12 CartSummary__text___1vrNO">
                            <span class="col-xs-6">Shipping Charges</span>
                            <span class="col-xs-6 text-right">As per delivery address</span>
                        </div>
                        <div class="col-xs-12 CartSummary__totalAmount___dkw0Q">
                            <span class="col-xs-6">To be paid</span>
                            <span class="col-xs-6 text-right discount-cart">Rs. {{ $totalPrice}}</span>
                        </div>
                        <div class="col-xs-12 CartSummary__cart-summary-footer___24W5I">
                    <span class="col-xs-12 CartSummary__cart-summary-footer-text___211r2">Total Savings
                        <span class="CartSummary__savings-amount___1D_o0">Rs. {{ $discountedPrice }}</span>
                    </span>
                        </div>
                        <!--                <div class="col-xs-12 CartSummary__shipping-footer___3HOkT">-->
                        <!--                    <div class="col-xs-12 CartSummary__shipping-footer-text___2ThfH">Free Shipping for orders above Rs 200</div>-->
                        <!--                </div>-->
                    </div>
                </div>
                <div>
                    <div>

                <form method="POST" action="{{ route('checkout') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="talalAmount" value="{{ $totalPrice }}"/>
                    <input type="hidden" name="totalDiscount" value="{{ $discountedPrice }}">
                    <input type="hidden" name="confirmedpaymentMethod" value="Cash on Delivery"/>

                    <input type="submit" value="PLACE ORDER" style="background-color: rgb(11, 197, 216); color: rgb(255, 255, 255);
                    height: 40px;width:100%;margin-top:15px; padding: 0px 16px; text-decoration: none; display: inline-flex; font-weight: 500;
                    font-size: 16px; border-radius: 4px; z-index: 100; cursor: pointer; align-items: center;
                    justify-content: center; border: 1px solid rgb(11, 197, 216);">

                </form>
                        </a>
                    </div>

                </div>
            </div>
        </div>

    </div>



    <div class="row">
        <div class="col-xs-12 col-md-5 col-md-push-7">
            <div class="Cart__disclaimer___N20CK">
                <div class="Cart__coupon-superscript-star___1fuIB">*</div>
                Swasthya Nepal will be credited 7 days after your complete order is delivered in case of Products and in case of
                Lab Services Swasthya Nepal will be credited within 24 hours from the time of generation of test report.
                Swasthya Nepal will not be credited in case a return request is initiated for the order.<br>
                <!--                <div class="Cart__coupon-superscript-star___1fuIB">**</div>Coupon Discount value may change if the total order value changes.-->
            </div>
        </div>
        <div class="col-xs-12 col-md-7 col-md-pull-5">
            <div class="Cart__disclaimer-small___1ptS3">
                Swasthya Nepal is a technology platform to facilitate transaction of business.
                The products and services are offered for sale by the sellers.
                The user authorizes the delivery personel to be his agent for delivery of the goods. For details read
                <a href="#">terms and conditions.</a>
            </div>
        </div>
    </div>
    <!--    <div>-->
    <!--        <a class="t-dont-require-prescription Cart__mobile-button___22S2i button-text" style="background-color: rgb(11, 197, 216); color: rgb(255, 255, 255); height: 40px; padding: 10px 16px; text-decoration: none; display: block; font-weight: 500; font-size: 16px; border-radius: 4px; z-index: 8; cursor: pointer; align-items: center; border: 1px solid rgb(11, 197, 216);">-->
    <!--            <div class="CartButton__to-be-paid___3DHbJ">TO BE PAID-->
    <!--                <div class="CartButton__total-amount___3yhJr">Rs 175</div>-->
    <!--            </div>-->
    <!--            <div class="CartButton__cart-button___2heyj">CHECKOUT</div>-->
    <!--        </a>-->
    <!--    </div>-->
</div>

