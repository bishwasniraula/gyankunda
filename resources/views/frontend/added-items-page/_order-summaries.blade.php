@extends('frontend.master')

@section('extra-header')

    <link type="text/css" href="{{ asset('frontend/css/_asideBar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/added-items/added-items.css') }}">
    <link href="{{ asset('frontend/css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('main-content')

<div class="container">
    <div id="cart_section" >
        @if(Session::has('cart'))
        <div class="CartStepper__margin___rPxU6">
            <div>
                <div class="Step__step___3K1ts Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 10 -->STEP <!-- /react-text --><!-- react-text: 11 -->1<!-- /react-text --><!-- react-text: 12 -->: <!-- /react-text --><!-- react-text: 13 -->My Cart<!-- /react-text --></div></div><div class="Step__step___3K1ts Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 16 -->STEP <!-- /react-text --><!-- react-text: 17 -->2<!-- /react-text --><!-- react-text: 18 -->: <!-- /react-text --><!-- react-text: 19 -->Order Summary<!-- /react-text --></div></div><div class="Step__step___3K1ts" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 22 -->STEP <!-- /react-text --><!-- react-text: 23 -->3<!-- /react-text --><!-- react-text: 24 -->: <!-- /react-text --><!-- react-text: 25 -->Payment<!-- /react-text --></div></div></div></div>

                <div class="row CartPayment__cart-items___KuzeB">
                    <div class="custom-snackbar" style="position: fixed; left: 0px; display: flex; right: 0px; bottom: 0px; z-index: 2900; visibility: hidden; transform: translate3d(0px, 48px, 0px); transition: transform 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, visibility 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;">
                <div width="3" style="background-color: rgb(255, 243, 193); padding: 0px 24px; height: 48px; line-height: 48px; border-radius: 2px; max-width: 568px; min-width: 288px; flex-grow: 0; margin: auto; color: rgba(33, 33, 33, 0.87);">
                    <div style="font-size: 14px; color: rgb(255, 255, 255); opacity: 0; transition: opacity 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;">
                        <span></span>
                    </div>
                </div>
            </div>

            <div class="CartPayment__mobile-cashback___azElx">
                <div class="ApplyCashback__outer-container___3ICor">
                    <div class="row ApplyCashback__inner-container___1mlzT">
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-9">
                <div class="row">
                    <div class="col-xs-12 CartPayment__select-payment-title___3Qzwq">Product(s) Summary</div>
                </div>
                <div class="CartLineItems__item-list___2p3SB">
                    <div style="padding: 7px 10px 0px 10px;font-size: 12px;">
                    <div class="row">
                        <div class="col-xs-12">
                                    <div class="row">
                                        
                                <div class="col-xs-3"><div class="CartLineItem__name___3JS5V">Product(s) Name</div></div>
                                <div class="col-xs-1"><div class="CartLineItem__price___1j-lm">Quantity</div></div>

                                <div class="col-xs-2"><div class="CartLineItem__price___1j-lm">Market Price</div></div>
                                <div class="col-xs-2"><div class="CartLineItem__price___1j-lm">Our Price</div></div>
                                <div class="col-xs-2"><div class="CartLineItem__price___1j-lm">Price Discount</div></div>
                                <div class="col-xs-2"><div class="CartLineItem__price___1j-lm">To be paid</div></div>
                                    </div>
                                </div>
                    </div> 
                            </div>
                            <hr style="padding:0px;">
                    @foreach($products as $product)
                        <div style="style=color: #757575;font-size: 12px;padding: 0px 10px 0px 10px;">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        
                                <div class="col-xs-3">-{{ $product['item']['title'] }}</div>
                                <div class="col-xs-1">{{ $product['qty'] }}</div>
                                <div class="col-xs-2">Rs. {{ $product['item']['selling_price'] }}</div>

                                <div class="col-xs-2">Rs. {{ $product['item']['price'] }}</div>
        <div class="col-xs-2">Rs. {{ ($product['selling_price']-$product['price']) }}</div>
         <div class="col-xs-2">Rs. {{ $product['price'] }}</div>
                                    </div>
                                </div>                                                              
                            </div>
                        </div>
                    @endforeach
                    <hr style="padding:0px;">
                    <div class="CartLineItem__container___1w1IL">
                    <div class="row">
                        <div class="col-xs-12">
                                    <div class="row">
                                        
                                <div class="col-xs-3"><div class="CartLineItem__name___3JS5V">Total</div></div>
                                <div class="col-xs-1"><div class="CartLineItem__price___1j-lm">--</div></div>
                                <div class="col-xs-2"><div class="CartLineItem__price___1j-lm">--</div></div>

                                <div class="col-xs-2"><div class="CartLineItem__price___1j-lm">--</div></div>
                                <div class="col-xs-2"><div class="CartLineItem__price___1j-lm">Rs. {{ $discountedPrice }}</div></div>

                                <div class="col-xs-2"><div class="CartLineItem__price___1j-lm">Rs. {{ $totalPrice }}</div></div>
                                    </div>
                                </div>

                    </div> 
                            </div>


                </div>
            </div>

            <div class="col-xs-12 col-md-3 ">
                <div class="CartSummary__outerContainer___1mopE">
                    <div class="row">
                        <div class="col-xs-12 CartSummary__cart-summary-footer___24W5I">
                    <span class="col-xs-6 CartSummary__cart-summary-footer-text___211r2">Total Saving

                    </span>
                    <span class="col-xs-6 CartSummary__cart-summary-footer-text___211r2 text-right" style="color: #1aab2a;font-weight: 600;">Rs. {{ $discountedPrice }}

                    </span>
                        </div>
                     </div>

                    </div>
                    <a class="t-cart-address-continue CartDeliveryDetails__desktop-button___29znG button-text" href="{{ URL::to('/payment') }}" style="background-color: rgb(11, 197, 216); color: rgb(255, 255, 255); margin-top: 15px;
               height: 40px;width:100%; padding: 0px 16px; text-decoration: none; display: inline-flex; font-weight: 500; font-size: 16px;
               border-radius: 4px; z-index: 100; cursor: pointer; align-items: center; justify-content: center; border: 1px solid rgb(11, 197, 216);">
                        <span style="display: inline-block;">GO TO PAYMENT</span>
                    </a>
                </div>

            <div class="col-xs-12" style="margin-bottom: 50px;">
                <div class="row">
                    
        <div class="col-xs-12 col-md-5 col-md-push-7">
            <div class="Cart__disclaimer___N20CK">
                <div class="Cart__coupon-superscript-star___1fuIB">*</div>
                Swasthya Nepal will be credited 7 days after your complete order is delivered in case of Products and in case of
                Lab Services Swasthya Nepal will be credited within 24 hours from the time of generation of test report.
                Swasthya Nepal will not be credited in case a return request is initiated for the order.<br>
                <!--                <div class="Cart__coupon-superscript-star___1fuIB">**</div>Coupon Discount value may change if the total order value changes.-->
            </div>
        </div>
        <div class="col-xs-12 col-md-7 col-md-pull-5">
            <div class="Cart__disclaimer-small___1ptS3">
                Swasthya Nepal is a technology platform to facilitate transaction of business.
                The products and services are offered for sale by the sellers.
                The user authorizes the delivery personel to be his agent for delivery of the goods. For details read
                <a href="#">terms and conditions.</a>
            </div>
        </div>
    
                </div>
            </div>
        </div>

 @else
      <div class="row">
        <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
          <h2>No Items In Cart</h2>
          <a href="{{ route('home/allproducts') }}" class="btn btn-success">Continue Shopping</a>
        </div>
      </div>
  @endif
    </div>
</div>

@endsection

