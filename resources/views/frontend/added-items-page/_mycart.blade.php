<div class="Cart__container___5UudH custom-top-margin">

    <!--    ajax section-->


    <div id="cart_section" >

        <div class="CartStepper__margin___rPxU6"><div><div class="Step__step___3K1ts Step__step-active___H-r3g" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 10 -->STEP <!-- /react-text --><!-- react-text: 11 -->1<!-- /react-text --><!-- react-text: 12 -->: <!-- /react-text --><!-- react-text: 13 -->My Cart<!-- /react-text --></div></div><div class="Step__step___3K1ts " style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 16 -->STEP <!-- /react-text --><!-- react-text: 17 -->2<!-- /react-text --><!-- react-text: 18 -->: <!-- /react-text --><!-- react-text: 19 -->Order Summary<!-- /react-text --></div></div><div class="Step__step___3K1ts" style="width: 32.3333%;"><div class="Step__title___cpJ5U"><!-- react-text: 22 -->STEP <!-- /react-text --><!-- react-text: 23 -->3<!-- /react-text --><!-- react-text: 24 -->: <!-- /react-text --><!-- react-text: 25 -->Payment<!-- /react-text --></div></div></div></div>
        <div class="row CartPayment__cart-items___KuzeB">
            <div class="custom-snackbar" style="position: fixed; left: 0px; display: flex; right: 0px; bottom: 0px; z-index: 2900; visibility: hidden; transform: translate3d(0px, 48px, 0px); transition: transform 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, visibility 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;">
                <div width="3" style="background-color: rgb(255, 243, 193); padding: 0px 24px; height: 48px; line-height: 48px; border-radius: 2px; max-width: 568px; min-width: 288px; flex-grow: 0; margin: auto; color: rgba(33, 33, 33, 0.87);">
                    <div style="font-size: 14px; color: rgb(255, 255, 255); opacity: 0; transition: opacity 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;">
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="CartPayment__mobile-cashback___azElx">
                <div class="ApplyCashback__outer-container___3ICor">
                    <div class="row ApplyCashback__inner-container___1mlzT">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-7">
                <div class="row">
                    <div class="col-xs-12 CartPayment__select-payment-title___3Qzwq">Order Summary</div>
                </div>


                <div class="CartLineItems__item-list___2p3SB">

                    @if (sizeof(Cart::content()) > 0)

                        @foreach (Cart::content() as $item)

                    <div class="CartLineItem__container___1w1IL"><div class="row"><div class="col-xs-7"><div class="CartLineItem__name___3JS5V">Himalaya Cocoa Butter Intensive Body Lotion</div><div class="CartLineItem__pack-size-label___xORIi">bottle of 400 ml Lotion</div></div><div class="col-xs-5 text-right"><div class="CartLineItem__price___1j-lm"><!-- react-text: 124 -->₹<!-- /react-text --><!-- react-text: 125 -->225<!-- /react-text --></div><div class="CartLineItem__actual-price___1Juw7"><!-- react-text: 127 -->₹<!-- /react-text --><!-- react-text: 128 -->250<!-- /react-text --></div><!-- react-text: 129 --><!-- /react-text --></div></div><div class="row"><a class="button-text"><div class="col-xs-6"><img src="/images/delete_icon.svg" alt="delete icon" class="CartLineItem__delete-medicine___1oaAq"><span class="CartLineItem__txt-delete-medicine___2PC18">Remove</span></div></a><div class="col-xs-6 text-right"><div><div class="text-left"><div class="custom-snackbar" style="position: fixed; left: 0px; display: flex; right: 0px; bottom: 0px; z-index: 2900; visibility: hidden; transform: translate3d(0px, 48px, 0px); transition: transform 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, visibility 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;"><div width="3" style="background-color: rgb(255, 243, 193); padding: 0px 24px; height: 48px; line-height: 48px; border-radius: 2px; max-width: 568px; min-width: 288px; flex-grow: 0; margin: auto; color: rgba(33, 33, 33, 0.87);"><div style="font-size: 14px; color: rgb(255, 255, 255); opacity: 0; transition: opacity 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;"><span></span></div></div></div></div><a><img src="/images/minus-cart.svg" alt="decrease"></a><div class="Quantity__quantity___3V97B"><span class="Quantity__text___2EoLZ">1</span></div><a><img src="/images/plus-cart.svg" alt="increase"></a></div></div></div></div>

                    @endforeach

                    @else

                         <h3 style="color: #d81010;text-align: center;margin-top: 26px;">You have no items in your shopping cart</h3>


                @endif


                </div>
            </div>
            <div class="col-xs-12 col-md-5 ">

                <div class="CartSummary__outerContainer___1mopE">
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="col-xs-6">MRP Total</span>
                            <span class="col-xs-6 text-right"><strong>Rs. </strong></span>
                        </div>

                        <div class="col-xs-12 CartSummary__cart-summary-footer___24W5I">
                    <span class="col-xs-12 CartSummary__cart-summary-footer-text___211r2">
                        <span class="CartSummary__savings-amount___1D_o0"> </span>
                    </span>
                        </div>
                        <!--                <div class="col-xs-12 CartSummary__shipping-footer___3HOkT">-->
                        <!--                    <div class="col-xs-12 CartSummary__shipping-footer-text___2ThfH">Free Shipping for orders above Rs 200</div>-->
                        <!--                </div>-->
                    </div>
                </div>
                <div>
                    <form method="POST" id="form-COD" action="" class="hide">
                        <input type="hidden" name="orderId" value="">
                        <input type="hidden" name="userId" value="">
                        <input type="hidden" name="cashback_availed" value="">
                        <input type="hidden" name="_csrf" value="">
                        <input type="submit" value="submit">
                    </form>
                    <form id="form-PAYTM" class="hide" method="POST" action="">
                        <input type="hidden" name="mode" value="">
                        <input type="hidden" name="origin_url" value="">
                        <input type="hidden" name="_csrf" value="">
                        <input type="hidden" name="cashback_availed" value=""><input type="submit" value="submit"></form>
                    <form method="POST" id="form-RAZORPAY" action="" class="hide">
                        <input type="hidden" name="payment_mode" value="">
                        <input type="hidden" name="txn_amount" value="">
                        <input type="hidden" name="order_id" value="">
                        <input type="hidden" name="cashback_availed" value="">
                        <input type="hidden" name="razorpay_payment_id" value="">
                        <input type="hidden" name="razorpay_order_id" value="">
                        <input type="hidden" name="razorpay_signature" value="">
                    </form>
                    <form id="form-PAYU" class="hide" method="POST" action="">
                        <input type="hidden" name="mode" value="">
                        <input type="hidden" name="origin_url" value="">
                        <input type="hidden" name="_csrf" value="">
                        <input type="hidden" name="cashback_availed" value="">
                        <input type="submit" value="submit">
                    </form>
                    <form id="form-CITRUS" class="hide" method="POST" action="">
                        <input type="hidden" name="mode" value="">
                        <input type="hidden" name="origin_url" value="">
                        <input type="hidden" name="_csrf" value="">
                        <input type="hidden" name="cashback_availed" value="">
                        <input type="hidden" name="callback_url" value="">
                        <input type="submit" value="submit">

                    </form>

                    <div>

                        <a class="t-confirm-order PlaceOrder__desktop-button___3kEQX button-text"
                           style="background-color: rgb(11, 197, 216); color: rgb(255, 255, 255);
                    height: 40px;width:100%;margin-top:15px; padding: 0px 16px; text-decoration: none; display: inline-flex; font-weight: 500;
                    font-size: 16px; border-radius: 4px; z-index: 100; cursor: pointer; align-items: center;
                    justify-content: center; border: 1px solid rgb(11, 197, 216);" href="{{ route('product/proceed') }}"><span style="display:
                     inline-block;">PLACE ORDER</span>
                        </a>
                    </div>

                </div>
            </div>
        </div>

    </div>



    <div class="row">
        <div class="col-xs-12 col-md-5 col-md-push-7">
            <div class="Cart__disclaimer___N20CK">
                <div class="Cart__coupon-superscript-star___1fuIB">*</div>
                Swasthya Nepal will be credited 7 days after your complete order is delivered in case of Products and in case of
                Lab Services Swasthya Nepal will be credited within 24 hours from the time of generation of test report.
                Swasthya Nepal will not be credited in case a return request is initiated for the order.<br>
                <!--                <div class="Cart__coupon-superscript-star___1fuIB">**</div>Coupon Discount value may change if the total order value changes.-->
            </div>
        </div>
        <div class="col-xs-12 col-md-7 col-md-pull-5">
            <div class="Cart__disclaimer-small___1ptS3">
                Swasthya Nepal is a technology platform to facilitate transaction of business.
                The products and services are offered for sale by the sellers.
                The user authorizes the delivery personel to be his agent for delivery of the goods. For details read
                <a href="#">terms and conditions.</a>
            </div>
        </div>
    </div>
    <!--    <div>-->
    <!--        <a class="t-dont-require-prescription Cart__mobile-button___22S2i button-text" style="background-color: rgb(11, 197, 216); color: rgb(255, 255, 255); height: 40px; padding: 10px 16px; text-decoration: none; display: block; font-weight: 500; font-size: 16px; border-radius: 4px; z-index: 8; cursor: pointer; align-items: center; border: 1px solid rgb(11, 197, 216);">-->
    <!--            <div class="CartButton__to-be-paid___3DHbJ">TO BE PAID-->
    <!--                <div class="CartButton__total-amount___3yhJr">Rs 175</div>-->
    <!--            </div>-->
    <!--            <div class="CartButton__cart-button___2heyj">CHECKOUT</div>-->
    <!--        </a>-->
    <!--    </div>-->
</div>