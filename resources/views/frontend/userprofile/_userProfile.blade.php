<section class="acc_info">
    <div class="container-fluid">
        
        <div id="content">
            <div class="headingtxt_single">
                <div>
                    <i class="fa fa-lock" aria-hidden="true"></i>
                </div>
                Your Account
            </div>
            <div class="row ords">
                <div class="col-md-4 col-sm-6  ords_tab">
                    <ul>
                        <li>
                          <a href="{{ route('userprofile') }}">
                            <button id="accinfo" data-id="accinfo_tab">Account Information</button></a>
                        </li>
                        <li>
                            <button id="ordhis" data-id="orderhis_tab">Order History</button>
                        </li>
                        <li>
                            <button id="uplpres" data-id="orderpre_tab">Prescriptions</button>
                        </li>
                        <li>
                            <button href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">Logout</button>
                        </li>

                    </ul>
                </div>
                <div class="col-md-8 ords_details">
                <!--    user ajax section-->
                    <div class="ords_details_in" id="content_sec">

                         <!-- account system Modal -->
<div class="modal fade" id="accInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Account system</h4>
            </div>
            <div class="modal-body">
    
           
                 <form method="post" action="javascript:void(0)" id="userUpdate">
                    <div id="acc_setup" class="popup">
                        <div id="divPeraccount" class="col-sm-12 mgn10">
                            <div class="row">
                                <div class="col-sm-6 " style="width:45%">
                                    <div class="form-group">
                                        <label for="user_name">Username</label>

                                        <input name="user_name" type="text" id="user_name" readonly="" class="form-control" size="33" value="{{ $userInfo[0]->email }}">

                                    </div>

                                    <div class="form-group">
                                        <label for="new_password">Enter New Password *</label>
                                        <input name="new_password" type="password" id="new_password" maxlength="50" class="form-control" size="33">
                                    </div>
                                    <div class="clear1"></div>
                                    <div class="form-group">
                                        <label for="confirm_new_password">Confirm Your Password *</label>
                                        <input name="confirm_new_password" type="password" id="confirm_new_password" maxlength="50" class="form-control" size="33">
                                    </div>
                                    <div class="clear1"></div>
                                    <input type="submit" name="btnSaveuser" value="Save" id="btnSaveuser" class="ords_btns">
                                </div>
                                <div class="col-sm-6 top-h" style="font-family:'open_sansregular'"> <b>Important:</b> Make sure your password is a mix of numbers and letters of at least 6 characters long.</div>
                            </div>
                        </div>
                        <div class="clear_h"></div>
                    </div>

                </form>
    


            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<!-- personal information modal-->
<div class="modal fade" id="personalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Personal Information</h4>
            </div>
            <div class="modal-body">

                <form method="post" action="#" id="personalInfos">
                    <div id="personal_info" class="popup">
                        <div id="divPerinfo" class="col-sm-12 mgn10">
                            <div class="row">
                                <div class="col-sm-6 line-right2" style="width:50%">
                                    <div class="form-group">
                                        <label for="first_name1">First Name *</label>
                                        <input name="first_name" type="text" id="first_name" maxlength="25" size="40" class="form-control" value="{{ $userInfo[0]->firstName }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name1">Last Name *</label>
                                        <input name="last_name" type="text" id="last_name" maxlength="25" size="40" class="form-control" value="{{ $userInfo[0]->lastName }}">
                                    </div>
                                    <div class="form-group">
                                        <label style="height:18px;" for="phone_number1">Mobile Number *</label>
                                        <input name="phone_number" type="text" id="phone_number" maxlength="10" size="36" onkeypress="return onlyNumbers();" class="form-control number-bg" value="{{ $userInfo[0]->mobileNum }}">
                                    </div>
                                    <div class="wall-txt pull-left">
                                        <input name="chkSMS" type="checkbox" id="chkSMS" {{ $userInfo[0]->notify_order_by =='sms' ? 'checked' :'' }}>
                                        <label for="chkSMS"><span></span>I wish to be notified of my order by SMS.</label>
                                    </div>
                                </div>
                                <div class="col-sm-5 lft-marg" style="width:50%">
                                    <div class="form-group">
                                        <label for="gender">Gender *</label>
                                        <div class="list-select">
                                            <label>
                                                <select name="gender" id="gender" tooltiptext="Please select Gender!">
                                                    @if(!empty($userInfo[0]->gender))
                                                    {{ $userInfo[0]->gender }}
                                                    @else
                                                    <option selected="selected" value="{{ $userInfo[0]->gender }}">
                                                    @endif
                                                    </option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                    <option value="Other">Other</option>
                                                    
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="date_of_birth">Date of Birth (e.g. YYYY-MM-DD)*</label>
                                        <input name="date_of_birth" type="text" id="date_of_birth" class="form-control is-datepick" maxlength="10" size="40" value="{{ $userInfo['0']->DoB }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email ID*</label>
                                        <input name="email" type="text" id="email"  class="form-control" maxlength="50" size="40" value="{{ $userInfo[0]->email }}">
                                    </div>
                                    <input type="submit" name="btnSavepersonal" value="Save" id="btnSavepersonal" class="ords_btns">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<!-- billing address modal-->
<div class="modal fade" id="billingInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Billing Information</h4>
            </div>
            <div class="modal-body">

                <form method="post" action="#" id="billingInfos">
                    {{ csrf_field() }}
                    <div id="billing" class="popup">
                        <div class="col-sm-12 mgn10">
                            <div class="row">
                                <div class="col-sm-6 line-right2" style="width:45%">
                                    <div class="form-group">
                                        <label for="billing_username">Pincode *</label>
                                        <input name="zipcode" type="text" id="zipcode" class="form-control" size="40" maxlength="6" onkeypress="testing();" value="{{ $userInfo[0]->zipcode }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="billing_street_address">Address*</label>

                                        <textarea name="address_full" id="address_full" type="text" class="form-control field-required" maxlength="200" tooltiptext="Please enter Address!" style="height:117px;width:90%" placeholder="Please enter full address: Flat/Door No., Street/Road name &amp; Locality." >{{ $userInfo[0]->address_full }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="billing_suite_or_apt">Mobile Number*</label>
                                        <input name="billing_phoneno" type="text" id="billing_phoneno" class="form-control" size="12" maxlength="10" value="{{ $userInfo[0]->billing_phoneno }}">
                                    </div>
                                </div>
                                <div class="col-sm-5 lft-marg" style="width:45%">
                                    <div class="form-group">
                                        <label for="billing_city">City *</label>
                                        <input name="inpBCity" type="text" id="inpBCity" class="form-control" size="40" maxlength="25" value="{{ $userInfo[0]->inpBCity }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ddlbill">State *</label>
                                        <input name="inpBillState" type="text" id="inpBillState" class="form-control field-required" maxlength="35" tooltiptext="Please enter Your State!" value="{{ $userInfo[0]->Bstate }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="country">Country*</label>
                                        <div class="list-select ErrorField">
                                            <label>
                                                <select name="ddlBillCountry" id="ddlBillCountry" title="Please Select Country!" class="form-control field-required">
                                                    @if(!empty($userInfo[0]->BillCountry))
                                                    <option value="{{ $userInfo[0]->BillCountry }}">{{ $userInfo[0]->BillCountry}}</option>
                                                    @else
                                                    <option selected="selected" value="Select">Select</option>
                                                    <option value="Nepal">Nepal</option>
                                                @endif
                                                </select>
                                            </label>
                                        </div><span class="ValidationErrors">Please select your Country!</span>
                                    </div>
                                    <div class="form-group">
                                        <div class="clear1"></div>
                                    </div>
                                    <input type="submit" name="saveBillingInfo" value="Save" id="saveBillingInfo" class="ords_btns">

                                </div>
                            </div>
                        </div>
                        <div class="clear1"></div>
                    </div>
                </form>

            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<!-- delivery address modal-->
<div class="modal fade" id="deliveryInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4  id="myModalLabel">Deliveries Information</h4>
            </div>
            <div class="modal-body">

                <form method="post" action="#" id="deliveryInfos">

                    {{ csrf_field() }}

                    <div id="shipping" class="popup">
                        <div class="col-sm-12 mgn10">
                            <div class="row">
                                <div class="col-sm-6 line-right2" style="width:45%">
                                    <div class="form-group">
                                        <label for="shipping_full_name">Full Name*</label>
                                        <input name="shipping_full_name" type="text" id="shipping_full_name" class="form-control" maxlength="30" tooltiptext="Please Enter Shipping First Name!" value="{{ $userInfo[0]->shipping_full_name }}"
>
                                    </div>
                                    <div class="form-group">
                                        <label for="shipping_zipcode">Pincode *</label>
                                        <input name="shipping_zipcode" type="tel" id="shipping_zipcode" class="form-control" size="40" maxlength="6" value="{{ $userInfo[0]->shipping_zipcode }}"
>
                                        <span id="lblspinerr"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="shipping_street_address">Address*</label>
                                        <div href="#" data-placement="top" data-trigger="focus" tabindex="0" id="example" data-toggle="popover" class="new_wrapperprd bckgnone" rel="popover" data-popover="popover_content" data-content="" data-original-title="">?</div>
                                        <div id="popover_content" style="display: none">
                                            <div><span><div style="font-size: 13px; text-align: left;" class="fnt-smb">Address Entry Guidelines:</div>
                <div class="text-left">To prevent any delay in delivering your order, please provide the complete/correct address, along with a landmark near your location.</div></span></div>
                                        </div>

                                        <textarea name="txtShipAddress" id="txtShipAddress" type="text" class="form-control field-required" maxlength="200" tooltiptext="Please enter Address!" placeholder="Please enter full address: Flat/Door no., Street/Road name &amp; Locality." style="height:87px;">{{ $userInfo[0]->txtShipAddress }}
</textarea>
                                    </div>
                                    <div class="form-group"><label for="shipping_suite_or_apt">Landmark</label>
                                        <input name="shipping_suite_or_apt" type="text" id="shipping_suite_or_apt" class="form-control" size="40" maxlength="30" value="{{ $userInfo[0]->shipping_suite_or_apt }}"
>
                                    </div>
                                </div>
                                <div class="col-sm-5 lft-marg" style="width:45%">


                                    <div class="form-group ">
                                        <label for="address_full">Mobile Number*</label>
                                        <input name="Shipping_mobno" type="text" id="Shipping_mobno" class="form-control number-bg" size="12" maxlength="10" value="{{ $userInfo[0]->Shipping_mobno }}"
>
                                    </div>
                                    <div class="form-group">
                                        <label for="shipping_city">City *</label>
                                        <input name="shipping_city" type="text" id="shipping_city" class="form-control" size="40" maxlength="25" value="{{ $userInfo[0]->shipping_city }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="ddlship">State *</label>
                                        <div class="list-select">
                                            <label>
                                                <select name="ShipState" id="ddlShipState" class="aspNetDisabled form-control field-required">
                                                    <option selected="selected" value="{{ $userInfo[0]->ShipState}}">{{ $userInfo[0]->ShipState }}</option>
                                                    <option value="Provience-1">Provience-1</option>
                                                    <option value="Provience-2">Provience-2</option>
                                                    <option value="Provience-3">Provience-3</option>
                                                    <option value="Provience-4">Provience-4</option>
                                                    <option value="Provience-5">Provience-5</option>
                                                    <option value="Provience-6">Provience-6</option>
                                                    <option value="Provience-7">Provience-7</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <br>
                                        <div id="lblShipCountry"><b>Nepal</b> (Delivery within Nepal ONLY)</div>
                                    </div>

                                    <div class="clear1"></div>

                                    <input type="submit" name="ImageButton4" value="Save" id="savedeliveryInfo" class="ords_btns">
                                </div>
                            </div>
                        </div>
                        <div class="clear_h"></div>
                        <div class="clear1 "></div>
                    </div>

                </form>

            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>



<!--        account information-->
<div id="accinfo_tab " class="myc_cont container-fluid" style="display:block">
    <div id="accview " style="display:block;">
        <div class="row pull-left ">
            <div class="col-md-6 ords_cinfo pull-left lga_height">
                <div class="ords_cinfo_h1">Login Information</div>
                <div class="ords_cinfo_inner">
                    <div class="pull-left ords_cinfo_gry myc_height">
                        <div class="form-group">
                            <div class="pull-left">Username:</div>
                            <div class="pull-left txt_blue">
                                &nbsp;
                                <span id="lblusername" style="font-weight: normal">{{ $userInfo[0]->email }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="pull-left">Password:</div>
                            <div class="pull-left txt_blue">
                                &nbsp;
                                <span id="lblpassword" style="font-weight: normal">*******</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="pull-right" data-target="#accInfo"><a class="ords_btns mdyt" href="#" data-toggle="modal" data-target="#accInfo">Modify</a> </div>

                </div>
            </div>
            <div class="col-md-6 ords_cinfo pull-left mrg-rt lga_height">
                <div class="ords_cinfo_h1">Personal Information</div>
                <div class="ords_cinfo_inner">
                    <div class="pull-left p100 myc_height">
                        <div>
                            <span id="lblFirstName" style="font-weight: normal">{{ $userInfo[0]->firstName }}</span>
                            <span id="lblLastName" style="font-weight: normal">{{ $userInfo[0]->lastName }}</span>
                        </div>
                        <div>
                            <span id="lblphone_number" style="font-weight: normal"></span>
                        </div>
                        <div>
                            <span id="lblGender" style="font-weight: normal">{{ $userInfo[0]->gender }}</span>
                        </div>
                        <div>
                            <span id="lbldateofbirth" style="font-weight: normal"></span>
                        </div>
                        <div>
                            <span id="lblEmail" style="font-weight: normal">{{ $userInfo[0]->pemail }}</span>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="pull-right"  ><a class="ords_btns mdyt" href="#" data-target="#personalInfo" data-toggle="modal">Modify</a> </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div id="divBilladd" class="col-md-6 col-xs-12 ords_cinfo_mid1 myc1_height pull-left">
                <div class="ords_cinfo_h1">Billing Address</div>
                <div class="ords_cinfo_inner">
                    <div id="divBillRH" class="myc1_height">
                        <div class="clear"></div>
                        <div class="lh20">
                            <span id="lblStreetAddress" style="font-weight: normal">Address: {{ $userInfo[0]->address_full }}</span>
                        </div>
                        <div>
                            <span id="lblstreet_address2" style="font-weight: normal"></span>
                        </div>
                        <div>
                            <span id="lblCity" style="font-weight: normal">City: {{ $userInfo[0]->inpBCity }}</span>
                            
                        </div>
                        <div>
                            <span id="lblzipcode" style="font-weight: normal">Zipcode: {{ $userInfo[0]->zipcode }}</span>
                        </div>
                        <div>
                            <span id="lblState" style="font-weight: normal">state: {{ $userInfo[0]->Bstate }}</span>
                            <span id="lblcountry" style="font-weight: normal"></span>
                        </div>
                    </div>
                    <div style="height: 7px" class="clearfix"></div>
                    <div id="divModAddbtn" class="pull-right">
                        <a class="ords_btns mdyt" href="#" data-toggle="modal" data-target="#billingInfo" >MODIFY
                        </a>
                    </div>

                </div>
            </div>
            <div id="divShipadd" class="col-md-6 ords_cinfo_mid1 pull-left mrg-rt" style="margin-right: 0px;">
                <div class="ords_cinfo_h1">Delivery Address(es)</div>


                <div id="divHeight" class="myc1_height">

                    <div class="carosel mycr" id="carosel1 ">
                        <a id="divHidden1" class="hidden"></a>
                        <div class="carosel-inner">
                            <ul id="SlideItMoo_items_1">
                                
                            <li>Full Name: {{ $userInfo[0]->shipping_full_name }}</li>
                            <li>Zip Code: {{ $userInfo[0]->shipping_zipcode }}</li>
                            <li>Address: {{ $userInfo[0]->txtShipAddress }}</li>
                            <li>Landmark: {{ $userInfo[0]->shipping_suite_or_apt }}</li>
                            <li>Mobile Number:{{ $userInfo[0]->Shipping_mobno }}</li>
                            <li>City: {{ $userInfo[0]->shipping_city }}</li>
                            <li>State: {{ $userInfo[0]->ShipState }}</li>
                            </ul>

                        </div>
                        <!--/carosel-inner-->

                        <a id="divHidden2" class="hidden"></a>
                    </div>

                    <div class="clear"></div>

                </div>

                <div class="clear1"></div>
                <div id="divAddmore" class="hidden" style="border-top: #afe6e9 solid 1px; padding-top: 10px"><a class="add-more mdyt" href="" >+ ADD ANOTHER ADDRESS</a> </div>
                <div id="divShipAddbtn" class="pull-right" style="margin-right:10px"><a class="ords_btns mdyt" href="#" data-toggle="modal" data-target="#deliveryInfo" >MODIFY</a></div>

                <div class="clear1"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>



                    </div>



                </div>


            </div>
        </div>



    </div>
</section>
@section('script')

    <script type="text/javascript">


  $(document).ready(function(){

    $('#billingInfos').on("submit", function(e){

      e.preventDefault();
      $("#bmgs").remove();

      
      var zipcode = $('input[name=zipcode]').val();
      var address_full = $('textarea[name=address_full]').val();
      var billing_phoneno = $('input[name=billing_phoneno]').val();
      var inpBCity = $('input[name=inpBCity]').val();
      var Bstate = $('input[name=inpBillState]').val();
      var BillCountry = $('select[name=ddlBillCountry]').val();


    $.ajax({
        url:"/add-billing-info",
        type:"POST",
        data:{'zipcode':zipcode,'address_full':address_full,'billing_phoneno':billing_phoneno,'inpBCity':inpBCity,'Bstate':Bstate,'BillCountry':BillCountry,'_token': $('input[name=_token]').val()},
        beforeSend:function(){
          $('#saveBillingInfo').val('SAVING...');
        },
        success:function (data) {
             $('#saveBillingInfo').val('SAVE');
              $('#saveBillingInfo').after('<span id="bmgs" style="color: #156215;font-size: 12px;padding: 0 2px;font-weight: bold;">Billing information updated successfully.</span>');
              setTimeout(function() {
                window.location.reload();
                }, 1000);
        },
        error: function (request, error) {
        console.log(arguments);
    }
    });
});

    $('#deliveryInfos').on("submit", function(event){
      event.preventDefault();
      $("#bmgs").remove();
      var shipping_full_name = $('input[name=shipping_full_name]').val();
      var shipping_zipcode = $('input[name=shipping_zipcode]').val();
      var txtShipAddress = $('textarea[name=txtShipAddress]').val();
      var shipping_suite_or_apt = $('input[name=shipping_suite_or_apt]').val();
      var Shipping_mobno = $('input[name=Shipping_mobno]').val();
      var shipping_city = $('input[name=shipping_city]').val();
      var ShipState = $('select[name=ShipState]').val();


    $.ajax({
        url:"/add-delevery-info",
        type:"POST",
        data:{'shipping_full_name':shipping_full_name,'shipping_zipcode':shipping_zipcode,'txtShipAddress':txtShipAddress,'shipping_suite_or_apt':shipping_suite_or_apt,'Shipping_mobno':Shipping_mobno,'shipping_city':shipping_city,'ShipState':ShipState,'_token': $('input[name=_token]').val()
      },
      beforeSend:function(){
          $('#savedeliveryInfo').val('SAVING...');
        },
        success:function (data) {
             $('#savedeliveryInfo').val('SAVE');
              $('#savedeliveryInfo').after('<span id="bmgs" style="color: #156215;font-size: 12px;padding: 0 2px;font-weight: bold;">Delivery information updated successfully.</span>');
              setTimeout(function() {
                window.location.reload();
                }, 1000);
        },
        error: function (request, error) {
        console.log(arguments);
    }
    });

    });

     $('#personalInfos').on("submit", function(ev){

      ev.preventDefault();

      $("#bmgs").remove();

      var first_name= $('input[name=first_name]').val();
      var last_name= $('input[name=last_name]').val();
      var phone_number= $('input[name=phone_number]').val();

        if($('input[name=chkSMS]').prop('checked') == true){
            var chkSMS = "SMS";
        }else{
           var chkSMS = "";
        }
      var gender = $('select[name=gender]').val();
      var date_of_birth = $('input[name=date_of_birth]').val();
      var email = $('input[name=email]').val();

     
    $.ajax({
        url:"add-personal-info",
        type:"POST",
        data:{'first_name':first_name,'last_name':last_name,'phone_number':phone_number,'chkSMS':chkSMS,'gender':gender,'date_of_birth':date_of_birth,'email':email,'_token': $('input[name=_token]').val()
      },
      beforeSend:function(){
          $('#btnSavepersonal').val('SAVING...');
        },
        success:function (data) {
             $('#btnSavepersonal').val('SAVE');
              $('#btnSavepersonal').after('<span id="bmgs" style="color: #156215;font-size: 12px;padding: 0 2px;font-weight: bold;">Personal information updated successfully.</span>');
              setTimeout(function() {
                window.location.reload();
                }, 1000);
        },
        error: function (request, error) {
        console.log(arguments);
    }
    });

    });
      
    $("#userUpdate").on("submit", function(e){


      e.preventDefault();
      $("#Amgs").remove();
      
      var password = $('input[name=new_password]').val();
      var cn_password = $('input[name=confirm_new_password]').val();
     
      if(password!=cn_password){
        $('#btnSaveuser').after('<span id="Amgs" style="color: #ef4b0a;font-size: 12px;padding: 0 2px;font-weight: bold;">Password didnot match.</span>');
      }else{
            
    $.ajax({
        url:"/update-password",
        type:"POST",
        data:{'password':password,'_token': $('input[name=_token]').val()},
        datatype:'josn',
        beforeSend:function(){
          $('#btnSaveuser').val('UPDATING...');
        },
        success:function (data) {
              if(data.success=='success'){

                $('#btnSaveuser').val('UPDATED');
                $('#btnSaveuser').after('<span id="Amgs" style="color: #156215;font-size: 12px;padding: 0 2px;font-weight: bold;">'+data.success+'</span>');
                setTimeout(function() {
                window.location.reload();
                }, 1000);

              }else{

                $('#btnSaveuser').val('SAVE');
                $('#btnSaveuser').after('<br/><span id="Amgs" style="color: #ef4b0a;font-size: 12px;padding: 0 2px;font-weight: bold;">'+data.error[0]+'</span><br/>');
              }
        },
        error: function (request, error) {
        console.log(arguments);
    }
    });
     }
});

  });
</script>
@endsection





