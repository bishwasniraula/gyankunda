    <div class="alert alert-success" id="success-mgs" style="display:none;">
        
    </div>
    <div class="alert alert-danger" id="error-mgs" style="display:none;">
  
    </div>
<!--        prescription-->
<form enctype="multipart/form-data" id="Userprescription">
     {{ csrf_field() }}
<div id="uplpres_tab" class="myc_cont" style="display:block;">
    <div class="col-sm-12 form-horizontal" id="uplodspit">
        <div class="row">
            <div class="form-group">
                <label class="col-sm-2 text-right notes_req">Order ID</label>
                <div class="col-sm-5">
                    
                    <div class="ords_in7">
                        <label>
                            <select name="ddlOrder"  id="ddlOrder" required>
                                <option selected hidden defalult value="">Select order ID</option>
                                @forelse($orders as $orderId)
                                <option value="{{ $orderId->id }}">ORDER-ID-{{ $orderId->id }}</option>
                                @empty
                                    <option selected hidden defalult value="">No Orders Placed Yet</option>
                                @endforelse

                            </select>
                        </label>
                    </div>
                </div>
                
            </div>
            <div class="form-group" id="presup_error">
                <label class="col-sm-2 text-right notes_req">Upload</label>
                <div class="col-sm-10 parentdivelement">
                    <label class="file-upload file-upload-con">
                        <input type="file" name="file" id="file_upload" class="file-pres" >
                        <div id="progress"></div>

                        <!-- <input type="file" name="file_upload" value="Upload image" title="Select File" id="btnUpload" class="ords_btns_upload"> -->
                    </label>
                    <span style="font-size: 11px;" class="file_allowed">(Only upload .jpeg, .jpg, .gif or .png Image size is less than 10MB)</span>
                   
                    <div>
                        <div>
                            <div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <label class="col-sm-2"></label>
            <div class="col-sm-3">
                <input type="submit" name="btnUpload" value="Upload Now" id="btnUpload" class="ords_btns_upload">
            </div>
            
        <ul></ul>
    </div
</form>
            <div class="clear_h"></div>
        </div>
    </div>
    <div class="clear_h"></div>
    <div class="ords_in3">
        <div>

        </div>
    </div>
</div>
<!--        /prescription-->
