@extends('frontend.master')

@section('extra-header')

    <link type="text/css" href="{{ asset('frontend/css/_asideBar.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/added-items/added-items.css') }}">
    <link href="{{ asset('frontend/css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">
	
@endsection

@section('main-content')

  @include('frontend.added-items-page._payments')

@endsection

@section('script')
    <script type="text/javascript">
            $('document').ready(function(){
            	$('input:checkbox').click(function() {
        			$('input:checkbox').not(this).prop('checked', false);
        			var paymentMethod = $(this).val();
					$('input[name=confirmedpaymentMethod]').val(paymentMethod);
    		});
          });
    </script>
@endsection