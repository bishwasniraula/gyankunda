@extends('frontend.master')

@section('extra-header')

    <link href="{{ asset('frontend/css/category/category.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/category/exclusive.css') }}" rel="stylesheet">
    
@endsection

@section('main-content')

        @include('frontend/pages/category/_category')

@endsection
