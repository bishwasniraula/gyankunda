<div class="main-content custom-top-margin">
    <div style="" class="content">
<!--        <button class="btn btn-secondary btn-back-to-top hide-imp">-->
<!--            <i class="fa fa-angle-up"></i></button>-->

<!--        <div class="container-fluid breadcrumb"></div>-->


        <div class="container-fluid">
            <div class="js-alert-section"></div>
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="display-inline-inline sku-page-cat">Popular Health Product Categories</h1>
                </div>
            </div>
            <div class="row">
                @forelse($categories as $category)
                <div class="col-xlg-2 col-md-3 col-sm-4 col-xs-6">
                    
                    <a href="{{ url('/medicine-category-products' ) }}/{{ $category->category_slug }} ">
                        <div class="category-wrap text-center individual-category">
                            <div class="category-image">
                                <img src="{{ asset('swasthya/products-category/'.$category->cover_photo )}}">
                                    <div class="category-name">{{ $category->category_name }}</div>
                            </div>
                        </div>
                    </a>
                </div>
                @empty
                    <p>No category found!!</p>
                @endforelse
    
            </div>
        </div>
    </div>
</div>

