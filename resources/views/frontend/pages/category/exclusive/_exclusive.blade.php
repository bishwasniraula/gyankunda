<div class="main-content custom-top-margin">
    <div style="" class="content">
        <div class="container-fluid">
            <div class="category-list-section">
                <div class="row">                    
                    <div class="col-md-12 col-sm-12 sku-list">
                        <div class="cat-breadcrumb">
                            <a href="{{ URL::to('/') }}" class="home-link button-text">Home</a>
                            <i class="fa fa-angle-right breadcrumb-icon"></i>
                            <a href="{{ route('medicine-categories') }}">Categories</a>
                            <i class="fa fa-angle-right breadcrumb-icon"></i>
                            <span>{{ $category->category_name }}</span>
                        </div>
                        <h1 class="display-inline-inline sku-page-cat">{{ $category->category_name }} products</h1>
                        
                        <div class="filter-list hidden-xs">
                            <a class="clear-filter button-text hide">CLEAR FILTERS</a>
                        </div>
                        <div class="row mt0 js-alert-section sku-list-row">
                    <!--container-->
                      @forelse($products as $product)

                            <div class="col-md-3 col-sm-3 col-xs-12 dia-sku">
                                <div class="sku-container">
                                    <div class="content-wrap">
                                        <a href="{{ url('/medicine/product') }}/{{ $product->slug }}" class="link-sku-item">
                                            <div class="sku-img">
                                                <img src="{{ asset('swasthya/products/'.$product->cover_photo) }}" alt="{{ $product->title }}">
                                            </div>
                                            <div class="sku-name">{{ $product->title }}</div>
                                        </a>
                                        <!-- <div class="sku-pack">packet of 1 Device</div> -->
                                        <div class="sku-price">
                                            <div class="sku-offer display-inline">Rs. {{ $product->selling_price }}</div>
                                            <div class="sku-actual display-inline">Rs. Rs. {{ $product->price }}</div>
                                        </div>
                                    </div>
                                    <a id="f-{{ $product->id }}" data-id="{{ $product->id }}" class="btn btn-checkout btn-add-sku-to-cart btn-primary addToCart">
                                    <div class="add-to-cart-text">Add to cart</div>
                                    </a>
                                </div>
                            </div>
                            @empty
                                <p>No medicine available !!</p>
                            @endforelse
                       </div>
                        @if($products->count() > 0)
                            <div class="row text-center loading">
                                <div class="col-md-12">
                                    <a href="#" class="pgntnCntnrBar btn btn-primary text-small button-text">Load More</a>
                                    <div class="loading-icon v2 hide">
                                    </div>
                                </div>
                            </div>
                        @else

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('script')
 <script type="text/javascript">

    $(document).ready(function(){

       $('.addToCart').click(function(){

        var qty = 1;
        var id = $(this).attr('data-id');
        
        $.ajax({
            url:"/single-add",
            type:"POST",
            data:{'qty':qty,'id':id,'_token':'<?php echo csrf_token() ?>'},
            datatType : 'json',
            success:function(data){
               
                if(data.success=='success'){

                    $('#f-'+id).text('Added');
                    $('.Iitems-count').text(data.cartCount);
                                        setTimeout(function(){
                    $('#f-'+id).text('Add To Cart');
                    }, 500);

                            }
            }
        })
       })
    });
    
    </script>
@endsection