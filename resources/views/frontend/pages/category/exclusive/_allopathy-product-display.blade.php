<div class="custom-top-margin">
    <div class="row content-container-row">
        <div class="container-fluid padding-none margin-b">
            <div class="col-xs-12 col-sm-7 col-md-7">
                
                <ol class="breadcrumb bgclr-tpt ellips margin-none" style="margin-top: 10px !important;">
                    <li class="">
                        <a href="{{ url('/allproducts') }}">
                            <span>Shop</span>
                        </a>
                    </li>
                    <li class="caps padding-none ellips">
                        <span>{{ $product->title }}</span>
                    </li>
                </ol>
            </div>
            <!-- <div class="col-xs-12 col-sm-5 col-md-5">
                <h5 class="text-right padding-r-15 margin-t-none more-related">
                    Other products from <a class="color-red" href="exclusive.php"><strong>AVELIA</strong></a>
                </h5>
            </div> -->
        </div>
    </div>
    <div class="row content-container-row padding-all-15">
        <div class="container-fluid padding-none">
            <div class="col-xs-12 col-sm-12 col-md-12" >
                <div class="container-fluid padding-none">
                    <div class="col-xs-12 col-sm-6 col-md-6 padding-none">
                        <div class="style_product-image">
                            <img src="{{ asset('swasthya/products/'.$product->cover_photo) }}" title="{{ $product->title }}" class="img-responsive" alt="{{ $product->title }}">
                        </div>
                        <div class="row DrugInfo__drug-uses" >
                            <div class="col-md-6 col-xs-12" >
                                <span class="DrugInfo__title" >Primarily used for</span>
                                <div class="DrugInfo__uses" >
                                    <span>
                                    {{ $product->basic_use }}
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12" >
                                <span class="DrugInfo__title" >Potentially
                                    <span class="label label-danger">unsafe</span> with
                                </span>
                                <div >
<!--                                    <i class="fa fa-glass" aria-hidden="true"></i>-->
                                    <div class="DrugInfo__potential-with fa fa-glass" >
                                     @if(!empty($product->  situations_states ))

                                     <?php 
                                        $data = json_decode($product->situations_states, true);

                                        foreach($data as $value){
                                            if($value['state']==0){

                                        echo $value['situation'].', ';
                                            }
                                        }?>

                                     @else
                                     @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <h1 class="caps">{{ $product->title}}</h1>
                        <!-- New Product Detail view over -->
                        <div class="container-fluid details-container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 padding-none ">
                                    <dl class="dl-horizontal">
                                        <dt>MRP</dt>
                                        <dd class="margin-b">
                                            <span class="strike sp" data-price="{{ $product->selling_price }}">Rs {{$product->selling_price }}</span>
                                            <div class="lead color-red" data-price="{{ $product->price }}">Rs <span>{{$product->price }}</span>
                                                <div class="offer">Save 50%</div>
                                                <div class="tc hide">
                                                    <a class="color-black" href="Javascript:void();" title="Terms &amp; Conditions">T&amp;C <i class="fa fa-external-link vvsmall-text" aria-hidden="true"></i></a>

                                                </div>

                                            </div>
                                        </dd>
                                        
                                        <dt class="margin-t">Delivery</dt>
                                        <dd class="margin-b">
                                            <form class="form-inline">
                                                <!-- <input type="text" class="form-control pincode" placeholder="Enter Pincode or Locality" value="">
                                                  <button type="submit" class="btn btn-link check"><strong>Check</strong></button> -->
                                                <p class="help-block">Expected in 4 - 48 HRS</p>
                                            </form>
                                        </dd>
                                        <dd class="margin-b">
                                            <span class="recommended" style="padding:5px;border:1px solid lightgreen;color:green">Swasthya nepal recommended</span>

                                            <div id="clicker" class="text-center">
                                                <span style="font-size: 18px;line-height: 36px;">
    <i class="fa fa-angle-left reduce-qty"></i><strong id="qty" style="padding:0 38px;font-size:12px;">1</strong> 
    <i class="fa fa-angle-right add-qty"></i>
</span>
                                            </div>



                                        </dd>

                                        <!-- <dd class="margin-b">
                                            <div id="clicker" style="margin-top: 15px;"></div>
                                        </dd> -->
                                        <dt></dt>
                                        <dd>
                                            <form role="form" id="addToBagForm" class="form-inline ">
                                                <input type="hidden" id="productId" name="productId" value="AVEL0006">
                                                <span class="productName hide">Paracetamol 500mg Tablet</span>

                                                <input type="hidden" name="quantity" value="1" placeholder="Quantity" class="form-control number qty" ondrop="return false">
                                                <!-- <button class="btn btn-danger addToCartBtn" type="button" id="addToCartBtn" onClick="addSingleProductToCart(this)"><i class="fa fa-shopping-cart padding-r"></i>Add to Bag</button> -->
                                                <div class="form-group btn-group hide">
                                                    <div class="input-group">
                                                        <div class="clearfix"></div>
                                                        <div class="qtyMsg"></div>
                                                    </div>
                                                </div>
                                                <div class="container-fluid">
                                                    <div class="row">

            <a id="addToCart" data-id="{{ $product->id }}" class="btn btn-danger addToCartBtn">
              Add To Cart
            </a>

                                                        <div class="AdditionalOffers__offer-container">
                                                            <div class="AdditionalOffers__offer-heading" >Additional offers</div>
                                                            <div class="AdditionalOffers__offer-content" >
                                                                <div class="AdditionalOffers__offer-detail" >Extra 15% MobiKwik Supercash* (max. Rs. 150) | On 1st MobiKwik wallet payment on 1mg&nbsp;| Once per user till Oct 31st, 2017</div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-none "></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- New Product Detail view over -->
    <div class="row content-container-row" >
        <div class="ProductDescription__product-description col-xs-12 col-sm-12 col-md-8 margin-t margin-b-20" style="border: 1px solid lightgrey;border-radius:5px">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Medicine Overview</a></li>
                    <li role="presentation"><a href="#warning" aria-controls="profile" role="tab" data-toggle="tab">Warnings</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="medicineOverview_container">
                            <span class="medicineOverview_title">Medicine Overview of {{ ucfirst($product->title) }}</span>
                            <div class="row medicineOverview_item">
                                <div class="col-xs-4">
                                    <div class="druguses_container">
                                        <span class="druguses_header">drug uses of {{ ucfirst($product->title) }}</span>
                                        <div class="druguses_content">
                                                {{ $product->description }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="drugSideEffects_container">
                                        <span class="drugSideEffects_header">side effects of {{ ucfirst($product->title) }}</span>
                                        <div class="drugSideEffects_content">
                                           {{ $product->side_effect }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="drugConsumption_container">
                                        <span class="drugConsumption_header">How to use {{ ucfirst($product->title) }}</span>
                                        <div class="drugConsumption_content">
                                           {{ $product->how_to_use }}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="warning">
                        <div class="drugsWarning_container">
                            <span class="drugsWarning_title">warnings</span>
                            <div class="drugWarning__sub-header">Special precautions for {{ ucfirst($product->title) }}</div>
                            <div class="table-responsive">

                                <table class="table table-hover">
                                    <thead>
                                    <tr class="info" style="color:#000000">
                                        <th>Situation</th>
                                        <th>State</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($product->  situations_states ))

                                     <?php 
                                        $data = json_decode($product->situations_states, true);

                                foreach($data as $value){
                                    if($value['state']==1){

                                echo '<tr>
                                        <td>'.$value['situation'].'</td>
                                        <td>
                                            <div class="label label-default">Safe</div>
                                        </td>
                                    </tr>';    
                                        }else{
                                           echo '<tr>
                                        <td>'.$value['situation'].'</td>
                                        <td>
                                            <div class="label label-danger">Unsafe</div>
                                        </td>
                                    </tr>';  
                                        }

                                        }?>

                                     @else
                                     @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4 OtcPage__doctor-card">
            <div >
<!--                substitute medicine section-->
                <div class="substitute_wrapper">
                    <div class="substitute_container">
                        <span class="substituteList_title">substitute for {{ $product->title }}</span>

                        <div class="substituteList_list">
                        @forelse($substitute_product as $alernative_product)
                            <div class="row substituteItem_item">
                                <div class="col-xs-6 substituteItem_item-container">
                                    <a href="{{ url('/product/' ) }}/{{ $alernative_product->slug }}">
                                        <span class="substituteItem_name">{{ $alernative_product->title }}</span>
                                    </a>
                                    <div class="substituteItem_manufacture-name">{{ $alernative_product->manufacturing }}</div>
                                </div>

                                <div class="col-xs-6 text-right substituteItem_price">
                                    <div class="substituteItem_unit-price">Rs.{{ $alernative_product->selling_price }}/Tablet</div>
                                    <div class="substituteItem_save-item">
                                        <span>save</span>
                                        <span>80%</span>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>No Substitute drug found!!</p>
                        @endforelse
                            
                        </div>
                        @if(!empty($count))
                        <div class="substituteList_extra-link text-right">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#viewall">
                                <span >View all</span>
                            </a>
                        </div>
                        @else
                        @endif
                    </div>
                </div>
                <!--related medicine-->
                <div class="relatedlabTests_container">
                    <div class="relatedlabTests_title">Related labtest</div>
                    <div class="relatedlabTests_test">
                        <a href="#" class="relatedlabTests_name">
                            blood count
                        </a>
                    </div>
                </div>

                <!--related physician-->
                <div class="relatedDoctorsList_container">
                    <div class="relatedDoctorsList_title">top physician</div>
                    <div class="row">
                        <div class="col-xs-2 relatedDoctorCard_doctor">
                            <a href="" class="button-text relatedDoctorCard_anchor">
                                <div class="relatedDoctorImage_container">
                                    <img src="http://www.sepeb.com/profile-pictures/profile-pictures-009.jpg"
                                         class="relatedDoctorCard_profile-pic" alt="">
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-7 relatedDoctorCard_info">
                            <a href="javascript:void(0)" class="relatedDoctorCard_doctor-link">Dr.susant shrestha</a>
                            <span class="relatedDoctorCard_degree caps">mbbs,md</span>
                        </div>
                        <div class="col-xs-3 text-right">
                            <div class="relatedDoctorCard_rate">
                                <span>4.4</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>




                <div class="text-center ConsultDoctor__container">
                    <div >
                        <a class="button-text" href="#">
                            <img src="https://1mgstaticfiles.s3.amazonaws.com/skuicons/consult_doctor_online.png" alt="consult doctor" class="ConsultDoctor__consult-image" >
                        </a>
                    </div>
                    <div class="ConsultDoctor__content">
                        <a class="button-text btn btn-secondary ConsultDoctor__link" href="doctor.php">CONSULT A DOCTOR ONLINE
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="viewall" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><div class="substitute_sub-heading">Substitutes for {{ $product->title }}</div></h4>
                </div>
                <div class="modal-body">

                    <div class="substitute_numbers" style="margin-bottom:10px">{{ $count }} Substitutes found</div>
                    <table class="table table-hover">
                        <thead>
                        <tr >
                            <th>Name</th>
                            <th>MRP</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($substitute_product as $alernative_product)    
                        <tr>
                            <td>
                                <a href="#" class="item-name non-uppercase drug-view-all-subsitute">
                                   {{ $alernative_product->title }}
                                </a>
                                <div class="text-small">10 tablets</div>
                                <div class="item-manufacturer">{{ $alernative_product->manufacurer }}</div>
                            </td>
                            <td>
                                <div class="item-price">{{ $alernative_product->price }}</div>
                                <div class="item-save">save&nbsp;81% more per Tablet</div>
                            </td>
                        </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
 <script type="text/javascript">
    $(document).ready(function(){
       $('.reduce-qty').click(function(){

            var qty = $('#qty').text();
            qty--;
            $('#qty').text(qty);

       });

       $('.add-qty').click(function(){

            var qty = $('#qty').text();
            qty++;
            $('#qty').text(qty);

       });

       $('#addToCart').click(function(){

        var qty = $('#qty').text();
        var id = $(this).attr('data-id');
        //console.log(id);

        $.ajax({
            url:"/single-add",
            type:"POST",
            data:{'qty':qty,'id':id,'_token':'<?php echo csrf_token() ?>'},
            datatType : 'json',
            success:function(data){
                //console.log(data);
                if(data.success=='success'){
                    $('#addToCart').text('Added');
                    $('.Iitems-count').text(data.cartCount);
                        setTimeout(function(){
                    $('#addToCart').text('Add To Cart');
                    }, 1000);

                    }
            }
        })
       })
    });
    
    </script>

@endsection
