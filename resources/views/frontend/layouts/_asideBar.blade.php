<aside class="main-sidebar navbar-collapse collapse" id="sidebar">
<!--	<div class="logo-box hidden-xs">-->
<!--		<a class="navbar-brand" href="index.php">-->
<!--			<img src="design/assets/images/logos.png" alt="Swasthya Nepal">-->
<!--		</a>-->
<!--	</div>-->
	<ul class="sidebar-menu">

                  <!-- GPS ICONS       -->
		<li class="slide-out">
			<a href="{{ route('medicine-categories') }}">
                <img src="{{ asset('frontend/design/assets/images/tablets.png') }}" alt=""  > Medicine
			</a>
		</li>
		<li class="slide-out">
			<a href="{{ route('hospital') }}">
                <img src="{{ asset('frontend/design/assets/images/hospital2.png') }}" alt=""  > Hospital
			</a>
		</li>
		<li class="slide-out">
			<a href="{{ route('labtest') }}">
                <img src="{{ asset('frontend/design/assets/images/microscope.png') }}" alt=""  > Lab Tests
			</a>
		</li>
		<li class="slide-out">
			<a href="{{ route('doctor') }}">
                <img src="{{ asset('frontend/design/assets/images/doctor.png') }}" alt=""  > Doctors
			</a>
		</li>
        <li class="slide-out">
			<a href="{{ route('article') }}">
                <img src="{{ asset('frontend/design/assets/images/text-lines.png') }}" alt=""  > Articles
			</a>
		</li>
        <li class="slide-out">
			<a href="{{ route('forum') }}">
                <img src="{{ asset('frontend/design/assets/images/chat.png') }}" alt=""  > Forum
			</a>
		</li>
	</ul>
</aside>
