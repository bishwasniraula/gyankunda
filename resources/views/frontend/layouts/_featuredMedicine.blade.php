<section class="featured-medicine-wrapper section">
    <div class="section-body">
        <h2>Featured Medicines</h2>
        <div class="owl-carousel owl-theme">

        @forelse($products as $product)
            <div class="item">
                <div class="image-thumbnail">
                    <img src="{{ asset('swasthya/products/' . $product->cover_photo) }}" alt="{{ $product->title }}">
                </div>
                <div class="item-footer text-center">
                    <h5>{{ $product->title }}</h5>
                    <p>
                        <span class="price-item">Rs.{{ $product->price }}</span>
                        <small class="default-price text-danger">Rs.{{ $product->selling_price }}</small>
                    </p>

                    <a id="f-{{ $product->id }}" data-id="{{ $product->id }}" class="btn btn-danger addToCartBtn addToCart">
                        Add To Cart
                    </a>
                    <!-- <button class="btn btn-block addToCartBtn add-to-cart"  type="button" data-name="himalayan diabeticon" data-price="255">Add to Cart</button> -->
                </div>
            </div>

        @empty
            <p>No featured medicine found !!</p>
        @endforelse

        </div>
    </div>
</section>

@section('script')
 <script type="text/javascript">

    $(document).ready(function(){

       $('.addToCart').click(function(){

        var qty = 1;
        var id = $(this).attr('data-id');
        
        $.ajax({
            url:"/single-add",
            type:"POST",
            data:{'qty':qty,'id':id,'_token':'<?php echo csrf_token() ?>'},
            datatType : 'json',
            success:function(data){
               
                if(data.success=='success'){

                    $('#f-'+id).text('Added');
                    $('.Iitems-count').text(data.cartCount);
                                        setTimeout(function(){
                    $('#f-'+id).text('Add To Cart');
                    }, 500);

                            }
            }
        })
       })
    });
    
    </script>

@endsection

