
<script src="{{ asset('frontend/design/vendor/jquery-3.2.1.min.js') }}"></script>
<!-- <script src="{{ asset('frontend/source/js/cart.js') }}"></script> -->
<script src="{{ asset('frontend/design/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/design/vendor/OwlCarousel2-2.2.1/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/design/vendor/slick-slider/slick/slick.js') }}"></script>

<script src="{{ asset('frontend/source/js/scripts.js') }}"></script>
<script src="{{ asset('frontend/source/js/custon_js.js') }}"></script>

<!-- <script src="{{ asset('frontend/source/js/maincartsystem.js') }}"></script> -->


<script src="{{ asset('frontend/source/js/testimonials.js') }}"></script>

<script>
    $(document).ready(function () {
        $(document).on('click', '.navbar-toggle', function (event) {
            event.preventDefault();
            $('body').toggleClass('open');
        });
    });
</script>
@yield('script')
