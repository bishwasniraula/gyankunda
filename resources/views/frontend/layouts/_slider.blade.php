<section class="main-slider-wrapper section" id="bg">

	@if(session()->has('success'))
    <span class="placed product btn btn-success" style="position: absolute;
    z-index: 5;
    left: 340px;
    font-family: sans-serif;
    font-size: 16px;
    box-shadow: 2px 2px black;
    top: 10px;">

  {{ session()->get('success') }}

    @endif
    </span>


    
	<div class="main-slider">
		<div class="item">
			<img src="{{ asset('frontend/design/assets/images/slider1.png') }}" alt="">
			<div class="slider-content-wrap">
				<div class="slider-mid">
					<div class="slider-content">
						<h1>Order with prescription & <br> get upto 20% discount</h1>
						<a href="#" class="btn btn-warning btn-lg" tabindex="0">Upload</a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<img src="{{ asset('frontend/design/assets/images/slider2.png') }}" alt="">
			<div class="slider-content-wrap">
				<div class="slider-mid">
					<div class="slider-content">
						<h1>Interactively create leading-edge <br> core competencie</h1>
						<a href="#" class="btn btn-warning btn-lg" tabindex="0">Make Appointment</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>