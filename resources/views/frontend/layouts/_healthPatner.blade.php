<section class="featured-medicine-wrapper section">
	<div class="section-body">
		<h2>Health Patners</h2>
		<div class="owl-carousel owl-theme">
			<div class="item">
				<div class="image-thumbnail">
					<img src="{{ asset('frontend/design/assets/images/Vidant.gif') }}" alt="">
				</div>
			</div>
			<div class="item">
				<div class="image-thumbnail">
					<img src="{{ asset('frontend/design/assets/images/heal.jpg') }}" alt="">
				</div>
			</div>
			<div class="item">
				<div class="image-thumbnail">
					<img src="{{ asset('frontend/design/assets/images/unicef.jpg') }}" alt="">
				</div>
			</div>
			<div class="item">
				<div class="image-thumbnail">
					<img src="{{ asset('frontend/design/assets/images/HP1.jpg') }}" alt="">
				</div>
			</div>
			<div class="item">
				<div class="image-thumbnail">
					<img src="{{ asset('frontend/design/assets/images/HP1.jpg') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>