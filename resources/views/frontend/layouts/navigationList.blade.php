
@foreach($product_categories_menus as $menu)

    @if($menu->categories->isNotEmpty())
    <li class="general common mega-menu">
        <a class="blue active" href="#" title="{{$menu->title}}">{{$menu->title}}</a>
        <ul class="block">
            <div class="menu-sub">
                @foreach($menu->categories as $key =>$category)
                    @if($category->inner_categories->isNotEmpty())
                        @if(($key+1)%2 == 1)
                        <div class="menu-col-1">
                        @endif
                        <h3 class="menu-category">{{ $category->title}}</h3>
                        <ul>
                            
                                @foreach($category->inner_categories as $inner_category)
                                    <li><a href="">{{$inner_category->title}}</a></li>

                                @endforeach
                            
                        </ul>
                        @if(($key+1)%2 ==0 )
                        </div>
                        @endif
                    @endif
                @endforeach
                
                
            </div>
        </ul>
    </li>

    @else

    <li><a class="" href="#"  title="{{$menu->title}}">{{$menu->title}}</a></li>

    @endif


@endforeach

