<aside class="main-sidebar navbar-collapse collapse" id="sidebar">
	<ul class="sidebar-menu">
        <li class="slide-out">
            <a href="{{ route('product.index') }}">
                <span class="lnr lnr-briefcase"></span> Product status
            </a>
        </li>
        <li class="slide-out">
         <a href="{{ route('userstatus') }}">
                <span class="lnr lnr-apartment"></span> User status
            </a>
        </li>
        <li class="slide-out">
        <a href="{{ route('product.create') }}">
        <span class="lnr lnr-briefcase"></span> Add Product
        </a>
        </li>
        <li class="slide-out">
        <a href="{{ route('add-product-category.index') }}">
        <span class="lnr lnr-apartment"></span>Product category
        </a>
        </li>
        <li class="slide-out">
        <a href="{{ route('order.index') }}">
        <span class="lnr lnr-cart"></span>Orders
        </a>
        </li>
	</ul>
</aside>
