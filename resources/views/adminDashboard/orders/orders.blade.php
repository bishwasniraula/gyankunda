@extends('adminDashboard.layouts.master')

@section('title', "Swasthya | Orders")

@section('main-content')

<div class="container" style="background:#ffffff;margin-top:10px;margin-bottom:10px;min-height: 600px;">
    <div class="col-md-12 col-sm-12 col-xs-12">
      @include('includes._messages')
      <div class="x_panel">
        <br>
        <h2><center>ORDERS LIST</center></h2>
        <div>
          <BR>
        <?php $carts = ''; $arr = ''; $c='' ?>
    
     @forelse ($orders as $order)
        <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Order Detail <em>({{ $order->email }})</em></h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <span>Payment Method <strong>{{ $order->payment_method }}</strong></span>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <table class="table table-striped table-bordered table-list">
                  <thead>
                    <tr>
                        <th>s.n</th>
                        <th>Product_code</th>
                        <th class="hidden-xs">Product Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                    </tr> 
                  </thead>
                  <tbody>
                    <?php                             
                     $arr = $order->cart;
                    $carts = json_decode($arr, TRUE);              
                    foreach($carts as $key=> $c){
                      $k = $key+1;

                        echo '<tr>';

                        echo '<td>'.$k.'</td>';
                        echo '<td>'.$c['pid'].'</td>';
                        echo '<td>'.$c['ptitle'].'</td>';
                        echo '<td>Rs. '.$c['price'].'</td>';
                        echo '<td>'.$c['qty'].'</td>';
                        echo '<td>Rs. '.$c['totalprice'].'</td>';

                        echo '</tr>';



                     }?>
                        </tbody>
                        <tr>
                        <td><strong>Total Discount</strong></td>
                        <td colspan="5" class="text-right"><strong>Rs. {{ $order->total_discount }} </strong></td>
                        
                        </tr>
                        <tr>
                        <td><strong>Total Amount</strong></td>
                        <td colspan="5" class="text-right"><strong>Rs. {{ $order->total_amount  }} </strong></td>
                        </tr>
                </table>
            
              </div>
              <div class="panel-footer">
                <div class="row">
    <div class="col col-xs-4">{{ $order->created_at }}</div>
                </div>
              </div>
            </div>

            @empty
                <p>Sorry! No orders have been made yet !</p>
            @endforelse

            <div class="col-md-12 text-right">
      {{ $orders->links() }}
    </div>
        

    </div>
      </div> 
  </div>
</div>
@endsection