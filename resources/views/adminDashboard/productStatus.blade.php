@extends('adminDashboard.layouts.master')
@section('individual-header')

    <link type="text/css" href="{{ asset('frontend/css/serviceProviderDashboard/product/products.css') }}"/>

@endsection
@section('title', "Swasthya | Product Status")
@section('main-content')

@include('includes._messages')

<div class="container" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
<h2 style="margin: 10px 0;">Products status</h2>
            <hr>
    <div>
        
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    
                    <li role="presentation" class="active"><a href="#activeProduct" aria-controls="activeProduct" role="tab" data-toggle="tab">Active Products</a></li>
                    <li role="presentation"><a href="#suspendedProduct" aria-controls="suspendedProduct" role="tab" data-toggle="tab">Suspended Products</a></li>
                    <li role="presentation"><a href="#pendingProduct" aria-controls="pendingProduct" role="tab" data-toggle="tab">Pending Products</a></li>
                    <li role="presentation"><a href="#allProduct" aria-controls="allProduct" role="tab" data-toggle="tab">All Products</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="activeProduct">
                        <!--                        table content-->
                        <div class="table-responsive" data-pattern="priority-columns" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
                            <table class="productstatus table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Product Id</th>
                                    <th>Product Name</th>
                                    <th data-priority="3">Current Inventory</th>
                                    <th data-priority="1">Product owner</th>
                                    <th data-priority="5">Role</th>
                                    <th data-priority="2">Email</th>
                                    <th data-priority="4">action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($ActiveProducts as $active)
                                    <tr id="data-{{ $active->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $active->title }}</td>
                                    <td>{{ $active->quantity }}</td>
                                    <td>{{ $active->distributor  }}</td>
                                    <td><span class="label label-success">service provider</span></td>
                                    <td>susantshrest@hotmail.com</td>
                                    <td>
                                        <a href="/admin/product/{{ $active->id }}/edit" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></a>
                                        <a href="/admin/product/{{ $active->id }}" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye font-14"></i></a>

                                        <button class="btn btn-default btn-sm btn-flat delete" data-id="{{ $active->id }}"><i class="fa fa-trash font-14"></i></button>
                                    </td>
                                </tr>
                                @empty
                                    <p>No Active product !!</p>
                                @endforelse
                                </tbody>
                            </table>
                        </div><!--end of .table-responsive-->
                    </div>
                    <div role="tabpanel" class="tab-pane" id="suspendedProduct">
                        <!--                        table content-->
                        <div class="table-responsive" data-pattern="priority-columns" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
                            <table class="productstatus table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Product Id</th>
                                    <th>Product Name</th>
                                    <th data-priority="3">Current Inventory</th>
                                    <th data-priority="1">Product owner</th>
                                    <th data-priority="5">Role</th>
                                    <th data-priority="2">Email</th>
                                    <th data-priority="4">action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($SuspendProducts as $suspend)
                                    <tr id="suspend-{{ $suspend->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $suspend->title }}</td>
                                    <td>{{ $suspend->quantity  }}</td>
                                    <td>{{ $suspend->distributor  }}</td>
                                    <td><span class="label label-success">service provider</span></td>
                                    <td>susantshrest@hotmail.com</td>
                                    <td>
                                        <a href="/admin/product/{{ $suspend->id }}/edit" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></a>

                                        <a href="/admin/product/{{ $suspend->id }}" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye font-14"></i></a>

                                        <button class="btn btn-default btn-sm btn-flat delete" data-id="{{ $suspend->id }}"><i class="fa fa-trash font-14"></i></button>
                                    </td>
                                </tr>
                                @empty
                                    <p>No Suspend product !!</p>
                                @endforelse
                                </tbody>
                            </table>
                        </div><!--end of .table-responsive-->

                    </div>
                    <div role="tabpanel" class="tab-pane" id="pendingProduct">
                        <!--table content-->
                        <div class="table-responsive" data-pattern="priority-columns" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
                            <table class="productstatus table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Product Id</th>
                                    <th>Product Name</th>
                                    <th data-priority="1">Product owner</th>
                                    <th data-priority="5">Role</th>
                                    <th data-priority="2">Email</th>
                                    <th data-priority="3">Description</th>
                                    <th data-priority="4">action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>zandu</td>
                                    <td>susant shrestha</td>
                                    <td><span class="label label-success">service provider</span></td>
                                    <td>susantshrest@hotmail.com</td>
                                    <td>this is the description place</td>
                                    <td>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></button>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash font-14"></i></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!--end of .table-responsive-->

                    </div>
                    <div role="tabpanel" class="tab-pane" id="allProduct">
                        <!--table content-->
                        <div class="table-responsive" data-pattern="priority-columns" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
                            <table  class="productstatus table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Product Id</th>
                                    <th>Product Name</th>
                                    <th data-priority="3">Current Inventory</th>
                                    <th data-priority="1">Product owner</th>
                                    <th data-priority="5">Role</th>
                                    <th data-priority="2">Email</th>
                                    <th data-priority="4">action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    
                                @forelse($AllProducts as $product)
                                    <tr id="allpro-{{ $product->id }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $product->title }}</td>
                                    <td>{{ $product->quantity  }}</td>
                                    <td>{{ $product->distributor  }}</td>
                                    <td><span class="label label-success">service provider</span></td>
                                    <td>susantshrest@hotmail.com</td>
                                    <td>
                                        <a href="/admin/product/{{ $product->id }}/edit" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></a>
                                        <a href="/admin/product/{{ $product->id }}" class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye font-14"></i></a>
                                        <button class="btn btn-default btn-sm btn-flat delete" data-id="{{ $product->id }}"><i class="fa fa-trash font-14"></i></button>
                                    </td>
                                </tr>
                                @empty
                                    <p>No product found !!</p>
                                @endforelse
                                
                                </tbody>
                            </table>
                        </div><!--end of .table-responsive-->

                    </div>

                </div>

           </div> 
    </div>
@endsection

@section('individual-footer')
    <script>
    $(document).ready(function() {
        $('.productstatus').DataTable();

        $('.delete').on('click', function(){

            var id = $(this).data('id');

            $.ajax({
                url:'/delete-product',
                type:'GET',
                data:{'id':id,'_token':'<?php echo csrf_token() ?>'},
                success:function(response){

                   $("#data-"+id).fadeOut('slow', function () {
                        $(this).remove();
                    });
                   $("#suspend-"+id).fadeOut('slow', function () {
                        $(this).remove();
                    });
                   $("#allpro-"+id).fadeOut('slow', function () {
                        $(this).remove();
                    });
                }
            })

        });
    } );
</script>
@endsection