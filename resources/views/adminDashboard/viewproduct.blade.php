@extends('adminDashboard.layouts.master')
@section('title', "Admin | Dashboard")
@section('main-content')

    <div class="container" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
        <h2 style="margin: 10px 0;">{{ $product->title }} Information</h2>
    <hr>
        <div>
            
                          <div class="col-sm-6">
                              <div class="card">
                                  <address>
                            <strong>Product Title:</strong> <span>{{ $product->title }}</span>
                              <br><strong>Product Quantity:</strong> <span>{{ $product->quantity }}</span>
                              <br><strong>Product Price:</strong> <span>{{ $product->price }}</span>
                              <br><strong>Selling Price:</strong> <span>{{ $product->selling_price }}</span>
                              <br><strong>Expiry Date:</strong> <span>{{ $product->expiry_date }}</span>
                              <br><strong>Manufactured by:</strong> <span>{{ $product->manufacturing }}</span>
                              <br><strong>Brand:</strong> <span>{{ $product->brand }}</span>
                              <br><strong>Distributor:</strong> <span>{{ $product->distributor }}</span>
                              </address>
                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="card text-right">
                            <address>
                                <strong>Published :</strong>
                                @if($product->published==0)
                                    <span>Unpublished</span>
                                @else
                                <span>Published</span>
                                @endif
                                    <br>
                                <strong>Featured :</strong>
                                @if($product->featured==0)
                                    <span>Not featured</span>
                                @else
                                <span>Featured</span>
                                @endif
                                

                              </address>
                              </div>
                          </div>


                        <div class="row">
                          <div class="col-xs-12">
                            <h4><strong>Product Image</strong></h4>
                            <img src="{{ asset('swasthya/products/'.$product->cover_photo) }}" alt="image-preview" class="img-responsive">
                          </div>
                      </div>

                      <!-- Desctiption row -->
                      <div class="row">
                          <div class="col-xs-12">
                            <h4><strong>Desctioption</strong></h4>
                            <p>{{ strip_tags($product->description) }}</p>
                          </div>
                      </div>
                      <!-- /.row -->
                  
                      <!-- salt composition row -->
                      <div class="row">
                        
                          <div class="col-xs-12">
                            <h4><strong>Salt Composition</strong></h4>
                            <p>{{ strip_tags($product->salt_composition) }}</p>
                          </div>
                      
                      </div>

                      <!-- basic row -->
                      <div class="row">
                        
                          <div class="col-xs-12">
                            <h4><strong>Basic use</strong></h4>
                            <p>{{ strip_tags($product->basic_use) }}</p>
                          </div>
                      
                      </div>
                      <!-- side effect row -->
                      <div class="row">
                        
                          <div class="col-xs-12">
                            <h4><strong>Side Effect</strong></h4>
                            <p>{{ strip_tags($product->side_effect) }}</p>
                          </div>
                      
                      </div>
                      <!-- how to use row -->
                      <div class="row">
                        
                          <div class="col-xs-12">
                            <h4><strong>How to use</strong></h4>
                            <p>{{ strip_tags($product->how_to_use) }}</p>
                          </div>
                      
                      </div>
                      <!-- /.row -->
        </div> 
    </div>
@endsection