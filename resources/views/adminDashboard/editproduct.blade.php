@extends('adminDashboard.layouts.master')

@section('individual-header')

    <link type="text/css" href="{{ asset('frontend/css/serviceProviderDashboard/product/products.css') }}"/>
    <style>
        sup{
            background: #ff0d0d;
            padding:1px 3px 1px 3px;
            color: #fff;
            cursor: pointer;
            font-size: 10px;
            top: -1.5em;
            border-radius: 15px;
        }
    </style>

@endsection
@section('title', "Admin | Edit- $product->title")
@section('main-content')

        <!--add  product-->
        <div class="container-fluid ">

            <div class="row add-product-container" style="display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;margin: 20px;">
                <div class="col-sm-8">
                    <h3 style="background: #024b68;color:#ffffff;padding:10px 20px;text-align:center">Edit product</h3>
                    <form class="form-horizontal" method="POST" style="background: #ffffff;padding: 20px;" action="/admin/product/{{ $product->id }}"" enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <fieldset>
                            <div class="col-sm-12 form-group margin50">
                                <label class="col-lg-3"  for="ProductName">Product Name</label>
                                <div class="col-sm-4">
                                    <input type="text" id="name" name="ProductName" class="form-control name" value="{{ $product->title }}" required>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group margin50">
                                <label class="col-lg-3"  for="slug">Slug</label>
                                <div class="col-sm-4">
                                    <input type="text" id="slug" name="slug" class="form-control slug" value="{{ $product->slug }}" required >
                                </div>
                            </div>

                            <div class=" col-sm-12 form-group">
                                <label class="col-sm-3" for="ProductType">Product Type</label>
                                <div class="col-sm-4">
                                    <select id="productType" name="ProductType" class="form-control product-type" required>
                                    @forelse($categories as $category)
                                        <option value="{{ $category->id }}" {{ $product->category_id==$category->id ? 'selected' :''  }}>{{ $category->category_name }}</option>
                                        @empty
                                        @endforelse 
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Manufacturer">Manufacturer</label>
                                <div class="col-sm-4">
                                    <select id="manufacturer" name="manufacturer" class="form-control manufacturer" required>
                                        <option value="Manufacturer A" {{ $product->manufacturing =='' ? 'selected' :'Manufacturer A'  }}>Manufacturer A</option>
                                        <option value="Manufacturer B" {{ $product->manufacturing =='Manufacturer B' ? 'selected' :''  }}>Manufacturer B</option>
                                        <option value="Manufacturer C" {{ $product->manufacturing =='Manufacturer C' ? 'selected' :''  }}>Manufacturer C</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Distributor">Distributor</label>
                                <div class="col-sm-4">
                                    <select id="distributor" name="distributor" class="form-control distributor" required>
                                       
                                        <option value="Distributor A" {{ $product->distributor =='Distributor A' ? 'selected' :''  }}>Distributor A</option>
                                        <option value="Distributor B" {{ $product->distributor =='Distributor B' ? 'selected' :''  }}>Distributor B</option>
                                        <option value="Distributor C" {{ $product->distributor =='Distributor C' ? 'selected' :''  }}>Distributor C</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="SKU">SKU</label>
                                <div class="col-sm-4">
                                    <input type="text" id="sku" name="SKU" value=" {{ $product->sku }}" class="form-control sku" required >
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="ManufacturerPart">Manufacturer Part</label>
                                <div class="col-sm-4">
                                    <input type="text" id="manufacturerPart" name="manufacturerPart" class="form-control manufacturer-part" value="{{ $product->manufacturer_part }}">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="Published">Published</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="Published" value="0" class="input-xlarge"  {{ $product->published =='0' ? 'checked' :''  }}><span>No</span>
                                    <input type="radio"  name="Published" value="1" class="input-xlarge" {{ $product->published =='1' ? 'checked' :''  }}><span>Yes</span>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="Featured">Featured</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="Featured" class="input-xlarge" value="0" {{ $product->featured  =='0' ? 'checked' :''  }}><span>No</span>
                                    <input type="radio"  name="Featured" class="input-xlarge" value="1" {{ $product->featured  =='1' ? 'checked' :''  }}><span>Yes</span>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="PageDisplay">Page Display</label>
                                <div class="col-sm-4">
                                    <select id="pageDisplay" name="PageDisplay" class="form-control page-display" required>
                                        <option value="page display-1" {{ $product->page_display =='page display-1' ? 'selected' :'' }}>page display-1</option>
                                        <option value="page display-2" {{ $product->page_display =='page display-2' ? 'selected' :'' }}>page display-2</option>
                                        <option value="page display-3" {{ $product->page_display =='page display-3' ? 'selected' :'' }}>page display-3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-lg-3" for="TaxClass">Tax Class</label>
                                <div class="col-lg-4">
                                    <select id="taxClass" name="TaxClass" class="form-control tax-class">
                                        <option value="1"  {{ $product->tax_class =='1' ? 'selected' :'' }}>Tax-1</option>
                                        <option value="2" {{ $product->tax_class =='2' ? 'selected' :'' }}>Tax-2</option>
                                        <option value="3" {{ $product->tax_class =='3' ? 'selected' :'' }}>Tax-3</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="QuantityDiscountTable">Quantity Discount Table</label>
                                <div class="col-sm-4">
                                    <select id="quantityDiscount" name="QuantityDiscountTable" class="form-control quantity-discount">
                                        <option value="1" {{ $product->qauantity_discount_table  =='1' ? 'selected' :'' }}>Quantity Discount-1</option>
                                        <option value="2" {{ $product->qauantity_discount_table  =='2' ? 'selected' :'' }}>Quantity Discount-2</option>
                                        <option value="3" {{ $product->     qauantity_discount_table  =='3' ? 'selected' :'' }}>Quantity Discount-3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="ShowBuyButton">Show Buy Button</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="ShowBuyButton" class="input-xlarge" value="0" {{ $product->show_buy_button =='0' ? 'checked' :''  }}><span>No</span>
                                    <input type="radio"  name="ShowBuyButton" class="input-xlarge" value="1" {{ $product->show_buy_button =='1' ? 'checked' :''  }}><span>Yes</span>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="RequiresRegistrationToView">Requires Registration To View</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="RequiresRegistrationToView" checked class="input-xlarge" value="0" {{ $product->     requires_registration_view=='0' ? 'checked' :''  }}><span>No</span>
                                    <input type="radio"  name="RequiresRegistrationToView"  class="input-xlarge" value="1" {{ $product->    requires_registration_view=='1' ? 'checked' :''  }}><span>Yes</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="HidePriceUntilCart">Hide Price Until Cart</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="HidePriceUntilCart" checked class="input-xlarge" value="0" {{ $product->    hide_price_until_cart =='0' ? 'checked' :''  }}><span>No</span>
                                    <input type="radio"  name="HidePriceUntilCart"  class="input-xlarge" value="1" {{ $product->hide_price_until_cart=='1' ? 'checked' :''  }}><span>Yes</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="Inventory">Track Inventory By Size and Color</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="Inventory" checked class="input-xlarge" value="0" {{ $product->track_inventory_by_size_and_color=='0' ? 'checked' :''  }}><span>No</span>
                                    <input type="radio"  name="Inventory" class="input-xlarge" value="1" {{ $product->track_inventory_by_size_and_color=='1' ? 'checked' :''  }}><span>Yes</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="SizeOptionPrompt">Size Option Prompt</label>
                                <div class="col-sm-4">
                                    <input type="text" id="sizeOptionPrompt" name="SizeOptionPrompt" placeholder="" class="form-control size-option-prompt" value="{{ $product->size_option }}">
                                </div>
                            </div>
                            
                            <h3 style="padding:10px 20px;text-align:center;text-decoration: underline">Default Variant Information</h3>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="description">Description</label>
                                <div class="col-sm-9">
                                    <textarea name="description" id="description" rows="10" rows="10">{{ $product->description }}</textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="composition">Salt Composition</label>
                                <div class="col-sm-9">
                                    <textarea name="composition" id="composition" rows="10" rows="10">{{ $product->salt_composition }}</textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="basicUse">Primarly used for</label>
                                <div class="col-sm-9">
                                    <textarea name="basicUse" id="basicUse" rows="10" rows="10">
                                        {{ $product->basic_use }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="sideEffect">Side effect</label>
                                <div class="col-sm-9">
                                    <textarea name="sideEffect" id="sideEffect" rows="10" rows="10">
                                        {{ $product->side_effect  }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="use">How to use</label>
                                <div class="col-sm-9">
                                    <textarea name="use" id="use" rows="10" rows="10">{{ $product->  how_to_use }}</textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Price">Situations & States</label>
                                <div class="col-sm-9">
                                    <div class="row" id="pandsgrp">
                                        <div class="col-sm-12">
                                                                                                                
                                                @if(!empty($product->situations_states ))
                                                    
                                                <?php 
                                                    $data = json_decode($product->situations_states, true);

                                                    foreach($data as $key=> $value){
                                                        $key = $key+1;

                                                        echo '<div class="row " id="s-'.$key.'"><div style="margin-top:10px 0 10px 0;">
            <div class="col-sm-6">
                <input type="text" id="sit-'.$key.'" name="situation'.$key.'" placeholder="Situation" class="form-control price" value="'.$value['situation'].'" required >
            </div>';
            ?>

            <div class="col-sm-6" style="margin:6px 0;">
                <label class="col-sm-4" for="state">State</label>
                <label class="radio-inline" style="margin-top: -7px;">
                    <input type="radio" name="radiogrp<?php echo $key;?>" value="1" <?php  echo $value['state']==1 ? 'checked':''?>>Safe
                </label>
                <label class="radio-inline" style="margin-top: -7px;">
                    <input type="radio" name="radiogrp<?php echo $key;?>" value="0" <?php  echo $value['state']==0 ? 'checked':''?>>Unsafe</label>
                    <span class="text-right delete" data-id="<?php echo "c-".$key;?>" ><sup>X</sup></span>
            </div>

        </div></div>
                               <?php                         
                                                    }
                                                    $a = $key+1;
                                                    echo '<button id="add-situation-state" type="button" class="btn btn-sm btn-info" data-counter="'.$a.'" style="margin-bottom:10px;">Add Situations & States</button>';
                                                       
                                                ?>

                                                @else
                                                    <button id="add-situation-state" type="button" class="btn btn-sm btn-info" data-counter="1" style="margin-bottom:10px;">Add Situations & States</button>
                                                @endif
                                         
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Price">Image upload</label>
                                <div class="col-sm-9">
                                    <div class="page-content fade-in-up">
                                        <div class="ibox">
                                            <div class="ibox-body">
                                                <input name="coverPhoto" type="file" multiple="multiple">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Price">Price</label>
                                <div class="col-sm-4">
                                    <input type="text" id="price" name="price" class="form-control price" value="{{ $product->price }}" required  >
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="SalePrice">Sale Price</label>
                                <div class="col-sm-4">
                                    <input type="text" id="saleprice" name="SalePrice" class="form-control sale-price" value="{{ $product->selling_price }}" required > 
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="exp-date">Expiry Date</label>
                                <div class="col-sm-4">
                                    <input type="date" id="expdate" name="expdate" class="form-control expdate" value="{{ $product->expiry_date }}"required >
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Weight">Weight</label>
                                <div class="col-sm-4">
                                    <input type="text" id="weight" name="Weight" class="form-control weight" value="{{ $product->weight }}">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Dimentions">Dimentions(Width x Height X Depth)</label>
                                <div class="col-lg-10">
                                    <div class="col-lg-3"><input type="text" id="dimentionsWidth" name="DimentionsWidth" placeholder="" class="form-control dimentions-width" value="{{ $product->dimention_width }}">  X</div>
                                    <div class="col-lg-3"><input type="text" id="dimentionsHeight" name="DimentionsHeight" placeholder="" class="form-control dimentions-height" value="{{ $product->dimention_height }}">  X</div>
                                    <div class="col-lg-3"><input type="text" id="dimentionsDepth" name="DimentionsDepth" placeholder="" class="form-control dimentions-depth" value="{{ $product->dimention_depth }}"></div>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="CurrentInventory">Current Inventory</label>
                                <div class="col-sm-4">
                                    <input type="number" id="currentInventory" name="qty" placeholder="" class="form-control current-inventory" value="{{ $product->quantity }}" required >
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Colors">Colors</label>
                                <div class="col-sm-4">
                                    <input type="text" id="colors" name="Colors" placeholder="" class="form-control colors" value="{{ $product->colors }}">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="ColorSKUModifier">Color SKU Modifiers</label>
                                <div class="col-sm-4">
                                    <input type="text" id="colorModifier" name="ColorSKUModifier" placeholder="" class="form-control color-modifier" value="{{ $product->    colors_sku_modifiers }}">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Sizes">Sizes</label>
                                <div class="col-sm-4">
                                    <input type="text" id="sizes" name="Sizes" placeholder="" class="form-control sizes" value="{{ $product->sizes }}">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="SizeSKUModifier">Size SKU Modifiers</label>
                                <div class="col-sm-4">
                                    <input type="text" id="sizeModifier" name="SizeSKUModifier" placeholder="" class="form-control size-modifier" value="{{ $product->size_sku_modifiers }}">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group" style="display: flex;justify-content:center">
                                <div class="col-lg-6">
                                    <input type="submit" id="submitForm" name="submit" class="form-control btn btn-primary">
                                </div>
                            </div>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>
        <!--/add product-->
    
@endsection

@section('individual-footer')

<script>
    $(document).ready(function(){

        $('#add-situation-state').on('click',function(){
            var counter = $(this).data('counter');

           if(counter>10){
            alert('Only 10 SITUATION & STATE allow !');
            return false;
           }

           var newStnState;

                newStnState = ('<div class="row" id="s-'+counter+'"><div style="margin-top:10px 0 10px 0;"><div class="col-sm-6"><input type="text" id="sit-'+counter+'" name="situation'+counter+'" placeholder="Situation" class="form-control price" required autofocus></div><div class="col-sm-6" style="margin:6px 0;"><label class="col-sm-4" for="state">State</label><label class="radio-inline" style="margin-top: -7px;"><input type="radio" checked name="radiogrp'+counter+'" value="1">Safe</label><label class="radio-inline" style="margin-top: -7px;"><input type="radio" name="radiogrp'+counter+'" value="0">Unsafe</label><span class="text-right delete" data-id="c-'+counter+'"><sup style="margin-left: 6px;">X</sup></span></div></div></div>');

                

                $('#add-situation-state').before(newStnState);
                counter++;
                $(this).data('counter',counter++);

                $('.delete').click(function(){

                    var cid = $(this).data('id');
                    var id = cid.split('c-').join('');

                    $('#s-'+id).fadeOut('fast', function () {
                        $(this).remove();
                    });
                    
                });
            });

       $('.delete').click(function(id){

            var cid = $(this).data('id');
            var id = cid.split('c-').join('');

        $('#s-'+id).fadeOut('fast', function () {
                        $(this).remove();
                    });

       });
       
    });
</script>

@endsection


