@extends('adminDashboard.layouts.master')
@section('individual-header')

 <link type="text/css" href="{{ asset('frontend/css/serviceProviderDashboard/product/products.css') }}"/>
 <style>
 	.category-image {
    position: relative;
    margin-top: 15px;
    margin-bottom: 15px;
	}
	.category-image img {
    max-width: 100%;
    vertical-align: middle;
    border: 0;
	}
	.category-image .category-name {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    bottom: 0;
    width: 100%;
    height: 20%;
    position: absolute;
    font-size: 1.5rem;
    line-height: 2.2rem;
    color: #fff;
    background: #024b68;
    letter-spacing: -0.2px;
    opacity: 0.8;
}
sup{
	top: 0.5em;
padding: 0px 5px 0px 5px;
background: red;
border-radius: 11px;
font-size: 11px;
color: #fff;
cursor: pointer;
}
 </style>
}

@endsection
@section('title', "Swasthya | Add product category")
@section('main-content')

@include('includes._messages')

	<div class="container-fluid ">
        <div class="row add-product-container" style="margin: 20px;">
        	<div class="col-sm-6 hidden-xs">
				<h3 style="background: #024b68;color:#ffffff;padding:10px 20px;text-align:center">All product categories</h3>
				<div class="col-sm-12" style="background: #ffffff;padding: 20px;">
					@forelse($categories as $category)
					<div class="col-xlg-2 col-md-4 col-sm-5 col-xs-12" id="cat-{{ $category->id }}">

                        <div class="category-wrap text-center individual-category">
                            <div class="category-image">
                            	<sup class="delete" data-id="{{ $category->id }}" data-name="{{ $category->category_name }}">x</sup>
                                <img src="{{ asset('swasthya/products-category/'.$category->cover_photo) }}">
                                    <div class="category-name">{{ $category->category_name }}</div>
                            </div>
                        </div>
                	</div>
                	@empty
                	<p>No product category found !!</p>
                	@endforelse
			</div>
        	</div>
        	<div class="col-sm-6">
				<h3 style="background: #024b68;color:#ffffff;padding:10px 20px;text-align:center">Add product category</h3>
				<form class="form-horizontal" method="POST" style="background: #ffffff;padding: 20px;" action="{{ route('add-product-category.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                   	<fieldset>
                   		<div class="col-sm-12 form-group margin50">
                                <label class="col-lg-4"  for="CategoryName">Category Name</label>
                                <div class="col-sm-8">
                                    <input type="text" id="name" name="CategoryName" placeholder="" class="form-control name" required>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group margin50">
                                <label class="col-lg-4"  for="CategorySlug">Product slug</label>
                                <div class="col-sm-8">
                                    <input type="text" id="slug" name="slug" placeholder="category-name" class="form-control name" required >
                                </div>
                            </div>
                            <div class="col-sm-12 form-group margin50">
                                <label class="col-lg-4"  for="CategoryName">Cover Photo</label>
                                <div class="col-sm-8">
                                    <div class="ibox">
                                        <div class="ibox-body">
                                            <input name="coverPhoto" type="file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group" style="display: flex;justify-content:center">
                                <label class="col-lg-4"></label>
                                <div class="col-lg-8">
                                    <input type="submit" id="submitForm" name="submit" class="form-control btn btn-primary" value="ADD">
                                </div>
                            </div>
                   	</fieldset>
                </form>
        	</div>
        	<br>
			<div class="visible-xs">
				<h3 style="background: #024b68;color:#ffffff;padding:10px 20px;text-align:center">All product categories</h3>
				<div class="col-sm-12" style="padding: 20px;">
					@forelse($categories as $category)
					<div class="col-xlg-2 col-md-3 col-sm-4 col-xs-6" id="cate-{{ $category->id }}" style="background: #ffffff;">

                        <div class="category-wrap text-center individual-category">
                            <div class="category-image">
                            	<sup class="delete" data-id="{{ $category->id }}" data-name="{{ $category->category_name }}">x</sup>
                                <img src="{{ asset('swasthya/products-category/'.$category->cover_photo) }}">
                                    <div class="category-name">{{ $category->category_name }}</div>
                            </div>
                        </div>
                	</div>
                	@empty
                	<p>No product category found !!</p>
                	@endforelse
			</div>
        	</div>
        	
    	</div>
	</div>

@endsection

@section('individual-footer')
    <script>
    $(document).ready(function() {

        $('.delete').on('click', function(){

            var id = $(this).data('id');
            var name = $(this).data('name');
            var result = confirm('Do you really want to delete '+name);

            if(result){

            	$.ajax({
                url:'/delete-category',
                type:'GET',
                data:{'id':id,'_token':'<?php echo csrf_token() ?>'},
                success:function(response){
                   $("#cat-"+id).fadeOut('slow', function () {
                        $(this).remove();
                    });
                   $("#cate-"+id).fadeOut('slow', function () {
                        $(this).remove();
                    });
                }
            })
            }else{
            	return false;
            }

        });
    });
</script>
@endsection