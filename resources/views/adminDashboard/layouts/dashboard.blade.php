@extends('adminDashboard.layouts.master')
@section('title', "Admin | Dashboard")
@section('main-content')
	<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="margin-top:50px;height:600px;">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                   <strong>Welcome</strong> {{ ucfirst(Auth::user()->username) }},  You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection