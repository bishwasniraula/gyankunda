  
    <link rel="icon" href="{{ asset('frontend/images/favicon.png') }}">
    <title>@yield('title')</title>
    <link type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">

    <link type="text/css" href="{{ asset('frontend/design/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/design/vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Include external CSS. -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

    <!-- Include Editor style. -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.2/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.2/css/froala_style.min.css" rel="stylesheet" type="text/css" />

    <!-- Include datatable style -->
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!--    main styles-->
    <link type="text/css" href="{{ asset('frontend/css/serviceProviderDashboard/navigation/navigation.css') }}" rel="stylesheet">
    
    <link type="text/css" href="{{ asset('frontend/css/serviceProviderDashboard/asideBar.css') }}" rel="stylesheet">
    
    @section('individual-header')
        @show

    <link type="text/css" href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">

    <link type="text/css" href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">

    <style>
        .main-sidebar .sidebar-menu{
            top:90px !important;
        }
    </style>