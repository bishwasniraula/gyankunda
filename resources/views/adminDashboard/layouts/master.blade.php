<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
	@include('adminDashboard.layouts.head')
</head>


<body  style="background: #f1f1f1;overflow-x: hidden">
  <!-- /.banner-->
<div class="wrapper" id="site-wrapper">

	@include('dashboardComponent.navigation')
    <!--    /.gloabl-nav -->

	@include('adminDashboard.asideBar')
    <!--   /.main-sidebar -->	
	
	
 <div class="content-wrapper">
  
      

       @section('main-content')
            @show


    </div>

  	
    @include('adminDashboard.layouts.footer')

  @section('individual-footer')
    @show
</div>
</body>
</html>