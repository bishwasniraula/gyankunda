<!--template script-->
<script src="{{ asset('frontend/design/vendor/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('frontend/design/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- Include external JS libs. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
<!-- Include datatable -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<!-- Include Editor JS files. -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.7.2/js/froala_editor.pkgd.min.js"></script>

<!-- PAGE LEVEL PLUGINS-->
<script src="{{ asset('frontend/source/js/dropzone/min/dropzone.min.js') }}" type="text/javascript"></script>
<!--<script src="source/js/dropzone/min/dropzone-amd-module.min.js" type="text/javascript"></script>-->
<!-- Initialize the editor. -->
<script> $(function() { $('textarea').froalaEditor() }); </script>
<!-- PAGE LEVEL SCRIPTS-->

<!--main script-->
<script src="{{ asset('frontend/source/js/serviceProviderDashboard.js') }}"></script>
<script src="{{ asset('frontend/source/js/scripts.js') }}"></script>

@yield('script')