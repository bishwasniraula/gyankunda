<!-- NAVBAR -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="brand">
        <a href="{{ route('dashboard') }}"><img src="{{ asset('frontend/design/assets/images/logos.png') }}" alt="logo" class="img-responsive logo" style="height:100%"></a>
        <img src="{{ asset('frontend/design/assets/images/swasthya-vector-logo.png') }}" alt="" style="height: 100%;margin-left: 50px;margin-top: -60px;"></a>
      </div>
      <div class="container-fluid">
        <div id="navbar-menu">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="" class="img-circle" alt="Avatar"> <span title="Username"></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
                <li><a href=""><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>