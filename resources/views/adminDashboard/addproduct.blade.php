@extends('adminDashboard.layouts.master')

@section('individual-header')

    <link type="text/css" href="{{ asset('frontend/css/serviceProviderDashboard/product/products.css') }}"/>

@endsection

@section('title', "Admin | Add product")
@section('main-content')

        <!--add  product-->
        <div class="container-fluid ">

            <div class="row add-product-container" style="display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;margin: 20px;">
                <div class="col-sm-8">
                    <h3 style="background: #024b68;color:#ffffff;padding:10px 20px;text-align:center">Add product</h3>
                    <form class="form-horizontal" method="POST" style="background: #ffffff;padding: 20px;" action="{{ route('product.store')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <fieldset>
                            <div class="col-sm-12 form-group margin50">
                                <label class="col-lg-3"  for="ProductName">Product Name</label>
                                <div class="col-sm-4">
                                    <input type="text" id="name" name="ProductName" placeholder="" class="form-control name" required>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group margin50">
                                <label class="col-lg-3"  for="slug">Slug</label>
                                <div class="col-sm-4">
                                    <input type="text" id="slug" name="slug" placeholder="" class="form-control slug" required>
                                </div>
                            </div>

                            <div class=" col-sm-12 form-group">
                                <label class="col-sm-3" for="ProductType">Product Type</label>
                                <div class="col-sm-4">
                                    <select id="productType" name="ProductType" class="form-control product-type" required>
                                        <option value="" selected default hidden>-Select-</option>
                                        @forelse($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Manufacturer">Manufacturer</label>
                                <div class="col-sm-4">
                                    <select id="manufacturer" name="manufacturer" class="form-control manufacturer" required>
                                        <option value="" selected default hidden>-Select-</option>
                                        <option value="Manufacturer A">Manufacturer A</option>
                                        <option value="Manufacturer B">Manufacturer B</option>
                                        <option value="Manufacturer C">Manufacturer C</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Distributor">Distributor</label>
                                <div class="col-sm-4">
                                    <select id="distributor" name="distributor" class="form-control distributor" required>
                                        <option value="" selected default hidden>-Select-</option>
                                        <option value="Distributor A">Distributor A</option>
                                        <option value="Distributor B">Distributor B</option>
                                        <option value="Distributor C">Distributor C</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="SKU">SKU</label>
                                <div class="col-sm-4">
                                    <input type="text" id="sku" name="SKU" placeholder="" class="form-control sku" required>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="ManufacturerPart">Manufacturer Part</label>
                                <div class="col-sm-4">
                                    <input type="text" id="manufacturerPart" name="manufacturerPart" placeholder="" class="form-control manufacturer-part">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="Published">Published</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="Published" value="0" class="input-xlarge"><span>No</span>
                                    <input type="radio"  name="Published" checked value="1" class="input-xlarge"><span>Yes</span>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="Featured">Featured</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="Featured" class="input-xlarge" value="0"><span>No</span>
                                    <input type="radio"  name="Featured" checked class="input-xlarge" value="1"><span>Yes</span>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="PageDisplay">Page Display</label>
                                <div class="col-sm-4">
                                    <select id="pageDisplay" name="PageDisplay" class="form-control page-display" required>
                                        <option value="" selected default hidden>-Select-</option>
                                        <option value="page display-1">page display-1</option>
                                        <option value="page display-2">page display-2</option>
                                        <option value="page display-3">page display-3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-lg-3" for="TaxClass">Tax Class</label>
                                <div class="col-lg-4">
                                    <select id="taxClass" name="TaxClass" class="form-control tax-class">
                                        <option selected default hidden>-Select-</option>
                                        <option value="1">Tax-1</option>
                                        <option value="2">Tax-2</option>
                                        <option value="3">Tax-3</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="QuantityDiscountTable">Quantity Discount Table</label>
                                <div class="col-sm-4">
                                    <select id="quantityDiscount" name="QuantityDiscountTable" class="form-control quantity-discount">
                                        <option selected default hidden>-Select-</option>
                                        <option value="1">Quantity Discount-1</option>
                                        <option value="2">Quantity Discount-2</option>
                                        <option value="3">Quantity Discount-3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="ShowBuyButton">Show Buy Button</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="ShowBuyButton" class="input-xlarge" value="0"><span>No</span>
                                    <input type="radio"  name="ShowBuyButton" checked class="input-xlarge" value="1"><span>Yes</span>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="RequiresRegistrationToView">Requires Registration To View</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="RequiresRegistrationToView" checked class="input-xlarge" value="0"><span>No</span>
                                    <input type="radio"  name="RequiresRegistrationToView"  class="input-xlarge" value="1"><span>Yes</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="HidePriceUntilCart">Hide Price Until Cart</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="HidePriceUntilCart" checked class="input-xlarge" value="0"><span>No</span>
                                    <input type="radio"  name="HidePriceUntilCart"  class="input-xlarge" value="1"><span>Yes</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3"  for="Inventory">Track Inventory By Size and Color</label>
                                <div class="col-sm-4">
                                    <input type="radio"  name="Inventory" checked class="input-xlarge" value="0"><span>No</span>
                                    <input type="radio"  name="Inventory" class="input-xlarge" value="1"><span>Yes</span>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="SizeOptionPrompt">Size Option Prompt</label>
                                <div class="col-sm-4">
                                    <input type="text" id="sizeOptionPrompt" name="SizeOptionPrompt" placeholder="" class="form-control size-option-prompt">
                                </div>
                            </div>
                            
                            <h3 style="padding:10px 20px;text-align:center;text-decoration: underline">Default Variant Information</h3>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="description">Description</label>
                                <div class="col-sm-9">
                                    <textarea name="description" id="description" rows="10" rows="10"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="composition">Salt Composition</label>
                                <div class="col-sm-9">
                                    <textarea name="composition" id="composition" rows="10" rows="10"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="basicUse">Primarly used for</label>
                                <div class="col-sm-9">
                                    <textarea name="basicUse" id="basicUse" rows="10" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="sideEffect">Side effect</label>
                                <div class="col-sm-9">
                                    <textarea name="sideEffect" id="sideEffect" rows="10" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="use">How to use</label>
                                <div class="col-sm-9">
                                    <textarea name="use" id="use" rows="10" rows="10"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Price">Situations & States</label>
                                <div class="col-sm-9">
                                    <div class="row" id="pandsgrp">
                                        <div class="col-sm-12">
                                                                
                                                <button id="add-situation-state" type="button" class="btn btn-sm btn-info" data-counter="1" style="margin-bottom:10px;">Add Situations & States</button>
                                         
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Price">Image upload</label>
                                <div class="col-sm-9">
                                    <div class="page-content fade-in-up">
                                        <div class="ibox">
                                            <div class="ibox-body">
                                                <input name="coverPhoto" type="file" multiple="multiple">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Price">Price</label>
                                <div class="col-sm-4">
                                    <input type="text" id="price" name="price" placeholder="" class="form-control price" required>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="SalePrice">Sale Price</label>
                                <div class="col-sm-4">
                                    <input type="text" id="saleprice" name="SalePrice" placeholder="" class="form-control sale-price" required> 
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="exp-date">Expiry Date</label>
                                <div class="col-sm-4">
                                    <input type="date" id="expdate" name="expdate" class="form-control expdate" required>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Weight">Weight</label>
                                <div class="col-sm-4">
                                    <input type="text" id="weight" name="Weight" placeholder="" class="form-control weight">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Dimentions">Dimentions(Width x Height X Depth)</label>
                                <div class="col-lg-10">
                                    <div class="col-lg-3"><input type="text" id="dimentionsWidth" name="DimentionsWidth" placeholder="" class="form-control dimentions-width">  X</div>
                                    <div class="col-lg-3"><input type="text" id="dimentionsHeight" name="DimentionsHeight" placeholder="" class="form-control dimentions-height">  X</div>
                                    <div class="col-lg-3"><input type="text" id="dimentionsDepth" name="DimentionsDepth" placeholder="" class="form-control dimentions-depth"></div>
                                </div>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="CurrentInventory">Current Inventory</label>
                                <div class="col-sm-4">
                                    <input type="number" id="currentInventory" name="qty" placeholder="" class="form-control current-inventory" required>
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Colors">Colors</label>
                                <div class="col-sm-4">
                                    <input type="text" id="colors" name="Colors" placeholder="" class="form-control colors">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="ColorSKUModifier">Color SKU Modifiers</label>
                                <div class="col-sm-4">
                                    <input type="text" id="colorModifier" name="ColorSKUModifier" placeholder="" class="form-control color-modifier">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="Sizes">Sizes</label>
                                <div class="col-sm-4">
                                    <input type="text" id="sizes" name="Sizes" placeholder="" class="form-control sizes">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group">
                                <label class="col-sm-3" for="SizeSKUModifier">Size SKU Modifiers</label>
                                <div class="col-sm-4">
                                    <input type="text" id="sizeModifier" name="SizeSKUModifier" placeholder="" class="form-control size-modifier">
                                </div>
                            </div>
                            <div class="col-sm-12 form-group" style="display: flex;justify-content:center">
                                <div class="col-lg-6">
                                    <input type="submit" id="submitForm" name="submit" class="form-control btn btn-primary">
                                </div>
                            </div>
                        </fieldset>

                    </form>

                </div>
            </div>
        </div>
        <!--/add product-->
    
@endsection

@section('individual-footer')

<script>
    $(document).ready(function(){
        $('#add-situation-state').on('click',function(){

            var counter = $(this).data('counter');

           if(counter>10){
            alert('Only 10 SITUATION & STATE allow !');
            return false;
           }

           var newStnState = $(document.createElement('div')).attr("id", 'newStnState'+counter, "class", 'col-sm-12');

                newStnState.after().html('<div style="margin-top:10px 0 10px 0;"><div class="col-sm-6"><input type="text" id="sit-'+counter+'" name="situation'+counter+'" placeholder="Situation" class="form-control price" required autofocus></div><div class="col-sm-6" style="margin:6px 0;"><label class="col-sm-4" for="state">State</label><label class="radio-inline" style="margin-top: -7px;"><input type="radio" checked name="radiogrp'+counter+'" value="1">Safe</label><label class="radio-inline" style="margin-top: -7px;"><input type="radio" name="radiogrp'+counter+'" value="0">Unsafe</label></div></div>');

                newStnState.appendTo('#pandsgrp');
                counter++;
                $(this).data('counter',counter++);
        });
       
    });
</script>

@endsection


                                        