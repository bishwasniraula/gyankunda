@extends('adminDashboard.layouts.master')

@section('individual-header')

    <link type="text/css" href="{{ asset('frontend/css/serviceProviderDashboard/product/products.css') }}"/>

@endsection

@section('title', "Admin | Users status")
@section('main-content')

        <!--add  product-->
        <div class="container-fluid ">
            

        <!--        order status display-->
        <div class="container" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
            <h2 style="margin: 10px 0;">Users status</h2>
            <hr>
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#activeUsers" aria-controls="profile" role="tab" data-toggle="tab">Active users</a></li>
                    <li role="presentation"><a href="#pendingUsers" aria-controls="home" role="tab" data-toggle="tab">Pending users</a></li>
                    <li role="presentation"><a href="#suspendedUsers" aria-controls="messages" role="tab" data-toggle="tab">Suspended users</a></li>
                    <li role="presentation"><a href="#allUsers" aria-controls="settings" role="tab" data-toggle="tab">All users</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="activeUsers">
                        <div class="table-responsive" data-pattern="priority-columns" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
                            <table class="userstatus table table-striped table-bordered table-hover" >
                                <thead>
                                <tr>
                                    <th>UserName</th>
                                    <th data-priority="1">Name</th>
                                    <th data-priority="2">Email</th>
                                    <th data-priority="3">Role</th>
                                    <th data-priority="4">Remark</th>
                                    <th data-priority="5">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                            @forelse($ActiveUsers as $active)
                                <tr>
                                    <td>{{ $active->email }}</td>
                                    <td>{{ $active->name }}</td>
                                    <td>{{ $active->email }}</td>
                                    <td><span class="label label-success">{{ $active->purpose }}</span></td>
                                    <td>

                                    </td>
                                    <td>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></button>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash font-14"></i></button>
                                    </td>
                                </tr>
                                @empty
                                    <p>No active users found!</p>
                                @endforelse
        
                                </tbody>
                            </table>
                        </div><!--end of .table-responsive-->

                    </div>
                    <div role="tabpanel" class="tab-pane" id="pendingUsers">
<!--                        table content-->
                        <div class="table-responsive" data-pattern="priority-columns" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
                            <table  class="userstatus table table-striped table-bordered table-hover" >
                                <thead>
                                <tr>
                                    <th>UserName</th>
                                    <th data-priority="1">Name</th>
                                    <th data-priority="2">Email</th>
                                    <th data-priority="3">Role</th>
                                    <th data-priority="4">Remark</th>
                                    <th data-priority="5">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                @forelse($PendingUsers  as $pending)
                                <tr>
                                    <td>{{ $pending->email }}</td>
                                    <td>{{ $pending->name }}</td>
                                    <td>{{ $pending->email }}</td>
                                    <td><span class="label label-success">{{ $pending->purpose }}</span></td>
                                    <td>

                                    </td>
                                    <td>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></button>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash font-14"></i></button>
                                    </td>
                                </tr>
                                @empty
                                    <p>No pending users found!</p>
                                @endforelse
                                
                                </tbody>
                            </table>
                        </div><!--end of .table-responsive-->
                    </div>
            
                    <div role="tabpanel" class="tab-pane" id="suspendedUsers">
                        <div class="table-responsive" data-pattern="priority-columns" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
                            <table class="userstatus table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>UserName</th>
                                    <th data-priority="1">Name</th>
                                    <th data-priority="2">Email</th>
                                    <th data-priority="3">Role</th>
                                    <th data-priority="4">Remark</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($SuspendUsers  as $suspend)
                                <tr>
                                    <td>{{ $suspend->email }}</td>
                                    <td>{{ $suspend->name }}</td>
                                    <td>{{ $suspend->email }}</td>
                                    <td><span class="label label-success">{{ $suspend->purpose }}</span></td>
                                    <td>

                                    </td>
                                    <td>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></button>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash font-14"></i></button>
                                    </td>
                                </tr>
                                @empty
                                    <p>No suspended users found!</p>
                                @endforelse
                                </tbody>
                            </table>
                        </div><!--end of .table-responsive-->

                    </div>
                    <div role="tabpanel" class="tab-pane" id="allUsers">
                        <div class="table-responsive" data-pattern="priority-columns" style="background:#ffffff;margin-top:10px;margin-bottom:10px">
                            <table class="userstatus table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>UserName</th>
                                    <th data-priority="1">Name</th>
                                    <th data-priority="2">Email</th>
                                    <th data-priority="3">Role</th>
                                    <th data-priority="4">action</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @forelse($Users  as $all)
                                <tr>
                                    <td>{{ $all->email }}</td>
                                    <td>{{ $all->name }}</td>
                                    <td>{{ $all->email }}</td>
                                    <td><span class="label label-success">{{ $all->purpose }}</span></td>
                                    <td>

                                    </td>
                                    <td>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil font-14"></i></button>
                                        <button class="btn btn-default btn-sm btn-flat" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash font-14"></i></button>
                                    </td>
                                </tr>
                                @empty
                                    <p>No users found!</p>
                                @endforelse
                                </tbody>
                            </table>
                        </div><!--end of .table-responsive-->

                    </div>
                </div>

            </div>
        </div>
       
    
        </div>
       
    
@endsection

@section('individual-footer')



@endsection


                                        