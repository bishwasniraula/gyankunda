<!DOCTYPE html>
<html lang="en">
<head>
	@include('admin.layouts.head')
</head>
<body class="nav-md">
	<div class="container body">
      <div class="main_container">
      	@include('admin.layouts.header')
		
		<div class="right_col" role="main">
			@section('main-content')
      			@show
		</div>
      	
      	@include('admin.layouts.footer')
      </div>
  </div>
</body>
</html>