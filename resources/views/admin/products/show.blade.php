@extends('admin.layouts.app')

@section('title', "$product->title".' Detail')

@section('main-content')
  
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
                  <div class="x_title">
                    <h2>{{ $product->title }}<small>Product Detail Information</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h2>
                            <center><i class="fa fa-paw"></i> Swasthya PVT.LTD.</center>
                            <small class="pull-right">Date: 16/08/2016</small>
                          </h2>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          <address>
                            <strong>Product Title:</strong> <span>{{ $product->title }}</span>
                              <br><strong>Product Quantity:</strong> <span>{{ $product->quantity }}</span>
                              <br><strong>Product Price:</strong> <span>{{ $product->price }}</span>
                              <br><strong>Selling Price:</strong> <span>{{ $product->selling_price }}</span>
                              <br><strong>Expiry Date:</strong> <span>{{ $product->expiry_date }}</span>
                              <br><strong>Manufactured by:</strong> <span>{{ $product->manufacturing }}</span>
                              <br><strong>Brand:</strong> <span>{{ $product->brand }}</span>
                              <br><strong>Distributor:</strong> <span>{{ $product->distributor }}</span>
                              </address>
                        </div>
                        
                      
                      </div>
                      <!-- /.row -->
                      <!-- Desctiption row -->
                      <div class="row">
                          <div class="col-xs-12">
                            <h4><strong>Product Image</strong></h4>
                            <img src="{{ asset('swasthya/products/'.$product->cover_photo) }}" alt="image-preview" class="img-responsive">
                          </div>
                      </div>
                      <!-- /.row -->
                      <!-- Desctiption row -->
                      <div class="row">
                          <div class="col-xs-12">
                            <h4><strong>Desctioption</strong></h4>
                            <p>{{ $product->description }}</p>
                          </div>
                      </div>
                      <!-- /.row -->
                  
                      <!-- salt composition row -->
                      <div class="row">
                        
                          <div class="col-xs-12">
                            <h4><strong>Salt Composition</strong></h4>
                            <p>{{ $product->salt_composition }}</p>
                          </div>
                      
                      </div>

                      <!-- basic row -->
                      <div class="row">
                        
                          <div class="col-xs-12">
                            <h4><strong>Basic use</strong></h4>
                            <p>{{ $product->basic_use }}</p>
                          </div>
                      
                      </div>
                      <!-- side effect row -->
                      <div class="row">
                        
                          <div class="col-xs-12">
                            <h4><strong>Side Effect</strong></h4>
                            <p>{{ $product->side_effect }}</p>
                          </div>
                      
                      </div>
                      <!-- how to use row -->
                      <div class="row">
                        
                          <div class="col-xs-12">
                            <h4><strong>How to use</strong></h4>
                            <p>{{ $product->how_to_use }}</p>
                          </div>
                      
                      </div>
                      <!-- /.row -->

                      

                      <!-- this row will not appear when printing -->
                      <!-- <div class="row no-print">
                        <div class="col-xs-12">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                          <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
                          <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                        </div>
                      </div> -->
                    </section>
                  </div>
                </div>
  </div>

@endsection
