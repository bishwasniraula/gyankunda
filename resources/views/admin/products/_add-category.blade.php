@extends('admin.layouts.app')

@section('title', "Add category")

@section('main-content')
  <div class="col-md-12 col-sm-12 col-xs-12">
     @include('includes._messages')
                <div class="x_panel">
                  <div class="x_title">
                    <h2 class="text-center">Add product category</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form id="demo-form2"  class="form-horizontal form-label-left" method="post" action="{{ route('add-product-category.store') }}">
                      {{ csrf_field() }}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="top-category">Top Category<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="top-category" name="tpCategory" required="required" class="form-control col-md-7 col-xs-12" >
                        </div>
                      </div>                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-right">
                          <button class="btn btn-primary" type="reset">Clear</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2 class="text-center">Add product middle-category</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form id="demo-form2"  class="form-horizontal form-label-left" method="post" action="{{ route('add-product-category.store') }}">
                      {{ csrf_field() }}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="t-category">Top Category
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="s_tpcat" id="t-category" class="form-control col-md-7 col-xs-12" >
                            @foreach($category as $c)
                            <option value="{{ $c->id }}">{{ $c->top_category }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="md-category">Middle Category<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="md-category" name="mdCategory" class="form-control col-md-7 col-xs-12" type="text" required>
                        </div>
                      </div>
                       <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-right">
                          <button class="btn btn-primary" type="reset">Clear</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2 class="text-center">Add product bottom-category</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form id="demo-form2"  class="form-horizontal form-label-left" method="post" action="{{ route('add-product-category.store') }}">
                      {{ csrf_field() }}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="t-category">Top Category
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="bott_tpcat" id="tmb-category" class="form-control col-md-7 col-xs-12"  required>
                            <option value="" selected default hidden>Select top-category</option>
                            @foreach($category as $c)
                            <option value="{{ $c->id }}">{{ $c->top_category }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="md-category">Middle Category
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="bott_middleCat" id="m-cat" class="form-control col-md-7 col-xs-12" required>
                            <option value="" selected default hidden>Select top category first..</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="btm-category">Bottom Category</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="btmI-category" name="btmCat" class="form-control col-md-7 col-xs-12" type="text" >
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-right">
                          <button class="btn btn-primary" type="reset">Clear</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
@endsection

@section('script')

<script>
  $(document).ready(function(){

    $('#btmI-category').attr('disabled','disabled');

    $('#tmb-category').change(function() {
      $('#m-cat').empty();

      $('#btmI-category').attr('disabled','disabled');
        var mid = $(this).val();

        $.ajax({
          method:'GET',
          url:'/get-cat-sub-item',
          dataType:'json',
          data:{'id':mid,'_token':'<?php echo csrf_token() ?>'},
          success:function(response){
            if(response.data==0){
              $('#m-cat').empty().append('<option value="">You need to insert middle category first.</option>');
            }else{
              var data = (response.data);
            for(i=0;i<data.length;i++){
               $('#m-cat').append('<option value="'+data[i].id+'">'+data[i].middle_category+'</option>');
                $('#btmI-category').removeAttr('disabled');
            }
          }
        }

        });
    });
  });
</script>
@endsection