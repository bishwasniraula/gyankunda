@extends('admin.layouts.app')

@section('title', "All Products")

@section('main-content')
	

	<div class="col-md-12 col-sm-12 col-xs-12">

    @include('includes._messages')

            
              <div class="x_panel">
                  <div class="x_title">
                    <h2>All products <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Product Name</th>
                          <th>Selling Price</th>
                          <th>Quantity</th>
                          <th>Status</th>
                          <th>Expiry Date</th>
                          <th colspan="2">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        @forelse ($products as $product)
                          <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->selling_price }}</td>
                            <td>{{ $product->quantity }}
                             @if($product->quantity<=100)
                              <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{ ($product->quantity/1000)*100 }}"
                              aria-valuemin="0" aria-valuemax="1000" style="width:{{ ($product->quantity/1000)*100 }}%">
                            
                              </div>
                            </div>
                              @else
                                <div class="progress">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ ($product->quantity/1000)*100 }}"
                              aria-valuemin="0" aria-valuemax="1000" style="width:{{ ($product->quantity/1000)*100 }}%">
                            
                          </div>
                        </div>
                              @endif 
                            </td>
                            <td>{{ $product->status==1 ? 'Available' : 'X' }}</td>
                            <td>{{ $product->expiry_date }}</td>
                            <td><a class="btn btn-primary btn-xs" href="/admin/product/{{ $product->id }}"><i class="fa fa-eye" aria-hidden="true"></i>
                            </a> <a class="btn btn-info btn-xs" href="/admin/product/{{ $product->id }}/edit"><i class="fa fa-pencil-square" aria-hidden="true"></i> 
                            </a>                             
                          </td>
                          <td>
                             <form action="/admin/product/{{ $product->id }}" method="POST">
                              <input type="hidden" name="_method" value="DELETE">
                              <input type="submit" class="btn btn-danger btn-xs" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                          </form>
                          </td>
                        </tr>
                        @empty
                        <p><center>No products have been added yet !!</center></p>
                        @endforelse
                      </tbody>
                    </table>

                  </div>
                </div>
            </div>

@endsection
