@extends('admin.layouts.app')
@section('title', "Add Product")
@section('main-content')
	<div class="col-md-12 col-sm-12 col-xs-12">
      @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

                <div class="x_panel">
                  <div class="x_title">
                    <h2 class="text-center">Add new product</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <form id="demo-form2"  class="form-horizontal form-label-left" method="post" action="{{ route('product.store')}}" enctype="multipart/form-data">
                    	{{ csrf_field() }}

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="first-name" name="productName" required="required" class="form-control col-md-7 col-xs-12" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="t-cat">Top Category<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="t_cat" id="t-cat" class="form-control col-md-7 col-xs-12" required>
                            <option value="" selected default hidden>Select product category...</option>
                            @foreach($category as $cat)
                            <option value="{{ $cat->id }}">{{ $cat->top_category or '' }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="md-cat">Middle category</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="md_cat" id="md-cat" class="form-control col-md-7 col-xs-12" disabled>
                            <option selected default hidden>Select product sub-category...</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bot-cat">Bottom category</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="bot_cat" id="bot-cat" class="form-control col-md-7 col-xs-12" disabled>
                           <option selected default hidden>Select product sub-category...</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slug">Slug <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="slug" min="5" max="200" name="slug" required="required" class="form-control col-md-7 col-xs-12" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Quantity <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="last-name" name="qty" required="required" class="form-control col-md-7 col-xs-12" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="price" class="control-label col-md-3 col-sm-3 col-xs-12">Price <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="price" class="form-control col-md-7 col-xs-12" name="price" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Market Price <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="middle-name" class="form-control col-md-7 col-xs-12" name="sellingPrice" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Expiry Date</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="middle-name" class="form-control col-md-7 col-xs-12" name="expdate" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="description" class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="description" required="required" class="form-control" name="description" rows="5"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="composition" class="control-label col-md-3 col-sm-3 col-xs-12">Salt Composition</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="composition" required="required" class="form-control" name="composition" rows="3"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="basicUse" class="control-label col-md-3 col-sm-3 col-xs-12">Primarly used for</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="basicUse" required="required" class="form-control" name="basicUse" rows="5"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="sideEffect" class="control-label col-md-3 col-sm-3 col-xs-12">Side effect</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="sideEffect" required="required" class="form-control" name="sideEffect" rows="5"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="use" class="control-label col-md-3 col-sm-3 col-xs-12">How to use</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="use" required="required" class="form-control" name="use" rows="4"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-3 col-md-offset-2 col-sm-3 col-sm-offset-2 col-xs-12">
                        <label for="use" class="control-label">Pregnancy & State</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-3 col-md-offset-2 col-sm-3 col-sm-offset-2 col-xs-12">
                          <label  class="control-label">Alcohol<label class="" style="padding-left: 25px;">
                              <div class="iradio_flat-green" style="position: relative;"><input class="flat" checked="checked" name="alcohol" value="Safe" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Safe
                            </label>  <label class="">
                              <div class="iradio_flat-green" style="position: relative;"><input class="flat" name="alcohol" value="Unsafe" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Unsafe
                            </label></label>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-3 col-md-offset-2 col-sm-3 col-sm-offset-2 col-xs-12">
                          <label class="control-label">Pregnancy<label class="" style="padding-left: 5px;">
                              <div class="iradio_flat-green" style="position: relative;"><input class="flat" checked="checked" name="pregnancy" value="Safe" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Safe
                            </label>  <label class="">
                              <div class="iradio_flat-green" style="position: relative;"><input class="flat" name="pregnancy" value="Unsafe" style="position:  absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Unsafe
                            </label></label>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-3 col-md-offset-2 col-sm-3 col-sm-offset-2 col-xs-12">
                          <label class="control-label">Driving<label class="" style="padding-left: 29px;">
                              <div class="iradio_flat-green" style="position: relative;"><input class="flat" checked="checked" name="driving" value="Safe" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Safe
                            </label>  <label class="">
                              <div class="iradio_flat-green" style="position: relative;"><input class="flat" name="driving" value="Unsafe" style="position: absolute; opacity: 0;" type="radio"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Unsafe
                            </label></label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="manufacturing" class="control-label col-md-3 col-sm-3 col-xs-12">Manufactured by <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="manufacturing" class="form-control col-md-7 col-xs-12" name="manufacturing" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="brand" class="control-label col-md-3 col-sm-3 col-xs-12">Brand <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="brand" class="form-control col-md-7 col-xs-12" name="brand" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="distributor" class="control-label col-md-3 col-sm-3 col-xs-12">Distributor <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="distributor" class="form-control col-md-7 col-xs-12" name="distributor" type="text">
                        </div>
                      </div>

                      <div class="form-group">
                      	<label class="control-label col-md-3 col-sm-3 col-xs-12">Status
                        </label>
                      	<div class="checkbox">
                            <label class="">
                              <div class="icheckbox_flat-green checked" style="position: relative;"><input class="flat" name="status" checked="checked" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Available
                            </label>
                          </div>
                      </div>
                      <div class="form-group">
                        <label for="coverPhoto" class="control-label col-md-3 col-sm-3 col-xs-12">Cover Photo</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="file" id="coverPhoto"  name="coverPhoto" />
                        </div>
                      </div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						  <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
@endsection

@section('script')

<script>
  $(document).ready(function(){

    $('#t-cat').change(function() {
        var mid = $(this).val();
        $('#md-cat').empty();
        $('#md-cat').removeAttr('disabled');

        $.ajax({
          method:'GET',
          url:'/get-cat-sub-item',
          dataType:'json',
          data:{'id':mid,'_token':'<?php echo csrf_token() ?>'},
          success:function(response){
            if(response.data==0){
              $('#md-cat').empty();
              $('#bot-cat').empty();
              $('#md-cat').attr('disabled','disabled');
              $('#bot-cat').attr('disabled','disabled');
            }else{

              var data = (response.data);

            for(i=0;i<data.length;i++){
              //console.log(data[i].middle_category);
              $('#md-cat').append('<option value="'+data[i].id+'">'+data[i].middle_category+'</option>');
            }
          }
        }

        });
    });

    $('#md-cat').change(function() {
        var id = $(this).val();
        $('#bot-cat').empty();
        $('#bot-cat').removeAttr('disabled');

        $.ajax({
          method:'GET',
          url:'/get-cat-subs-items',
          dataType:'json',
          data:{'id':id,'_token':'<?php echo csrf_token() ?>'},
          success:function(response){
            //console.log(response.data);
            if(response.data==0){
              $('#bot-cat').empty();
              $('#bot-cat').attr('disabled','disabled');
            }else{

              var data = (response.data);
            for(i=0;i<data.length;i++){
              $('#bot-cat').append('<option value="'+data[i].id+'">'+data[i].bottom_category +'</option>');
            }
          }
        }

        });
    });


  });
</script>
@endsection