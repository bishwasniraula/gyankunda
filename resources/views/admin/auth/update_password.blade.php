@extends('adminDashboard.layouts.master')

@section('individual-header')

    <link type="text/css" href="{{ asset('frontend/css/serviceProviderDashboard/product/products.css') }}"/>

@endsection

@section('title', "Admin | Add product")
@section('main-content')

        <!--add  product-->
        <div class="container-fluid ">
            <div class="col-md-8 col-md-offset-2">
                
            </div>
            <div class="row add-product-container" style="display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;margin: 20px;">

                <div class="col-sm-8">
                    @include('includes._messages')
                    <h3 style="background: #024b68;color:#ffffff;padding:10px 20px;text-align:center">Update admin password</h3>
                    <form class="form-horizontal" method="POST" style="background: #ffffff;padding: 20px;" action="{{ route('admin.password') }}">
                        
                        {{ csrf_field() }}
     
                        <div class="form-group{{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                            <label for="inputPassword" class="col-sm-4 control-label">Current Password</label>

                            <div class="col-sm-8 margin50">
                                <input id="oldpassword" type="password" class="form-control" name="oldpassword" placeholder="Current password" required>

                                @if ($errors->has('oldpassword'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('oldpassword') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-4 control-label">New Password</label>

                            <div class="col-sm-8 margin50">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-sm-4 control-label">Confirm Password</label>

                            <div class="col-sm-8 margin50">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="password-confirm" class="col-sm-4 control-label"></label>

                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-primary" style="border-radius:0;width:100%;font-size:18px;border-color:#046686;">
                                    CHANGE
                                </button>
                            </div>
                        </div>                    
                    </form>

                </div>
            </div>
        </div>
        <!--/add product-->
    
@endsection





                                        