@extends('admin.layouts.app')

@section('title', "Oreder Bill")

@section('main-content')
	

	<div class="col-md-12 col-sm-12 col-xs-12">

   <div class="x_panel">
                  <div class="x_title">
                    <h2><strong>Swasthya Nepal Pvt. Ltd.</strong><small>Order list</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li>

                        <form action="/admin/order/{{ $order[0]->id }}" method="POST">
                              <input type="hidden" name="_method" value="DELETE">
                              <input type="submit" class="btn btn-danger btn-xs" value="X">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                          </form>
                        
                        <!-- <form action="/admin/order/{{ $order[0]->id }}" method="POST">
                              <input type="hidden" name="_method" value="DELETE">
                              <input type="submit" class="close-link" value="X" style="border: 0px;background: #fff;margin-top: 4px;font-size: 15px;color: #C5C7CB;font-weight: bold;" title="Delete">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                          </form> -->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h2>
                            <small class="pull-right">Date: 16/08/2016</small>
                          </h2>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        
                          <address>
                              <strong>Order By</strong>
                                <br><strong><i class="fa fa-user"></i> <span>{{ $order[0]->name }}</span>
                                <br><strong><i class="fa fa-globe"></i> <span>Kathmandu, Nepal</span>
                                <br><strong><i class="fa fa-phone"></i></strong> <span>1 (804) 123-9876</span>
                                <br><strong><i class="fa fa-envelope"></i></strong> <span>{{ $order[0]->email }}</span>
                          </address>
                        </div>
                        
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Price (Rupees.)</th>
                                <th>Total Amount (Rupees.)</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>{{ $order[0]->id }}</td>
                                <td>{{ $order[0]->product_title }}</td>
                                <td>{{ $order[0]->quantity }}</td>
                                <td>{{ $order[0]->price }}</td>
                                <td>{{ $order[0]->quantity*$order[0]->price }}</td>
                              </tr>
                              
                              
                            
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      

                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                          
                         
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
 
            
              
  </div>

@endsection
