@extends('admin.layouts.app')

@section('title', "Oreders List")

@section('main-content')
	

	<div class="col-md-12 col-sm-12 col-xs-12">
      @include('includes._messages')
      <div class="x_panel">
        <h2><center>ORDERS LIST</center></h2>
        <div>
          <BR>
        <?php $carts = ''; $arr = ''; $c='' ?>
    
     @forelse ($orders as $order)
        <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Order Detail <em>({{ $order->email }})</em></h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <span>Payment Method <strong>{{ $order->payment_method }}</strong></span>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                <table class="table table-striped table-bordered table-list">
                  <thead>
                    <tr>
                        <th>s.n</th>
                        <th>Product_code</th>
                        <th class="hidden-xs">Product Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                    </tr> 
                  </thead>
                  <tbody>
                    <?php                             
                     $arr = $order->cart;
                    $carts = json_decode($arr, TRUE);              
                    foreach($carts as $key=> $c){
                      $k = $key+1;

                        echo '<tr>';

                        echo '<td>'.$k.'</td>';
                        echo '<td>'.$c['pid'].'</td>';
                        echo '<td>'.$c['ptitle'].'</td>';
                        echo '<td>Rs. '.$c['price'].'</td>';
                        echo '<td>'.$c['qty'].'</td>';
                        echo '<td>Rs. '.$c['totalprice'].'</td>';

                        echo '</tr>';



                     }?>
                        </tbody>
                        <tr>
                        <td><strong>Total Amount</strong></td>
                        <td colspan="5" class="text-right"><strong>Rs. {{ $order->total_amount  }} </strong></td>
                        </tr>
                        <tr>
                        <td><strong>Total Discount</strong></td>
                        <td colspan="5" class="text-right"><strong>Rs. {{ $order->total_discount }} </strong></td>
                        </tr>
                        <tr>
                        <td><strong>Total Amount</strong> <em>(including 13% VAT)</em></td>
                        <td colspan="5" class="text-right"><strong>Rs. {{ $order->total_amount_with_VAT }} </strong></td>
                        </tr>
                </table>
            
              </div>
              <div class="panel-footer">
                <div class="row">
    <div class="col col-xs-4">{{ $order->created_at }}</div>
    <div class="col-xs-8 text-right">
      <div class="checkbox">
          <label class="">
            <div class="icheckbox_flat-green" style="position: relative;"><div class="icheckbox_flat-green checked" style="position: relative;"><input class="flat" name="status" checked="checked" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> DELIVERED
          </label>
        </div>
      
                  </div>
                </div>
              </div>
            </div>

            @empty
                <p>Sorry! No orders have been made yet !</p>
            @endforelse

            <div class="col-md-12 text-right">
      {{ $orders->links() }}
    </div>
        

    </div>
      </div> 
  </div>

@endsection

@section('script')

 

@endsection
