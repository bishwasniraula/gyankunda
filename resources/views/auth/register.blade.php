<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
     <link rel="icon" href="{{ asset('frontend/images/favicon.png') }}">
    <title>Swasthya Nepal | signup</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">

     <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">

    <link type="text/css" href="{{ asset('frontend/design/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/design/vendor/font-awesome/css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    
    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/design/vendor/font-awesome/css/font-awesome-animation.min.css') }}">
    
    <link type="text/css" href="{{ asset('frontend/css/navigation/navigation.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('frontend/css/_asideBar.css') }}" rel="stylesheet">

    <link type="text/css" href="{{ asset('frontend/css/doctorProfile/style.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('frontend/css/_footers.css') }}" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body style="background: #f1f1f1;">

<!-- /.banner-->
<div class="wrapper" id="site-wrapper">

         @include('frontend.layouts._navigation')
    <!--    /.gloabl-nav -->

    <div class="content-wrapper">

    <div class="container doctorRegister__main" style="border-top: 5px solid #024b68;">
    <div class="sc-right">
        <div class="seller1">
            <h2 style="margin-top:20px;text-align: center">Why SwasthyaNepal?</h2>
            <div class="row seller-register">
                <div class="col-md-6 col-md-offset-3">
                   <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="inputName" class="col-sm-4 control-label">Name</label>

                            <div class="col-sm-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Name">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-4 control-label">E-Mail Address</label>

                            <div class="col-sm-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="email" class="col-sm-4 control-label">Mobile Number</label>

                            <div class="col-sm-8">
                                <input id="mobile" type="mobile" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Phone Number" required>

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-4 control-label">Password</label>

                            <div class="col-sm-8">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-sm-4 control-label">Confirm Password</label>

                            <div class="col-sm-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4 class="text-center" style="color:#ffffff;margin-bottom:20px;margin-top:20px;">What do you plan to use Swasthya Nepal for?</h4>
                        </div>
                        <div class="middle">
                                <label>
                                    <input type="radio" name="purpose" value="lab test" checked/>
                                    <div class="labtest box">
                                        <span>Lab test</span>
                                    </div>
                                </label>

                                <label>
                                    <input type="radio" name="purpose" value="online shopping" />
                                    <div class="Online-shopping box">
                                        <span>Online shopping</span>
                                    </div>
                                </label>
                                <label>
                                    <input type="radio" name="purpose" value="hospital booking"/>
                                    <div class="Hospital-Booking box">
                                        <span>Hospital Booking</span>
                                    </div>
                                </label>

                                <label>
                                    <input type="radio" name="purpose" value="online appointment"/>
                                    <div class="appointment box">
                                        <span>Online appointment</span>
                                    </div>
                                </label>
                        </div>
                        <div class="form-group text-center">
                            <div >
                                <button type="submit" class="btn btn-default" style="border-radius:0;width:100%;font-size:18px;background:#00B4B4;border-color:#00B4B4;color:#ffffff;font-weight:600;">
                                    Sign up
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

                <div class="sh-newsletters text-center">
                    <h4>Become a medical vendor today and reach millions of customers across Nepal</h4>
                </div>
        </div>
            <div class="contactDetails text-center">
                <div><strong style="color:#ffffff">Need more help?</strong>
                    <div style="color:#ffffff">
                        <span style="color:#ffffff">Call: 999999999</span><br>
                        <span style="color:#ffffff">Emai-swasthya@nepal.com.np</span>
                    </div>
                </div>
            </div>
            <div class="row services">
                <div class="col-md-4 text-center">
                    <img class="img-circle" src="https://static.daraz.com.np/cms/Help_page/map.png">
                    <h3>Sell 24x7 across</h3>
                    <h3>cities and towns in Nepal</h3>

                </div>
                <div class="col-md-4 text-center">
                    <img class="img-circle" src="https://static.daraz.com.np/cms/Help_page/Million-users.jpg">
                    <h3>Millions of users and</h3>
                    <h3>500+ sellers across Nepal</h3>

                </div>
                <div class="col-md-4 text-center">
                    <img class="img-circle" src="https://static.daraz.com.np/cms/Help_page/Quick-payments.jpg">
                    <h3>Quick payments</h3>
                    <h3>and transparent processes</h3>
                </div>
            </div>

            <div class="container">
                <div class="page-header">
                    <h1 id="" style="text-align:center;">4 Simple steps to start selling online</h1>
                </div>
                <div id="timeline">
                    <div class="row timeline-movement">

                        <div class="timeline-badge">
<!--                            <span class="timeline-balloon-date-day"><img src="https://static.daraz.com.np/cms/Help_page/s1.jpg"></span>-->

                        </div>


                        <div class="col-sm-6  timeline-item">
                            <div class="row">
                                <div class="col-sm-11">
                                    <div class="timeline-panel credits">
                                        <img src="https://static.daraz.com.np/cms/Help_page/Register.jpg" class="img-responsive">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6  timeline-item">
                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-11">
                                    <div class="timeline-panel debits">
                                        <ul class="timeline-panel-ul">
                                            <li><span class="importo">Step 1 : Register on Shop</span></li>
                                            <li><span class="causale">Register your business for free and create a product catalogue.
                                                    Our Seller Support Team will help you at every step and fully assist you in taking your business online.
                                                    Get free training on how to run your online business.
                                                    Get your documentation, photo-shoots, cataloguing, etc. done with ease from our Professional Services network.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row timeline-movement">

                        <div class="timeline-badge">
<!--                            <span class="timeline-balloon-date-day"><img src="https://static.daraz.com.np/cms/Help_page/s2.jpg"></span>-->
                        </div>
                        <div class="col-sm-6  timeline-item">
                            <div class="row">
                                <div class="col-sm-11">
                                    <div class="timeline-panel credits">
                                        <img src="https://static.daraz.com.np/cms/Help_page/Receive-orders.jpg" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6  timeline-item">
                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-11">
                                    <div class="timeline-panel debits">
                                        <ul class="timeline-panel-ul">
                                            <li><span class="importo">Step 2: Receive orders and sell across Nepal</span></li>
                                            <li><span class="causale">Once listed, your products will be available to millions of users across Nepal.
Get orders and manage your online business via our top of the line Seller Software and Mobile App.</span> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row timeline-movement">
                        <div class="timeline-badge">
<!--                            <span class="timeline-balloon-date-day"><img src="https://static.daraz.com.np/cms/Help_page/s3.jpg"></span>-->
                        </div>
                        <div class="col-sm-6  timeline-item">
                            <div class="row">
                                <div class="col-sm-11">
                                    <div class="timeline-panel credits">
                                        <img src="https://static.daraz.com.np/cms/Help_page/Package-&amp;-Ship.jpg" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6  timeline-item">
                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-11">
                                    <div class="timeline-panel debits">
                                        <ul class="timeline-panel-ul">
                                            <li><span class="importo">Step 3 : Package and ship with ease</span></li>
                                            <li><span class="causale">On receiving orders, pack the goods &amp; leave the worries of pick-up &amp; delivery to our courier partners.
Simply hand over the responsibilities of inventory storage, packaging &amp; delivering the orders to us.</span> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row timeline-movement">

                        <div class="timeline-badge">
<!--                            <span class="timeline-balloon-date-day"><img src="https://static.daraz.com.np/cms/Help_page/s4.jpg" class="img-responsive"></span>-->
                        </div>


                        <div class="col-sm-6  timeline-item">
                            <div class="row">
                                <div class="col-sm-11">
                                    <div class="timeline-panel credits">
                                        <img src="https://static.daraz.com.np/cms/Help_page/Get-payments.jpg" class="img-responsive">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6  timeline-item">
                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-11">

                                    <div class="timeline-panel debits">
                                        <ul class="timeline-panel-ul">
                                            <li><span class="importo">Step 4: Get payments and grow your business</span></li>
                                            <li><span class="causale">Receive quick and hassle-free payments in your account once your orders are fulfilled.
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <br><br>
            <div class="seller4 text-center">
                <h2>What is SwasthyaNepal? </h2>
                <p>
                    SwasthyaNepal is the leading e-commerce platform in Pakistan, Bangladesh, Sri Lanka and Myanmar.
                    Daraz (previously Kaymu) started in Nepal in 2015 as a general marketplace for quality brands within electronics,
                    home appliances, fashion and many other categories..
                </p>
                <p>
                    Daraz is owned by CDC Group – the UK Government’s Development Finance Institution (DFI)
                    focused on supporting and developing businesses in Africa and South Asia – as well as the Asia
                    Pacific Internet Group (APACIG) which supports some of the leading internet companies in the region.
                    Founded by Rocket Internet in 2014, APACIG’s mission is to promote innovation and entrepreneurship throughout
                    Asia and the Pacific and to support the development of a vibrant online culture.
                </p>
            </div>

        </div>





    </div>
</div>

        @include('frontend.layouts._footers')
        <!-- ./footer -->
    </div>
</div>
<!--/.wrapper-->



<script src="{{ asset('frontend/design/assets/js/jquery-3.1.1.slim.min.js') }}"></script>

<script src="{{ asset('frontend/design/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/design/vendor/OwlCarousel2-2.2.1/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/design/vendor/slick-slider/slick/slick.js') }}"></script>

<script src="{{ asset('frontend/source/js/scripts.js') }}"></script>

<script>
    $(document).ready(function () {
        $(document).on('click', '.navbar-toggle', function (event) {
            event.preventDefault();
            $('body').toggleClass('open');
        });
    });
</script>
</body>
</html>