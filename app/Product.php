<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id',
        'title',
        'covor_photo',
        'price',
        'quantity',
        'selling_price',
        'expiry_date',
        'status',
        'description',
        'created_at',
        'updated_at'
    ];
}
