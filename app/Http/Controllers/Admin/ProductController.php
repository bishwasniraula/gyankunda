<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\admin\product;
use App\Model\admin\category;
use Session;
use Image;
use File;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $ActiveProducts = Product::orderBy('id','desc')
                            ->where('published',1)
                            ->get();
        $SuspendProducts = Product::orderBy('id','desc')
                            ->where('published',0)
                            ->get();
        $AllProducts = Product::orderBy('id','desc')->get();

    return view('adminDashboard.productStatus',[
        'ActiveProducts'=> $ActiveProducts,
        'SuspendProducts' => $SuspendProducts,
        'AllProducts' => $AllProducts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       
         $categories = Category::all();
         return view('adminDashboard.addproduct',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        for($counter=1;$counter<=10;$counter++){

            $j='situation'.$counter;
            $k = 'radiogrp'.$counter;

            if(!empty($request->$j) && !empty($k)){

                $stnsState[] = array('situation'=>$request->$j,'state'=>$request->$k);
            }
            
        }
        if(!empty($stnsState)){
            $situationState = json_encode($stnsState);
        }else{
            $situationState ="";
        }

        $product = new Product;

        $product->title = $request->ProductName;
        $product->slug = $request->slug;
        $product->price = $request->price;
        $product->selling_price = $request->SalePrice;
        $product->expiry_date = $request->expdate;
        $product->description = $request->description;
        $product->salt_composition = $request->composition;
        $product->basic_use = $request->basicUse;
        $product->side_effect = $request->sideEffect;
        $product->how_to_use= $request->use;
        $product->situations_states= $situationState;
        $product->manufacturing = $request->manufacturer;
        $product->distributor = $request->distributor;
        $product->category_id  = $request->ProductType;
        $product->sku = $request->SKU;
        $product->manufacturer_part = $request->manufacturerPart;
        $product->published = $request->Published;
        $product->featured = $request->Featured;
        $product->page_display = $request->PageDisplay;
        $product->tax_class = $request->TaxClass;
        $product->qauantity_discount_table = $request->QuantityDiscountTable;
        $product->show_buy_button = $request->ShowBuyButton;
        $product->requires_registration_view = $request->RequiresRegistrationToView;
        $product->hide_price_until_cart = $request->HidePriceUntilCart;
        $product->track_inventory_by_size_and_color = $request->Inventory;
        $product->size_option = $request->SizeOptionPrompt;
        $product->weight = $request->Weight;
        $product->dimention_width = $request->DimentionsWidth;
        $product->dimention_height = $request->DimentionsHeight;
        $product->dimention_depth = $request->DimentionsDepth;
        $product->quantity= $request->qty;
        $product->colors = $request->Colors;
        $product->colors_sku_modifiers = $request->ColorSKUModifier;
        $product->sizes = $request->Sizes;
        $product->size_sku_modifiers = $request->SizeSKUModifier;
        $product->status = 1;


        // save img
        if($request->hasFile('coverPhoto')){
            $img = $request->file('coverPhoto');
            $filename = time() . '.' .$img->getClientOriginalExtension();
            $location = ('swasthya/products/'.$filename);
            Image::make($img)->resize(380,380)->save($location);

            $product->cover_photo = $filename;
        }
        
        $product->save();

        Session::flash('success','New product has been successfully stored.');
        

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('adminDashboard.viewproduct',compact('product'));
        // return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $product = Product::findOrFail($id);
        $categories = Category::all();
        
        return view('adminDashboard.editproduct',['product'=>$product,'categories'=>$categories]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        for($counter=1;$counter<=10;$counter++){

            $j='situation'.$counter;
            $k = 'radiogrp'.$counter;

            if(!empty($request->$j) && !empty($k)){

                 $stnsState[] = array('situation'=>$request->$j,'state'=>$request->$k);
            }
            
        }
        if(!empty($stnsState)){
            $situationState = json_encode($stnsState);
        }else{
            $situationState ="";
        }

        $product = Product::findOrFail($id);
    
        $product->title = $request->ProductName;
        $product->slug = $request->slug;
        $product->price = $request->price;
        $product->selling_price = $request->SalePrice;
        $product->expiry_date = $request->expdate;
        $product->description = $request->description;
        $product->salt_composition = $request->composition;
        $product->basic_use = $request->basicUse;
        $product->side_effect = $request->sideEffect;
        $product->how_to_use= $request->use;
        $product->situations_states= $situationState;
        $product->manufacturing = $request->manufacturer;
        $product->distributor = $request->distributor;
        $product->category_id  = $request->ProductType;
        $product->sku = $request->SKU;
        $product->manufacturer_part = $request->manufacturerPart;
        $product->published = $request->Published;
        $product->featured = $request->Featured;
        $product->page_display = $request->PageDisplay;
        $product->tax_class = $request->TaxClass;
        $product->qauantity_discount_table = $request->QuantityDiscountTable;
        $product->show_buy_button = $request->ShowBuyButton;
        $product->requires_registration_view = $request->RequiresRegistrationToView;
        $product->hide_price_until_cart = $request->HidePriceUntilCart;
        $product->track_inventory_by_size_and_color = $request->Inventory;
        $product->size_option = $request->SizeOptionPrompt;
        $product->weight = $request->Weight;
        $product->dimention_width = $request->DimentionsWidth;
        $product->dimention_height = $request->DimentionsHeight;
        $product->dimention_depth = $request->DimentionsDepth;
        $product->quantity = $request->qty;
        $product->colors = $request->Colors;
        $product->colors_sku_modifiers = $request->ColorSKUModifier;
        $product->sizes = $request->Sizes;
        $product->size_sku_modifiers = $request->SizeSKUModifier;
        $product->status = 1;
       
        
        // save img
        if($request->hasFile('coverPhoto')){

            if($product['cover_photo']){
                $pathToImg = ('swasthya/products/').$product['cover_photo'];
                File::delete($pathToImg);
            }

            $img = $request->file('coverPhoto');
            $filename = time() . '.' .$img->getClientOriginalExtension();
            $location = ('swasthya/products/'.$filename);
            Image::make($img)->resize(380,380)->save($location);

            $product->cover_photo = $filename;
        }
        
        $product->update();

        Session::flash('success','Product has been successfully updated.');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product['cover_photo']){
            $pathToImg = public_path('admin/images/products/').$product['cover_photo'];
            File::delete($pathToImg);
        }
        $product->delete();

        Session::flash('success', 'Product has been deleted successfully.');

        return redirect()->route('product.index');

    }

    public function deleteProduct(Request $request){
        
        $id = $request->id;
        $product = Product::find($id);
        if($product['cover_photo']){
            $pathToImg = ('swasthya/products/').$product['cover_photo'];
            File::delete($pathToImg);
        }
        $product->delete();

        return response('success');
    }
}
