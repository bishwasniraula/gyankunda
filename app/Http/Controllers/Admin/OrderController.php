<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\admin\order;
use Session;
use Image;

class OrderController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $orders = Order 
                ::join('users', 'orders.user_id', '=', 'users.id')
                ->select('users.email','orders.cart','orders.payment_method','orders.total_amount','orders.total_discount','orders.created_at')
                ->getQuery()
                ->orderBy('orders.id','DES')
                ->paginate(2);
        
        // return view('admin.orders.index', compact('orders'));
        return view('adminDashboard.orders.orders', compact('orders'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
       
        $order = Order 
                ::join('products','orders.product_id', '=', 'products.id')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->select('orders.id','orders.product_title','orders.quantity','orders.price','orders.created_at','users.username','users.name','users.email')
                ->where('orders.id', '=', $id)
                ->getQuery()
                ->get();
        return view('admin.orders.show',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();

        Session::flash('success', 'Order has been deleted form system.');

        return redirect()->route('order.index');
    }
}
