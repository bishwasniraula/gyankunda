<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Model\admin\category;
use Session;
use DB;
use Image;
use File;

class CategoryController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index(){  


    $categories = DB::table('categories')->get();

    	 return view('adminDashboard.category.category',compact('categories'));

    }
    
    public function store(Request $request)
    {     

      // save img
        if($request->hasFile('coverPhoto')){
            $img = $request->file('coverPhoto');
            $filename = $request->CategoryName.'-'.time() . '.' .$img->getClientOriginalExtension();
            //$location = public_path('swasthya/products-category'.$filename);
             $location = ('swasthya/products-category/'.$filename);
            Image::make($img)->resize(360,360)->save($location);
        }
        if(!empty($filename)){
          DB::table('categories')->insert([
                "category_name"=>ucwords($request->CategoryName),
                "category_slug"=>strtolower($request->slug),
                "cover_photo"=>$filename,
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now() 
                ]);
        }else{
          DB::table('categories')->insert([
                "category_name"=>ucwords($request->CategoryName),
                "category_slug"=>strtolower($request->slug),
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now() 
                ]);
        }  
        
        Session::flash('success','New product category has been successfully added.');
        

        return redirect()->route('add-product-category.index');
    }

  
public function deleteCategory(Request $request){
        
        $id = $request->id;
        $category = Category::find($id);

        if($category['cover_photo']){
            //$pathToImg = public_path('swasthya/products-category/').$category['cover_photo'];
            $pathToImg = ('swasthya/products-category/').$category['cover_photo'];
            File::delete($pathToImg);
        }
        $category->delete();

        return response('success');
    }


}
