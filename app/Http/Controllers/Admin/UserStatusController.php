<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class UserStatusController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(){

    	$ActiveUsers = User::orderBy('id','desc')
                            ->where('is_active',1)
                            ->where('token','=',null)
                            ->get();

        $PendingUsers = User::orderBy('id','desc')
                            ->where('token','!=',null)
                            ->get();

       	$SuspendUsers = User::orderBy('id','desc')
                            ->where('is_active',0)
                            ->where('token','=',null)
                            ->get();

        $Users = User::orderBy('id','desc')->get();

    	return view('adminDashboard.admin_usersStatus',[
        'ActiveUsers'=> $ActiveUsers,
        'PendingUsers' => $PendingUsers,
        'SuspendUsers' => $SuspendUsers,
        'Users' => $Users
        ]);

    }
}
