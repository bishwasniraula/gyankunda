<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\admin\admin;

use Auth;
use Validator;
use Hash;
use Session;

class UpdatePasswordController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	return view('admin.auth.update_password');
    }

    public function updatePassword(Request $request){


    	$validator = validator::make($request->all(),[
    					'oldpassword' => 'required',
    					'password' => 'required|string|min:8|max:15|confirmed',
    					]);

    	if($validator->fails()){

    		return redirect()->route('admin/changepasword')
                        ->withErrors($validator)
                        ->withInput();
    	}

    	//$UserTB = new User();
    	$admin = Admin::find(Auth::user()->id);

    	if(Hash::check($request->oldpassword, $admin['password'])){

    		$admin->password = bcrypt($request->password);
    		$admin->update();
    		
    		Session::flash('success', 'Admin password has been changed successfully.');

    	return redirect()->route('admin/changepasword');
    	}else{

    	Session::flash('error', 'Current password didnot match !');

    	return redirect()->route('admin/changepasword');
			}
    	 
    	}
 
}
