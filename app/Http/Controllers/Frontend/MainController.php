<?php

namespace App\Http\Controllers\Frontend;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use Auth;
use Illuminate\Http\Request;
use Session;

class MainController extends Controller
{
  public function index()
  {
     $products = Product::where('featured',1)
                ->where('published',1)
                ->select('id','title','cover_photo','price','selling_price')
                ->orderBy('id','desc')
                ->get();
    return view('frontend.index', compact('products'));
  }

  public function showAllProducts()
  {
        $products = Product::all();
        
        return view('frontend.products', compact('products'));
  }



  public function proceed()
  {
    session()->flash('success', "");

    if(!Session::has('cart') ){
        return view('shop.shopping-cart');
      } else {

    if(Auth::user())
      {

      $user_id = Auth::user()->id;
      $email = Auth::user()->email;
      $name = Auth::user()->name;

      $oldCart = Session::get('cart');
      $cart = new Cart($oldCart);
      // print_r(serialize(Session::get('cart')));
      // die();
      $order = new Order();

      $order->cart = serialize($cart);
      $order->user_id = $user_id;
      $order->email = $email;
      $order->name = $name;

      $order->save();

      Session::forget('cart');
      return redirect('/')->with('success', 'Your Order(s) have been Placed. We will Contact You.');
      } else {
        return redirect('/');
      }

    }
     // return redirect('/');
  }


}
