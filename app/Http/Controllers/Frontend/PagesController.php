<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class PagesController extends Controller
{
    public function category(){

        $categories = Category::all();
    	return view('frontend.category',['categories'=>$categories]);
    }

    public function hospital(){

    	return view('frontend.hospital');
    }
    public function labtest(){

    	return view('frontend.labtest');
    }
    public function labtestSearch(){

    	return view('frontend.labtest_search');
    }
    public function packagecard(){
    	return view('frontend.packagecard');
    }
    public function doctor(){

    	return view('frontend.doctor');
    }
    public function doctorsFeild(){
    	return view('frontend.doctors-feild');
    }
    public function doctorProfile(){
    	return view('frontend.doctor-profile');
    }

}
