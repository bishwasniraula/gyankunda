<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\User;
use App\Model\user\userinfo;
use App\Model\user\billinginfo;
use App\Model\user\deliveryinfo;
use App\Model\user\prescription;
use Auth;
use Validator;
// use Image;
// use File;




class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $userInfo = User 
                ::leftJoin('userinfos', 'userinfos.uid', '=', 'users.id')
                ->leftJoin('billinginfos','billinginfos.uid', '=', 'users.id')
                ->leftJoin('deliveryinfos', 'deliveryinfos.uid', '=','users.id')
                ->select('users.email','userinfos.firstName','userinfos.lastName','userinfos.mobileNum','userinfos.email as pemail','userinfos.gender','userinfos.DoB','userinfos.notify_order_by','billinginfos.zipcode','billinginfos.address_full','billinginfos.billing_phoneno','billinginfos.inpBCity','billinginfos.Bstate','billinginfos.BillCountry','deliveryinfos.shipping_full_name','deliveryinfos.shipping_zipcode','deliveryinfos.txtShipAddress','deliveryinfos.shipping_suite_or_apt','deliveryinfos.Shipping_mobno','deliveryinfos.shipping_city','deliveryinfos.ShipState')
                ->where('users.id', Auth::user()->id)
                ->getQuery()
                ->get();



        
        return view('frontend.userprofile',compact('userInfo'));
    }
 public function UpdateuserPassword(Request $request){

        $validation = Validator::make($request->all(),[
                        'password'=>'required|min:8|max:12',
                        ]);
        if($validation->passes()){
            $request->all();

            $userPass = User::findOrFail(Auth::user()->id);
            $userPass->password = bcrypt($request->password);

           $userPass->update();
            return response()->json(['success'=>'success']);
        }return response()->json(['error'=>$validation->errors()->all()]);

    }
    public function userOrderlist(){

        $orders = Order::where('user_id',Auth::user()->id)
         ->orderBy('id','DES')
        ->get();
        
       return view('frontend.userprofile._ordshis',compact('orders'));
    }



    public function uploadPrescription(){

        $orders = Order::where('user_id',Auth::user()->id)
         ->orderBy('id','DES')
         ->select('id')
        ->get();
        return view('frontend.userprofile._uplpres',compact('orders'));
    }

    public function uploadUserPrescription(Request $request){

        $validator = Validator::make($request->all(),[
                        'ddlOrder'=>'required',
                     'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:10240'
                    ]);
        if($validator->passes()){
            $prescription = new Prescription();
            $prescription->user_id = Auth::user()->id;
            $prescription->order_id = $request->ddlOrder;

            $img = $request->file('file');
            $move = $img->move('swasthya/prescription',$img->getClientOriginalName());
             $prescription->prescription_file = $img->getClientOriginalName();
             $prescription->save();

             return response()->json(['success'=>'success']);

         }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }


       
    }

    public function accountInfo(){

       // $userInfo = User::where('id', Auth::user()->id)->get();

        $userInfo = User 
                ::leftJoin('userinfos', 'userinfos.uid', '=', 'users.id')
                ->leftJoin('billinginfos','billinginfos.uid', '=', 'users.id')
                ->leftJoin('deliveryinfos', 'deliveryinfos.uid', '=','users.id')
                ->select('users.email','userinfos.firstName','userinfos.lastName','userinfos.mobileNum','userinfos.email as pemail','userinfos.gender','userinfos.DoB','userinfos.notify_order_by','billinginfos.zipcode','billinginfos.address_full','billinginfos.billing_phoneno','billinginfos.inpBCity','billinginfos.Bstate','billinginfos.BillCountry','deliveryinfos.shipping_full_name','deliveryinfos.shipping_zipcode','deliveryinfos.txtShipAddress','deliveryinfos.shipping_suite_or_apt','deliveryinfos.Shipping_mobno','deliveryinfos.shipping_city','deliveryinfos.ShipState')
                ->where('users.id', Auth::user()->id)
                ->getQuery()
                ->get();

        return view('frontend.userprofile._accinfo',compact('userInfo'));
    }


    public function billingInfo(Request $request){

       $data = array('uid'=> Auth::user()->id,
                     'zipcode' =>$request->zipcode,
                     'address_full'=>$request->address_full,
                     'billing_phoneno'=>$request->billing_phoneno,
                     'inpBCity'=>$request->inpBCity,
                     'Bstate'=>$request->Bstate,
                     'BillCountry'=>$request->BillCountry
                 );

    $deliveryInfo = Billinginfo::updateOrCreate(['uid'=> Auth::user()->id], $data);
        
        return response('success');
 
        
    }

    public function deliveryInfo(Request $request){

    $data =array('uid'=> Auth::user()->id,
                    'shipping_full_name'=>$request->shipping_full_name,
                    'shipping_zipcode'=>$request->shipping_zipcode,
                    'txtShipAddress'=>$request->txtShipAddress,
                    'shipping_suite_or_apt'=>$request->shipping_suite_or_apt,
                    'Shipping_mobno'=>$request->Shipping_mobno,
                    'shipping_city'=>$request->shipping_city,
                    'ShipState'=>$request->ShipState
                    );

    $deliveryinfo = Deliveryinfo::updateOrCreate(['uid'=> Auth::user()->id], $data);
        
    return response('success');

    }

    public function personalInfo(Request $request){

       
        $data =array('uid'=> Auth::user()->id,
                    'firstName'=>$request->first_name,
                    'lastName'=>$request->last_name,
                    'mobileNum'=>$request->phone_number,
                    'email'=>$request->email,
                    'gender'=>$request->gender,
                    'DoB'=>$request->date_of_birth,
                    'notify_order_by'=>$request->chkSMS
                    );
       $userinfo = Userinfo::updateOrCreate(['uid'=> Auth::user()->id], $data);

        return response('success');
    }


    
}
// 