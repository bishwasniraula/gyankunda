<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
use App\Product;

class MedicineController extends Controller
{


    public function show($slug)
    {
        
        $category = Category::where('category_slug', $slug)
                    ->select('id','category_name','category_slug')
                    ->firstOrFail();
        $product = Product::where('category_id','=',$category->id)
            ->where('published',1)
            ->select('id','title','cover_photo','price','selling_price','slug')
            ->orderBy('id','desc')
            ->get();
        //return $product; 
        return view('frontend.products',['products'=>$product,'category'=>$category]);  
    }


    

   
}
