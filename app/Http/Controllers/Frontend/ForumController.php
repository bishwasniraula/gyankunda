<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForumController extends Controller
{
    public function index(){

    	return view('frontend.forum.forum_mainpage');
    }

    public function forumQAnswer(){

    	return view('frontend.forum.forum_answer');
    }
}
