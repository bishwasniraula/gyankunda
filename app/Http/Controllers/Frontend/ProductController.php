<?php

namespace App\Http\Controllers\Frontend;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use Auth;
use Illuminate\Http\Request;
use Session;

class ProductController extends Controller
{


    public function index()
    {
     $products = Product::where('featured',1)
                ->where('published',1)
                ->select('id','title','cover_photo','price','selling_price')
                 ->orderBy('id','desc')
                ->get();
    return view('frontend.index', compact('products'));
    }

    public function show($slug)
    {
    $product = Product::where('slug', $slug)->firstOrFail();

    $substitute_product = Product::where('id', '!=', $product->id)
                                  ->where('salt_composition',$product->salt_composition)
                                  ->get();

      $substitute_products_count = Product::where('id', '!=', $product->id)
                                  ->where('salt_composition',$product->salt_composition)
                                  ->count();

    return view('frontend/singleproduct',['product'=>$product,'substitute_product'=>$substitute_product,'count'=>$substitute_products_count]);

    }

    public function getAddQTY(Request $request){

      $id = $request->id;
      $qty = $request->qty;

      $product = Product::find($id);
      $oldCart = Session::has('cart') ? Session::get('cart') : null;
      $cart = new Cart($oldCart);

      $cart->bunchAdd($product, $id,$qty);

      $request->session()->put('cart', $cart);
      $cartCount = count(session()->get('cart')->items);

      return response()->json(['success'=>'success',
                                'cartCount'=>$cartCount
                        ]);

    }

    public function getAddToCart(Request $request, $id) {

      $product = Product::find($id);
      $oldCart = Session::has('cart') ? Session::get('cart') : null;
      $cart = new Cart($oldCart);
      $cart->add($product, $product->id);

      $request->session()->put('cart', $cart);
      // dd($request->session()->get('cart'));
      //return redirect()->back();
      //
      $cartCount = count(session()->get('cart')->items);
      
      return response()->json(['success'=>'success',
                                'cartCount'=>$cartCount
                        ]);
    }

    public function getReduceByOne($id)
    {
      $product = Product::findOrFail($id);
      $oldCart = Session::has('cart') ? Session::get('cart') : null;
      $cart = new Cart($oldCart);

      $cart->reduceByOne($id);

      if(count($cart->items) > 0){
      Session::put('cart', $cart);
      } else {
      Session::forget('cart');
      return redirect()->back();
      }

      $cartCount = count(session()->get('cart')->items);
        //$updateCartCount = $cartCount-1;

        if($cartCount==0){

          $totalCartItem = 0;

        }else{

          $totalCartItem = 'has';
        }

      if (array_key_exists($id, $cart->items)){
       return response()->json([
          'countItem' =>$cart->items[$id],
          'uCartCount'=>$cartCount,
          'totalCartCount'=>$totalCartItem, 
          'totalAmount' =>$cart->totalPrice,
          'totalMarketprice'=> $cart->totalMarketprice,
          'discountedPrice'=> $cart->discountedPrice
        ]); 

        }else{

           return response()->json([
          'uCartCount'=>$cartCount,
          'totalCartCount'=>$totalCartItem,
          'totalAmount' =>$cart->totalPrice,
          'totalMarketprice'=> $cart->totalMarketprice,
          'discountedPrice'=> $cart->discountedPrice
        ]); 
        }      

     

    }

    public function cartCount(){

        $cartCount = count(session()->get('cart')->items);
        $updateCartCount = $cartCount-1;

        if($updateCartCount==0){

            $totalCartItem = 0;

        }else{

          $totalCartItem = 'has';
        }


        return response()->json([
                                'totalCartCount'=>$totalCartItem
                              ]);
      }

    public function getAddByOne($id)
    {
      $product = Product::find($id);
      $oldCart = Session::has('cart') ? Session::get('cart') : null;
      $cart = new Cart($oldCart);

      $cart->addByOne($id);

      if(count($cart->items) > 0){
      Session::put('cart', $cart);
      } else {
      Session::forget('cart');
      return redirect()->back();
      }

      return response()->json([
        'countItem' =>$cart->items[$id], 
        'totalAmount' =>$cart->totalPrice,
        'totalMarketprice'=> $cart->totalMarketprice,
        'discountedPrice'=> $cart->discountedPrice
        ]);

      // return view('frontend.cart', ['products'=> $cart->items, 'totalPrice'=> $cart->totalPrice]);

    }


    public function getRemoveItem($id)
    {

      $product = Product::find($id);
      $oldCart = Session::has('cart') ? Session::get('cart') : null;
      $cart = new Cart($oldCart);

      $cart->removeItem($id);

      if(count($cart->items) > 0){
      Session::put('cart', $cart);
      } else {
      Session::forget('cart');
      }

      return response('success');

      // return view('frontend.cart', ['products'=> $cart->items, 'totalPrice'=> $cart->totalPrice]);

    }

    public function updateCart()
    {     

      if(!Session::has('cart') ){
        return view('frontend.cart', ['products' => null]);
      }
      $oldCart = Session::get('cart');
      $cart = new Cart($oldCart);

      $cartCount = count(session()->get('cart')->items);
      if($cartCount>0){
        return response()->json(['totalPrice'=>$cart->totalPrice,'totalMarketprice'=> $cart->totalMarketprice,'discountedPrice'=> $cart->discountedPrice,'cartCount'=>$cartCount]);
      }
    }

    public function getCart()
    {     

      if(!Session::has('cart') ){
        return view('frontend.cart', ['products' => null]);
      }
      $oldCart = Session::get('cart');
      $cart = new Cart($oldCart);

      //dd($cart);
      return view('frontend.cart', ['products'=> $cart->items, 'totalPrice'=> $cart->totalPrice,'totalMarketprice'=> $cart->totalMarketprice,'discountedPrice'=> $cart->discountedPrice]);
    }

    public function getCheckout()
    {
      if(!Session::has('cart') ){
        return view('frontend.cart');
      }
      $oldCart = Session::get('cart');
      $cart = new Cart($oldCart);

      $total = $cart->totalPrice;
      return view('frontend.payment', ['total' => $total]);
    }

    public function orderSummary(){

      if(!Session::has('cart') ){
        return view('frontend.cart', ['products' => null]);
      }
      $oldCart = Session::get('cart');
      $cart = new Cart($oldCart);

      
      return view('frontend.summary', ['products'=> $cart->items, 'totalPrice'=> $cart->totalPrice,'totalMarketprice'=> $cart->totalMarketprice,'discountedPrice'=> $cart->discountedPrice,'totalQty'=> $cart->totalQty]);
    }

    public function proceedPayment(){
      if(!Session::has('cart') ){
        return view('frontend.cart', ['products' => null]);
      }
      $oldCart = Session::get('cart');
      $cart = new Cart($oldCart);

      
      //calculating 13% VAT
      //$VATamount =($cart->totalPrice*1.13)-$cart->totalPrice;
      //$totalAmountwithVAT = $cart->totalPrice + $VATamount;
      //'toBePaid'=>$totalAmountwithVAT,'VATamount'=>$VATamount


      return view('frontend.mypayment', ['products'=> $cart->items, 'totalPrice'=> $cart->totalPrice,'discountedPrice'=> $cart->discountedPrice]);
    }
    public function postCheckout(Request $request)
    {

   if(!Session::has('cart') ){
        return view('shop.shopping-cart');
      } else {

    if(Auth::user())
      {

      $user_id = Auth::user()->id;

      $oldCart = Session::get('cart');
      $cart = new Cart($oldCart);
      // print_r(serialize(Session::get('cart')));
      // die();
      $order = new Order();
  
      foreach($cart->items as $pro){
        $product_id = $pro['item']['id'];
        $product_title = $pro['item']['title'];
        $product_price = $pro['item']['price'];

        $fcart[] = array("pid"=>$product_id,
                            "ptitle"=>$product_title,
                            "price"=>$product_price,
                            "qty"=>$pro['qty'],
                            "totalprice"=>$pro['price']
                            );
        

        }
      $order->user_id = $user_id;
      $order->cart =json_encode($fcart);
      $order->payment_method = $request->confirmedpaymentMethod;
      $order->total_amount = $request->talalAmount;
      $order->total_discount  = $request->totalDiscount; 
      $order->save();

      foreach($cart->items as $pro){
       $product=Product::find($pro['item']['id']);
        $product->decrement('quantity',$pro['qty']);       
        }

      Session::forget('cart');
      return redirect('/')->with('success', 'Your Order(s) have been Placed. We will Contact You.');
      } else {
        return redirect('/');
      }

      Session::forget('cart');
      return redirect()->route('product.index')->with('success', 'Successfully Purchased Product');

    }
}

}
