<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;

class VerifyController extends Controller
{
    public function verify($token){

    	User::where('token',$token)
    	->firstOrFail()
    	->update(['token'=>null]);

    	return redirect()
    		->route('userprofile')
    		->with('success', 'Account Verified.');
    }

    public function notification(){

    	return 'We have sent you confirmation link, please verified your account to login.';
    }
}
