<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Modules\Hospital\Models\MenuProductCategory as Menu;

class ProductMenuComposer
{
    public function __construct()
    {
    }

    public function compose(View $view)
    {
        $product_categories_menus=Menu::all_menus();
        
        $view->with(compact('product_categories_menus'));
    }
}
