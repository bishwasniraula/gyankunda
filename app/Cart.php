<?php

namespace App;


class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;
    public $totalMarketprice = 0;
    public $discountedPrice = 0;


    public function __construct($oldCart)
    {
        if($oldCart){
            $this->items        = $oldCart->items;
            $this->totalQty     = $oldCart->totalQty;
            $this->totalPrice   = $oldCart->totalPrice;
            $this->totalMarketprice = $oldCart->totalMarketprice;
            $this->discountedPrice = $oldCart->discountedPrice;
           
        }
    }

    public function add($item, $id)
    {
    $storedItem = ['qty' => 0, 'price' => $item->price, 'item' => $item,'selling_price'=>$item->selling_price,'discount_price'=>$item->discount_price];

        if($this->items){
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['qty']++;
        $storedItem['price'] = $item->price * $storedItem['qty'];
        $storedItem['selling_price'] = $item->selling_price * $storedItem['qty'];

        $storedItem['discount_price'] = ($item->selling_price - $item->price) * $storedItem['qty'];

        $totalDiscountPerItem = ($item->selling_price - $item->price);

               
        $this->items[$id] = $storedItem;
        $this->totalQty++;
        $this->totalPrice += $item->price;
        $this->totalMarketprice += $item->selling_price;
        $this->discountedPrice += $totalDiscountPerItem;

    }

    public function bunchAdd($item,$id,$qty)
    {

        $storedItem = ['qty' => 0, 'price' => $item->price, 'item' => $item,'selling_price'=>$item->selling_price,'discount_price'=>$item->discount_price];

        if($this->items){

            if (array_key_exists($id, $this->items)) {

                $this->totalQty -= $this->items[$id]['qty'];
                $this->totalPrice -= $this->items[$id]['price'];
                $this->totalMarketprice -= $this->items[$id]['selling_price'];
                $this->discountedPrice -= $this->items[$id]['discount_price'];
                unset($this->items[$id]);
            }
        }

        $storedItem['qty']=$qty;

        $storedItem['price'] = $item->price * $storedItem['qty'];
        $storedItem['selling_price'] = $item->selling_price * $storedItem['qty'];

        $storedItem['discount_price'] = ($item->selling_price - $item->price) * $storedItem['qty'];

        $totalDiscountPerItem = ($item->selling_price - $item->price);

               
        $this->items[$id] = $storedItem;
        $this->totalQty= $storedItem['price'];
        $this->totalPrice += $storedItem['price'];
        $this->totalMarketprice += $storedItem['selling_price'];
        $this->discountedPrice += $totalDiscountPerItem*$qty;
    }

    public function reduceByOne($id)
    {
        $this->items[$id]['qty']--;
        $this->items[$id]['price'] -= $this->items[$id]['item']['price'];
        $this->items[$id]['selling_price'] -= $this->items[$id]['item']['selling_price'];
        
        $this->totalQty--;
        $this->totalPrice -= $this->items[$id]['item']['price'];
        $this->totalMarketprice -= $this->items[$id]['item']['selling_price'];
        $totalDiscountPerItem = ($this->items[$id]['item']['selling_price'] - $this->items[$id]['item']['price']);
        $this->items[$id]['discount_price'] -=  $totalDiscountPerItem;
        $this->discountedPrice -= $totalDiscountPerItem;

        

        if( $this->items[$id]['qty'] <= 0) {
            unset($this->items[$id]);
        }

    }

    public function addByOne($id)
    {
        $this->items[$id]['qty']++;
        $this->items[$id]['price'] += $this->items[$id]['item']['price'];
        $this->items[$id]['selling_price'] += $this->items[$id]['item']['selling_price'];

        $this->totalQty++;
        $this->totalPrice += $this->items[$id]['item']['price'];
        $this->totalMarketprice += $this->items[$id]['item']['selling_price'];

        $totalDiscountPerItem = ($this->items[$id]['item']['selling_price'] - $this->items[$id]['item']['price']);

        $this->items[$id]['discount_price'] +=  $totalDiscountPerItem;

        $this->discountedPrice += $totalDiscountPerItem;


    }


    public function removeItem($id)
    {
        $this->totalQty -= $this->items[$id]['qty'] ;
        $this->totalPrice -= $this->items[$id]['price'];
        $this->totalMarketprice -= $this->items[$id]['selling_price'];
        $this->discountedPrice -= $this->items[$id]['discount_price'];
        unset($this->items[$id]);
    }
}
