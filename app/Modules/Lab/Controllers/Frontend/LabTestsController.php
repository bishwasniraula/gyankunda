<?php

namespace App\Modules\Lab\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Lab\Models\Test;
use App\Modules\Lab\Models\TestCategory;


class LabTestsController extends Controller
{
    protected $lab_test;

    public function __construct(Test $lab_test)
    {
        $this->lab_test = $lab_test;
    }

    public function search()
    {
        $request = request();
        if(count($request->all()) !=0){
            session(['search_url'=>$request->all()]);
            foreach($request->all() as $type=> $value)
            {
                $data = $this->typed_labs($type);
                $details[$type] = $data;
            }

            return view('Lab::lab.frontend.search',compact('details'));
        }

    }
    public function detail($id)
    {
        $test = $this->lab_test->findOrFail($id);
        $div_status['interpretations'] = (!empty($test->interpretations))? 1:0;
        $div_status['faqs'] = ($test->faqs->isNotEmpty())? 1 : 0;
        $div_status['inner_tests'] = ($test->inner_tests->isNotEmpty())? 1:0;
        $required_divs = array_filter($div_status, function ($value) {
            return $value ==1;
        });
        $div_col = 12/(count($required_divs)+1);
        return view('Lab::lab_test.frontend.detail',compact('test','div_col','div_status'));


    }
    public function url_remove($type,$slug)
    {
        if(\Session::has('search_url')){
            $array = session('search_url')[$type];
            unset($array[array_search($slug,$array)]);
            $search_array = array_except(session('search_url'),[$type]);
            $search_array[$type] = $array;
            $search_array = array_filter($search_array, function ($value) {
                return ! empty($value);
            });
            if(count($search_array)==0){
                return redirect()->route('lab.index');
            }
            return redirect()->route('lab.search',$search_array);


            
            
        }


    }

    public function url_add($type , $slug)
    {
        if(\Session::has('search_url')){
            if(session()->has('search_url.'.$type)){
                $search_array[$type] = session('search_url')[$type];
                $search_array[$type][] = $slug;
            }
            else{
                
                $search_array=session('search_url');
                $search_array[$type][] = $slug;
            }
           
            
            
            session(['search_url'=>$search_array]);
            return redirect()->route('lab.search',session('search_url'));

            
        }
    }

    public function index()
    {
        $radiology = TestCategory::where('name','RADIOLOGY')->get()->first();
        $pathology = TestCategory::where('name','PATHOLOGY')->get()->first();
        return view('Lab::lab.frontend.index',compact('radiology','pathology'));
    }
    public function typed_labs($type)
    {
        $request = request();
        if($request->has($type)){
            $pates = $request->all()[$type];
            foreach (array_values($pates) as $key =>$pate) {
                $test = Test::where('slug',$pate)->get()->first();
                if($test){
                    $tests[]=$test;
                    $labs[] = $test->labs;
                    if($key != 0 ){
                        $labs[0] = $labs[$key]->intersect($labs[0]);
                    }

                }
            }

            foreach($labs[0] as $lab){

                foreach($tests as $test){
                    $lab_test = $test->labs->where('id',$lab->id)->first();
                    $real_price[] = $lab_test->pivot->price;
                    $discounted_price[] = real_price($lab_test->pivot->price,$lab_test->pivot->discount);
                }
                $total_price[$lab->id] = array_sum($real_price);
                $total_discounted_price[$lab->id] = array_sum($discounted_price);
                unset($real_price);
                unset($discounted_price);
            }
            $price = ['total'=>$total_price,'real'=>$total_discounted_price];
            return ['labs'=>$labs,'tests'=>$tests,'price'=>$price];
            
            

        }
    }
}
