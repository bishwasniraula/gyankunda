<?php

Route::group(
    [
        'namespace'=>'App\Modules\Lab\Controllers\Frontend',
        'middleware'=>['web']
    ], function(){

        Route::get('/lab-test/detail/{id}', 'LabTestsController@detail')->name('test.detail');
        Route::get('/labs/search', 'LabTestsController@search')->name('lab.search');
        Route::get('/labs', 'LabTestsController@index')->name('lab.index');
        Route::get('/labs/lab-search-remove/{type}/{slug}', 'LabTestsController@url_remove')->name('lab.search.remove');
        Route::get('/labs/lab-search-add/{type}/{slug}', 'LabTestsController@url_add')->name('lab.search.add');
    
   
    });