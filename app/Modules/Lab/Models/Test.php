<?php

namespace App\Modules\Lab\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Test extends Eloquent
{

    public function labs()
    {
       return $this->belongsToMany(Lab::class)->withPivot(['price','discount','tax']);
    }
    // public function newPivot(\Illuminate\Database\Eloquent\Model $parent, array $attributes, $table, $exists)
    // {
    //     return new LabTest($parent, $attributes, $table, $exists);
    // }
    public function related_tests()
    {
        return $all_tests = $this->all();

    }
    public function category()
    {
        return $this->belongsTo(TestCategory::class,'test_category_id');
    }
    public function faqs()
    {
        return $this->hasMany(Faq::class);
    }
    public function inner_tests()
    {
        return $this->hasMany(InnerTest::class,'lab_test_id');
    }
}
