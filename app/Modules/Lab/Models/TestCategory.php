<?php

namespace App\Modules\Lab\Models;

use Illuminate\Database\Eloquent\Model;

class TestCategory extends Model
{

    protected $table = 'test_categories';
    
    public function tests()
    {
       return $this->hasMany(Test::class);
    }


    

}
