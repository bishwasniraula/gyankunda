<?php

namespace App\Modules\Lab\Models;

use Illuminate\Database\Eloquent\Model;

class InnerTest extends Model
{
    protected $table = 'inner_tests';
}
