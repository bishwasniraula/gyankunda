@extends('frontend.master')

@section('extra-header')
<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/labtest/labTest.css') }}">
@endsection
@section('main-content')

<div class="main">
    <div class="container-fluid" style="padding:0;background:#024b68;">
    </div>
    <div class="container-fluid style_search-container">
        <div class="Breadcrumbs__breadcrumb-wrapper">
            <span>
                <a class="button-text Breadcrumbs__title" href="#">Home</a>
                <span class="Breadcrumbs__seperator">›</span>
            </span>
            <span>
                <span class="Breadcrumbs__title">{{$test->name}}</span>
            </span>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-7">
                <div class="row style_test-name-box">
                    <div class="col-xs-8">
                        <div class="style_truncate style_name">{{$test->name}}</div>
                        <div class="style_truncate style_sub-name">{{$test->short_name}}</div>
                    </div>
                    
                    <div class="col-xs-4 style_add-test-box">
                            <a href="{{route('lab.search',[strtolower($test->category->name)=>[$test->slug]])}}">
                        <button class="btn btn-primary style_button ">Add Test</button>
                    </a>
                    </div>
                    
                </div>

                <div class="row" style="margin-top: 20px;">
                    <div class="style_labtest-tab">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Overview</a></li>
                            @if($div_status['interpretations']==1)
                            <li role="presentation" class="">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">
                                    Test result &amp; Interpretations
                                </a>
                            </li>
                            @endif
                            @if($div_status['faqs']==1)
                            <li role="presentation" class="">
                                <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false">
                                    Patient Concens
                                </a>
                            </li>
                            @endif
                            @if($div_status['inner_tests']==1)
                            <li role="presentation" class="">
                                <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">
                                    Tests Included
                                </a>
                            </li>
                            @endif
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">

                                <div class="row style__overview-wrapper" id="Overview">
                                    <h3 class="style_header">Overview</h3>
                                    <div class="row style__description-wrapper">
                                        <div class="col-xs-12"><h3 class="style__sub-header">Test Description</h3>
                                            <div class="style__text">
                                                {!!$test->description!!}
                                            </div>
                                        </div>
                                        @if(!empty($test->test_reasons))
                                        <div class="col-xs-12">
                                            <h3 class="style__sub-header style__mt-12">Why Get Tested</h3>
                                            <div class="style__text">
                                                {!!$test->test_reasons!!}
                                            </div>
                                        </div>
                                        @endif
                                        @if(!empty($test->precautions))
                                        <div class="col-xs-12 style__precautions-wrapper">
                                            <h3 class="style__sub-header style__precautions-sub-header">Precautions</h3>
                                            <p class="style__precautions">{!!$test->precautions!!}
                                            </p>
                                        </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="row style__test-results-wrapper style__mt-12">
                                    <div class="col-xs-12 style__interpretation-wrapper">
                                        <h3 class="style_header">Interpretations</h3>
                                        <div class="style__text">
                                            {!!$test->interpretations!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                                <div class="row style__patient-wrapper style__mt-12">
                                    <div class="col-xs-12 style__patient-concerns">
                                        <h2 class="style_header">Patient Concerns</h2>
                                    </div>
                                    <div class="col-xs-12 style__faqs-wrapper">
                                        <h3 class="style__sub-header">Frequently Asked Questions</h3>
                                        @foreach($test->faqs as $faq)
                                        <div class="col-xs-12 style__qa-wrapper">
                                            <div class="style__questions">
                                                Q.{{$faq->question}}
                                            </div>
                                            <div class="style__text">{{$faq->answer}}</div>
                                        </div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="settings">
                                <div class="row style_test-results-wrapper style__mt-12" id="Tests Included">
                                    <div class="col-xs-12 style_test-interpretations">
                                        <div>
                                            <h3 class="style_header">Tests Included
                                                <span class="style_included-tests">{{count($test->inner_tests)}}</span>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 style_interpretation-wrapper">
                                        <div>
                                            <ul class="style_none-lists">
                                                @foreach($test->inner_tests as $inner)
                                                <li>
                                                    <div class="style_collapsible-header">
                                                        {{$inner->name}}
                                                    </div>
                                                    <div class="collapsible-body style_collapsible-body">
                                                        <div class="style_sub-coll-body">

                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-5">
                <div class="row">

                <span class="style_title">
                    provided by {{count($test->labs)}} Labs 
                </span>
                    <div class="style_list">
                        <div class="style_container">

                        @foreach($test->labs as $lab)
                            <div class="style_card">
                                <div class="style_lab-wrapper">
                                    <div class="row style_lab-info-wrapper">
                                        <div class="col-xs-6 style_lab-name">
                                            <span>{{$lab->name}}</span>
                                            <div class="row style_lab-ratings-wrapper">
                                                <div class="col-xs-12 pad-0">
                                                <span class="style_rating">
                                                    <div class="StarRating_rating-green StarRating_star-rating">
                                                        <span>4.7</span>
                                                        <i class="fa fa-star StarRating_rating-star" aria-hidden="true"></i>
                                                    </div>
                                                </span>
                                                    <span class="style_accreditation">{{$lab->certification}}</span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-6 style_price-section">
                                            @if($lab->pivot->discount!=0)
                                            <span class="style_actual-price"> Rs {{$lab->pivot->price}}</span>
                                            @endif
                                            <span class="style_offered-price">Rs {{real_price($lab->pivot->price,$lab->pivot->discount)}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row style_select-lab">
                                    <div class="col-xs-6 text-right col-xs-offset-6">
                                        <span>
                                            <span class="style_text-button style_desktop-view">SELECT LAB</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="row">
                    <span class="style_title">
                        related tests
                    </span>
                    <div class="style_lists">
                        <div class="style_popular-test">
                            @foreach($test->related_tests() as $r_test)
                             @if($test->id != $r_test->id)
                            <div class="style_test-card">
                                <div class="style_popular-test-div">
                                    <div>
                                        <a href="{{route('lab.search',[strtolower($r_test->category->name)=>[$r_test->slug]])}}">

                                            <div class="col-xs-11 style_pointer">
                                                <div class="style_truncate style_name style_margin-top-0">{{$r_test->name}}</div>
                                                <div class="style_truncate style_sub-name">{{$r_test->short_name}}</div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xs-1 style_info-icon">
                                        <a href="{{route('test.detail',$r_test->id)}}">
                                            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection