<div class="col-sm-{{(count($details)==1)? 8 : 6}}">
        <span class="style_title">
            select lab for {{($type=='radiology')? 'Lab Visit' : 'Home sample collection'}}
        </span>
        <div class="style_list">
            <div class="style_container">

                    @foreach($data['labs'][0] as $lab)
                    <div class="style_card">
                        <div class="style_lab-wrapper">
                            <div class="row style_lab-info-wrapper">
                                <div class="col-xs-6 style_lab-name">
                                    <span>{{$lab->name}}</span>
                                    <div class="row style_lab-ratings-wrapper">
                                        <div class="col-xs-12 pad-0">
                                        <span class="style_rating">
                                            <div class="StarRating_rating-green StarRating_star-rating">
                                                <span>4.7</span>
                                                <i class="fa fa-star StarRating_rating-star" aria-hidden="true"></i>
                                            </div>
                                        </span>
                                            <span class="style_accreditation">{{$lab->certification}}</span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-6 style_price-section">
                                    @if($data['price']['total'][$lab->id] !=$data['price']['real'][$lab->id])

                                    <span class="style_actual-price"> Rs {{$data['price']['total'][$lab->id]}}</span>
                                    @endif
                                    <span class="style_offered-price">Rs {{$data['price']['real'][$lab->id]}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row style_select-lab">
                            <div class="col-xs-6 text-right col-xs-offset-6">
                                <span>
                                    <span class="style_text-button style_desktop-view">SELECT LAB</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    @endforeach
            </div>
        </div>
    </div>