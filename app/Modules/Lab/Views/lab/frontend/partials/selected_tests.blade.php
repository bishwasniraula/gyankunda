<div class="style_pathology-added">
    @if(request()->has($type) && count(request()->all()[$type]) !=0)
        <div class="style_label">
            <label for="">{{ucwords($type)}}</label>
        </div>
        @foreach($data['tests'] as $test)
            <div class="style_in-process-to-cart">
                <span>{{$test->name}}
                    <a href="{{route('lab.search.remove',['type'=>$type,'slug'=>$test->slug])}}"> 
                        <i class="fa fa-close" aria-hidden="true" style="color:white"> </i> 
                    </a>
                </span>
            </div>
        @endforeach    
    @endif
</div>