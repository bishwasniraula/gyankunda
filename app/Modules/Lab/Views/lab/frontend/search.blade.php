@extends('frontend.master')

@section('extra-header')
    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/labtest/labTest.css') }}">
@endsection
@section('main-content')
    <div class="main">
        <div class="container-fluid" style="padding:0;background: #042430">
            <div class="row">
                @foreach($details as $type =>$data)
                    
                        <div class="col-sm-{{(count($details)==1)? 12 : 6}} col-xs-12 " style="padding:0" >
                            @include('Lab::lab.frontend.partials.selected_tests')
                        </div>
                @endforeach
            </div>
        </div>
    
        <div class="container-fluid style_search-container">
            <div class="Breadcrumbs__breadcrumb-wrapper">
                <span>
                    <a class="button-text Breadcrumbs__title" href="#">Home</a>
                    <span class="Breadcrumbs__seperator">›</span>
                </span>
                <span>
                    <span class="Breadcrumbs__title">Search</span>
                </span>
            </div>
            <div class="row">
            @foreach($details as $type =>$data)

                        @include('Lab::lab.frontend.partials.lab_box_view')
                    
            @endforeach

            @if(count($details)==1)
                <div class="col-sm-4">
                    <div class="row">
                        <span class="style_title">
                            test information
                        </span>
                        <div class="style_list">
                            <div class="style_container">
                                @foreach(reset($details)['tests'] as $k => $test)
                                    <div class="style_card">
                                        <div class="style_lab-wrapper">
                                            <div class="collapsible-container style_lab-info-wrapper">
                                                <div class="style_collapsible-header">
                                                    <span>{{$test->name}}</span>
                                                    <a href="#{{$test->slug}}" data-toggle="collapse">
                                                        <span class="arrow-down"></span>
                                                    </a>
                                                </div>
                                                <div class="style_collapsible-footer">
                                                    <div class="style_sub-coll-body  collapse {{($k==0)? 'in':''}}" id="{{$test->slug}}">
                                                        <div>
                                                            {{str_limit($test->description,130,'....')}}<a href="{{route('test.detail',$test->id)}}">Read More</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <span class="style_title">
                            related tests
                        </span>
                        <div class="style_lists">
                            <div class="style_popular-test">
                                    @foreach($test->related_tests() as $r_test)
                                    @if($test->id != $r_test->id)
                                   <div class="style_test-card">

                                       @if(session()->has('search_url.'.strtolower($r_test->category->name)) && in_array($r_test->slug,session('search_url')[strtolower($r_test->category->name)]))
                                       <div class="style_popular-test-div" style="background-color: rgb(113, 187, 192)">
                                            <div>
                                                
                                                    <a href="{{route('lab.search.remove',['type'=>strtolower($r_test->category->name),'slug'=>$r_test->slug])}}">
                                       @else
                                       <div class="style_popular-test-div">
                                            <div>
                                                
                                                <a href="{{route('lab.search.add',['type'=>strtolower($r_test->category->name),'slug'=>$r_test->slug])}}">
                                       @endif
                                                     <div class="col-xs-11 style_pointer">
                                                       <div class="style_truncate style_name style_margin-top-0">{{$r_test->name}}</div>
                                                       <div class="style_truncate style_sub-name">{{$r_test->short_name}}</div>
                                                   </div>
                                               </a>
                                           </div>
                                           <div class="col-xs-1 style_info-icon">
                                               <a href="{{route('test.detail',$r_test->id)}}">
                                                   <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                               </a>
                                           </div>
                                       </div>
                                   </div>
                                   @endif
                                   @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
    </div>

@endsection
