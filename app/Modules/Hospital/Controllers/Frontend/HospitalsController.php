<?php

namespace App\Modules\Hospital\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Hospital\Models\Hospital;

class HospitalsController extends Controller
{
    protected $hospital;

    public function __construct(Hospital $hospital)
    {
        $this->hospital = $hospital;
    }
    public function profile($id)
    {
        $hospital = $this->hospital->findOrFail($id);
        return view('Hospital::hospital.frontend.profile',compact('hospital'));

    }
    public function index()
    {
        $hospitals = $this->hospital->all();
        return view('Hospital::hospital.frontend.index',compact('hospitals'));

    }
}
