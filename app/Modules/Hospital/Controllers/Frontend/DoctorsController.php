<?php

namespace App\Modules\Hospital\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Hospital\Models\Doctor;

class DoctorsController extends Controller
{
    protected $doctor;

    public function __construct(Doctor $doctor)
    {
        $this->doctor = $doctor;
    }
    public function profile($id)
    {
        $doctor = $this->doctor->findOrFail($id);
        return view('Hospital::doctor.frontend.profile',compact('doctor'));

    }
}
