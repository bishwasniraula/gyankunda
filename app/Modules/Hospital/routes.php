<?php

Route::group(
    [
        'namespace'=>'App\Modules\Hospital\Controllers\Frontend',
        'middleware'=>['web']
    ], function(){

        Route::get('/doctor/profile/{id}', 'DoctorsController@profile')->name('doctor.profile');
        Route::get('/hospital/profile/{id}', 'HospitalsController@profile')->name('hospital.profile');
        Route::get('/hospitals', 'HospitalsController@index')->name('hospital.index');
    
   
    });