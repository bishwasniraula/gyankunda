<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class MenuProductCategory extends Model
{

    public $inner_categories =[]; // Third Level categories
    protected $table = 'product_menu_categories';

    public function categories()
    {
       return $this->hasMany(self::class,'parent_id');
    }

    public static function all_menus()
    {

        $first_level_menus =  self::where(['parent_id'=>null,'visibility'=>1])->orderBy('position')->with('categories')->get();
        $second_level_menus =  self::where('parent_id','!=',null)->where('visibility',1)->orderBy('position')->with('categories')->get();

        foreach ($first_level_menus as $key => $menu) {
                
            if($menu->categories->isNotEmpty()){

                foreach ($menu->categories as $inner_key => $inner_menu) {

                    $nested_menus = $second_level_menus->where('id',$inner_menu->id)->first();
                    if($nested_menus){

                        $inner_menu->inner_categories = $nested_menus->categories ;
                    };                      
                }  
            }
        }

        return $first_level_menus;
    }


}
