<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Hospital extends Model
{
    protected $fillable=['opening_hours'];

    protected $appends=['total_years'];

    public function doctors()
    {
       return $this->belongsToMany(Doctor::class)->withPivot(['fee']);
    }

    public function banners()
    {
        return $this->hasMany(HospitalBanner::class);
    }

    public function getOpeningHoursAttribute()
    {
        $array =  json_decode($this->attributes['opening_hours'],true);
        $data['type'] = $array['type'];
        $data['week'] = number_to_day($array['week']['start']).'-'.number_to_day($array['week']['end']);
        $start_time = new Carbon($array['day']['start']);
        $end_time = new Carbon($array['day']['end']);
         if($end_time->diffInHours($start_time)==24)
         {
            $data['day'] = 'Open 24 Hours';
         }else{
            $data['day'] = $array['day']['start'].'-'.$array['day']['end'];
         }
        
        $today = Carbon::now()->dayOfWeek;
        $tomorrow = Carbon::now()->dayOfWeek+1;
        $now = Carbon::now()->addHours(5)->addMinutes(45);
        if($array['week']['start']-1<= $today && $today<=$array['week']['end']-1){
            $data['status']['today']=1;
            if($now->between($start_time,$end_time)){
                $data['status']['now']=1;
            }
            else{
                $data['status']['now']=0;
            }
        }else{
            $data['status']['today']=0;
            $data['status']['now']=0;
        }
        if($array['week']['start']-1<=$tomorrow && $tomorrow<=$array['week']['end']-1){
            $data['status']['tomorrow']=1;
        }else{
            $data['status']['tomorrow']=0;
        }
        return $data;

    }

    public function getTotalYearsAttribute()
    {
        return Carbon::now()->year -$this->attributes['established_date'];
    }
}
