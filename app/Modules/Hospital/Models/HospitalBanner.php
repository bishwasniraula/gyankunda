<?php

namespace App\Modules\Hospital\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalBanner extends Model
{
    protected $table = 'hospital_banners';
}
