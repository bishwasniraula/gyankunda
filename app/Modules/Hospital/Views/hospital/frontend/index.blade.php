@extends('frontend.master')

@section('extra-header')
<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/hospital/hospital.css') }}">
@endsection
@section('main-content')

<div class="pure-g o-page-container c-listing container custom-top-margin">
    <div class="row">
        <div class="pure-u-18-24 c-listing__left col-md-8 ">    
                <div class="u-clearfix">
                        <div class="u-grey_3-text u-cushion--vertical u-d-inlineblock u-spacer--right-less">
                            
                            <h1  class="u-spacer--left-thin u-bold u-d-inlineblock u-smallest-font">Hospitals in Kathmandu</h1>
                        </div>
                </div>
            <div>
                @foreach($hospitals as $hospital)
                <!--        start here-->
                <div  class="u-cushion u-white-fill u-normal-text o-card o-card--separated c-list-card">
                    <span></span>
                    <div class="c-card">
                        <div class="pure-g c-card__body row">
                            <div class="pure-u-17-24 col-md-8">
                                <div class="o-media">
                                    <div class="o-media__object img-thumbnail">
                                        <div class="u-spacer--right c-card__left c-card__photo ">
                                            <div class="LazyLoad is-visible" >
                                                <img src="{{$hospital->profile_picture}}">
                                            </div>
                                            <span class="u-d-inlineblock u-spacer--top-less" ></span>
                                        </div>
                                    </div>
                                    <div class="o-media__body u-cushion--right" >
                                        <div class="c-card-info">
                                            <a href="{{route('hospital.profile',$hospital->id)}}" class="u-color--primary">
                                                <h2 class="u-title-font u-c-pointer u-bold u-color--primary">{{$hospital->name}}</h2>
                                            </a>
                                            <div class="c-card-info__item"  >
                                                <span >Multi-speciality</span>
                                                <span class="u-spacer--left-thin">
                                                    <span>Hospital</span>
                                                </span></div><div class="c-card-info__item" >
                                                <span>{{count($hospital->doctors)}}
                                                </span><span>Doctors</span>
                                            </div>
                                            <div class="c-card-info__item u-spacer--top-thin">
                                                <div><div>
                                                    @foreach($hospital->doctors as $doctor)
                                                        <div class="LazyLoad is-visible c-carousel__lazy" style="height:36px;width:36px;">
                                                            <img class="c-carousel__item" style="border-radius: 8px" src="{{$doctor->profile_picture}}">
                                                        </div>
                                                    @endforeach
                                                    </div>
                                                    <noscript ></noscript>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pure-u-7-24 col-md-4" >
                                <div class="c-card-info">
                                    <div class="u-spacer--bottom-less">
                                        <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                        <a href="#" >
                                            21
                                            <span>Feedback</span>
                                        </a>
                                    </div>
                                    <div class="u-spacer--bottom-less">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span class="u-color--green">
                                            <span>Open 24 x 7</span>
                                        </span>
                                    </div>
                                    <div class="u-spacer--bottom-less">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <div class="u-d-inlineblock">
                                            <a href="#">
                                                <span >{{$hospital->location}}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pure-g u-cushion--top-less">
                            <div class="o-media">
                                <div class="o-media__object o-media--middle pure-u-7-24 col-md-offset-4 ">
                                    <div class="c-card-info-section" style="text-align: right">
                                        <div class="pure-g">
                                            <div class="pure-u-1">
                                                <button class="c-btn--dark u-t-capitalize u-bold" data-toggle="collapse" data-target="#demo{{$hospital->id}}">
                                                    <span>Call Now</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="c-vn c-vn--fadein collapse" id="demo{{$hospital->id}}">
                                        <div class="c-vn__info">
                                            <span>Ph: </span>
                                            <span class="c-vn__number">{{$hospital->phones}}</span>
                                        </div>
                                        <div class="c-vn__message" data-qa-id="va_disclaimer_text">
                                            <p>
                                                <span>Note: This call may be recorded for quality control purpose and call recording may be shared with the center you are trying to connect.</span>
                                            </p>
                                            <p >
                                                <span>By calling this number, you agree to the</span>
                                                <a class="u-t-link" target="_blank" href="#">
                                                    <span>Terms &amp; Conditions</span>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--ends here-->
                @endforeach
    
               <div class="pure-g u-cushion--vertical" style="display: flex">

                   <div class="pure-u-12-24" style="text-align: right">
                       <ul class="c-paginator">
                           <li class="disabled">
                               <a href="#" >Previous</a>
                           </li>
                           <li class="active">
                               <a href="#" >1</a>
                           </li>
                           <li class="">
                               <a href="#">2</a>
                           </li>
                           <li class="">
                               <a href="#" >Next</a>
                           </li>
                       </ul>
                   </div>
               </div>
            </div>
        </div>
        <div class="pure-u-6-24 c-listing__right col-md-4">
            <div class="u-spacer--medium-bottom">
                <div class="u-grey_3-text">
                    <div class="">
                        <h2 class="u-bold u-smallest-font u-spacer--bottom-less">Most searched localities in Kathmandu</h2>
                        <a href="/bangalore/hospitals/all-hospitals/vijayanagar" class="u-grey_3-text u-spacer--bottom-thin u-smallest-font u-d-block" >Hospitals in baneshwor</a>
                        <a href="/bangalore/hospitals/all-hospitals/rajajinagar" class="u-grey_3-text u-spacer--bottom-thin u-smallest-font u-d-block" >Hospitals in koteshwor</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection