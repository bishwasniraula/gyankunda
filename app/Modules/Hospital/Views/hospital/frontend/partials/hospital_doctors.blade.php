<div role="tabpanel" class="tab-pane" id="profile">
        <div class="specialities_container">
            <span class="specialities_header">filter specialists</span>
            <div class="specialities_items">
                <a href="javascript:void(0)">
                    <span class="specialitiesItem_item item_selected">All&nbsp;(<span class="specialitiesItem_item-count ">{{count($hospital->doctors)}}</span>)</span>
                </a>
                <a href="javascript:void(0)">
                    <span class="specialitiesItem_item" id="dentist">Dentist&nbsp;(<span class="specialitiesItem_item-count ">5</span>)</span>
                </a>
                <a href="javascript:void(0)">
                    <span class="specialitiesItem_item">Physician&nbsp;(<span class="specialitiesItem_item-count">6</span>)</span>
                </a>
            </div>
        </div>
        <div class="doctorProfile_cards-list">
            @foreach($hospital->doctors as $doctor)
            <!--       doctor profile card start here-->
            <div class="doctorProfile_cards-container">
                <div class="doctorProfile_card">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="doctorProfile_media">
                                <div class="img-thumbnail">
                                    <div class="doctorProfile_card-photo ">
                                        <img src="{{$doctor->profile_picture}}">
                                    </div>
                                </div>
                                <div class="doctorProfile_card-info">
                                    <a href="javascript:void(0)" class="u-color--primary">
                                        <span class="doctorProfile_doctor-name truncate">{{$doctor->name}}</span>
                                    </a>
                                    <span class="doctorProfile_doctor-qualification truncate doctor-capitalize">{{$doctor->graduation}}</span>
                                    <div class="doctorProfile_doctor-experience doctor-capitalize">
                                        <span class="truncate">{{$doctor->experience}} years experience</span>
                                    </div>
                                    <div class="doctorProfile_doctor-specialities truncate doctor-capitalize">
                                        <span>{{$doctor->occupation}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="doctorProfile_right-side">
                                <div class="doctorProfile_doctor-ratings">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                    <span style="color: limegreen;">94%</span>
                                </div>
                                <div class="doctorProfile_doctor-fee"><i class="fa fa-money" aria-hidden="true"></i>
                                    Rs.<span>{{$doctor->pivot->fee}}</span>
                                </div>
                                <div>
                                    <i class="fa fa-clock-o doctorProfile_time-icon" aria-hidden="true"></i>
                                    <div class="doctorProfile_schedule-info">
                                        <div class="doctorProfile_doctor-visiting-day">Sun-Fri</div>
                                        <div class="doctorProfile_doctor-visiting-hour">10:30AM-1:00PM</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pure-g u-cushion--top-less">
                        <div class="o-media">
                            <div class="row ">
                                <div style="text-align: center">
                                    <button class="btn btn-primary" id="fadeinButton">
                                        <span>Call Now</span>
                                    </button>
                                </div>
                            </div>
                            <div>
                                <div class="doctorProfile_card-fadein" id="fadein" style="display: none">
                                    <div class="doctorProfile_card-number">
                                        <span>Ph: </span>
                                        <span class="c-vn__number">977 9808883403</span>
                                    </div>
                                    <div class="doctorProfile_card-message">
                                <span>Note: This call may be recorded for quality control purpose and call recording may be shared with the center you are trying to connect.
                                </span>
                                        <p>
                                            <span>By calling this number, you agree to the</span>
                                            <a class="term&amp;condition_link" target="_blank" href="#">
                                                <span>Terms &amp; Conditions</span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--ends here-->
            <!--       doctor profile card start here-->
            @endforeach
        </div>

    </div>