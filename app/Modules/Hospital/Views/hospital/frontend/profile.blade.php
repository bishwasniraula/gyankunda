@extends('frontend.master')

@section('extra-header')
<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/doctorProfile/style.css') }}">
@endsection
@section('main-content')
<div class="main" style="padding-left:80px">
<div class="container doctorprofile-container">
        <div class="row doctorheader-container">
            <div class="col-md-12">
                <div class="doctorimagebanner-section">
                    <img src="{{$hospital->cover_picture}}" class="bannerpicture" alt="">
                    <div class="social-link">
                            <ul>
                                <li><a href=""><i class="fa fa-globe" aria-hidden="true"></i></a></li>
                                <li><a href="{{$hospital->facebook_link}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            </ul>
                    </div>
                </div>
                <div class="doctorcontext-section">
                    <p><span>{{
                        $hospital->short_detail
                    }}
                    </span></p>
    <!--                <a href="" class="updatePhoto">-->
                        <img src="{{$hospital->profile_picture}}" class="profilepicture" alt="">
    <!--                    <div class="updatepic"></div>-->
    <!--                </a>-->
                </div>
            </div>
        </div>
        <div class="row doctorbody">
            <div class="col-md-8 doctorbody__left">
                <div class="doctorinfo__section">
                    <div class="row">
                        <div class="col-sm-6 col-md-12">
                            <h3 class="hospitalName" style="background: #024b68;padding: 10px;color: white;width: 100%;">
                                {{$hospital->name}}
                            <div class="arrow"></div>
                            </h3>
                        </div>
                        <div class="col-md-6">
    
                        </div>
                    </div>
                    <div>
    <!--                    tab section-->
                        <div>
    
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home Details</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Doctors Profile</a></li>
                            </ul>
    
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <div class="card-basic-info">
                                        <h5>Business Information</h5>
                                        <table class="table table-hover">
                                            <tbody>
                                            <tr class="one">
                                                <td width="33%"><strong>Business Category</strong><span class="float-right">:</span></td>
                                                <td><a href="">{{$hospital->business_category}}</a></td>
                                            </tr>
                                            <tr>
                                                <td width="33%"><strong>Hours</strong><span class="float-right">:</span></td>
                                                <td>
                                                    <ul>
                                                        <li>{{($hospital->opening_hours['type'])}}</li>
                                                        <li>{{($hospital->opening_hours['week'])}}</li>
                                                        <li>{{($hospital->opening_hours['day'])}}</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="33%"><strong>Services/Product</strong><span class="float-right">:</span></td>
                                                <td>
                                                    {!!$hospital->services!!}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="33%"><strong>Payment method</strong><span class="float-right">:</span></td>
                                                <td>{{$hospital->payment_methods}}</td>
                                            </tr>
                                            <tr>
                                                <td width="33%"><strong>Location</strong><span class="float-right">:</span></td>
                                                <td>{{$hospital->location}}</td>
                                            </tr>
                                            <tr>
                                                <td width="33%"><strong>Other Link</strong><span class="float-right">:</span></td>
                                                <td><a href="">{{$hospital->other_link}}</a></td>
                                            </tr>
                                            <tr>
                                                <td width="33%"><strong>Categories</strong><span class="float-right">:</span></td>
                                                <td>
                                                    {!!$hospital->categories!!}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="33%"><strong>Other Information</strong><span class="float-right">:</span></td>
                                                <td>
                                                        {!!$hospital->other_information!!}
                                                </td>
                                                
    
    
                                            </tr>
                                            <tr>
                                                <td width="33%"><strong>Banner</strong><span class="float-right">:</span></td>
                                                <td>
                                                    <div class="row">
                                                        @foreach($hospital->banners as $banner)
                                                        <div class="col-xs-6 col-md-4">
                                                                <a href="#" class="thumbnail">
                                                                    <img src="{{$banner->image_url}}" alt="...">
                                                                </a>
                                                        </div> 
                                                        @endforeach
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
    
                                    </div>
                                    <div class="card-job-specification">
    
    
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <div class="specialities_container">
                                        <span class="specialities_header">filter specialists</span>
                                        <div class="specialities_items">
                                            <a href="javascript:void(0)">
                                                <span class="specialitiesItem_item item_selected">All&nbsp;(<span class="specialitiesItem_item-count ">{{count($hospital->doctors)}}</span>)</span>
                                            </a>
                                            <a href="javascript:void(0)">
                                                <span class="specialitiesItem_item" id="dentist">Dentist&nbsp;(<span class="specialitiesItem_item-count ">5</span>)</span>
                                            </a>
                                            <a href="javascript:void(0)">
                                                <span class="specialitiesItem_item">Physician&nbsp;(<span class="specialitiesItem_item-count">6</span>)</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="doctorProfile_cards-list">
                                        @foreach($hospital->doctors as $doctor)
                                        <!--       doctor profile card start here-->
                                        <div class="doctorProfile_cards-container">
                                            <div class="doctorProfile_card">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="doctorProfile_media">
                                                            <div class="img-thumbnail">
                                                                <div class="doctorProfile_card-photo ">
                                                                    <img src="{{$doctor->profile_picture}}">
                                                                </div>
                                                            </div>
                                                            <div class="doctorProfile_card-info">
                                                                <a href="{{route('doctor.profile',$doctor->id)}}" class="u-color--primary">
                                                                    <span class="doctorProfile_doctor-name truncate">{{$doctor->name}}</span>
                                                                </a>
                                                                <span class="doctorProfile_doctor-qualification truncate doctor-capitalize">{{$doctor->graduation}}</span>
                                                                <div class="doctorProfile_doctor-experience doctor-capitalize">
                                                                    <span class="truncate">{{$doctor->experience}} years experience</span>
                                                                </div>
                                                                <div class="doctorProfile_doctor-specialities truncate doctor-capitalize">
                                                                    <span>{{$doctor->occupation}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="doctorProfile_right-side">
                                                            <div class="doctorProfile_doctor-ratings">
                                                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                                <span style="color: limegreen;">94%</span>
                                                            </div>
                                                            <div class="doctorProfile_doctor-fee"><i class="fa fa-money" aria-hidden="true"></i>
                                                                Rs.<span>{{$doctor->pivot->fee}}</span>
                                                            </div>
                                                            <div>
                                                                <i class="fa fa-clock-o doctorProfile_time-icon" aria-hidden="true"></i>
                                                                <div class="doctorProfile_schedule-info">
                                                                    <div class="doctorProfile_doctor-visiting-day">Sun-Fri</div>
                                                                    <div class="doctorProfile_doctor-visiting-hour">10:30AM-1:00PM</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pure-g u-cushion--top-less">
                                                    <div class="o-media">
                                                        <div class="row ">
                                                            <div style="text-align: center">
                                                                <button class="btn btn-primary" id="fadeinButton">
                                                                    <span>Call Now</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="doctorProfile_card-fadein" id="fadein" style="display: none">
                                                                <div class="doctorProfile_card-number">
                                                                    <span>Ph: </span>
                                                                    <span class="c-vn__number">977 9808883403</span>
                                                                </div>
                                                                <div class="doctorProfile_card-message">
                                                            <span>Note: This call may be recorded for quality control purpose and call recording may be shared with the center you are trying to connect.
                                                            </span>
                                                                    <p>
                                                                        <span>By calling this number, you agree to the</span>
                                                                        <a class="term&amp;condition_link" target="_blank" href="#">
                                                                            <span>Terms &amp; Conditions</span>
                                                                        </a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--ends here-->
                                        <!--       doctor profile card start here-->
                                        @endforeach
                                    </div>
    
                                </div>
    
                            </div>
                            <!--                 end   tab section-->
                        </div>
    
                    </div>
                </div>
            </div>
            <div class="col-md-4 doctorbody__right">
                <section class="primary-info">
                    <section class="ratings">
                        <a href="#reviews" class="yp-ratings" style="align-items: center;display: flex;">
                            <div class="rating-indicator five  " style="font-size: 24px;color:#FFC200;">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star-half-o" aria-hidden="true"></i>
                            </div>
                            <span class="count">(1 Review)</span>
                        </a>
                    </section>
                    <div class="contact">
                        <i class="fa fa-location-arrow" aria-hidden="true" style="margin-right: 10px ;float: left;"></i>
                        <p class="address">
                            <span>{{$hospital->location}}</span>
                        </p>
                        <div class="clearfix"></div>
                        <p class="phone" style="font-size: 20px;">
                            <i class="fa fa-phone" aria-hidden="true" style="margin-right: 10px"></i>{{$hospital->phones}}</p>
                        <div class="time-info">
                            @if($hospital->opening_hours['status']['now']==1)
                            <div class="status-text open now">Open NOW</div>
                            @else
                            <div class="status-text closed now">Closed NOW</div>
                            @endif
                            <div>Today: {{($hospital->opening_hours['status']['today']==1) ? 'Open':'Closed'}}</div>
                            <div>Tomorrow: {{($hospital->opening_hours['status']['tomorrow']==1) ? 'Open':'Closed'}}</div>
                        </div>
                    </div>
                    <div class="contact-bottom">
    
                        <div class="preferred-label">
                            <p>PREFERRED</p>
                        </div>
                        <div class="years-in-business">
                            <div class="count">
                                <div class="number">{{$hospital->total_years}}</div>
                            </div>
                            <span>YEARS <br> IN BUSINESS</span>
                        </div>
                        <div class="accepting-new-patients">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 19" width="14" height="19"><g id="new-patient-icon" fill="none"><path stroke="#555" d="m0.5 2.5l13 0 0 16 -13 0 0-16z"></path><path fill="#555" d="m3 6l8 0 0 1 -8 0 0-1zm0 2l8 0 0 1 -8 0 0-1zm2-6l-2 0 0 2 8 0 0-2 -2 0a2 2 0 1 0-4 0zm1 10l-2 0 0 2 2 0 0 2 2 0 0-2 2 0 0-2 -2 0 0-2 -2 0 0 2z"></path></g></svg>
                            <span>ACCEPTING <br> NEW PATIENTS</span>
                        </div>
                    </div>
                </section>
                <div class="map-section">
                    <div style="width: 100%">
                        <iframe src="{{$hospital->google_map_link}}" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen="">
    
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>
</div>
@endsection
        @section('script')
        <script>
                $(document).ready(function(){
            //        selected specialities section
                   $('.specialities_items span').on('click',function(){
                       $('.specialities_items span').removeClass('item_selected');
                       $(this).addClass('item_selected');
                   });
            //       filter physician
                    $('#dentist').on('click',function(){
                       var val=$(this).val();
                       console.log(val);
            //            var value='dentist';
            //            $('.doctorProfile_cards-list').filter(function(){
            //                $(this).toogle($(this).text().toLowerCase().indexOf(value) > -1)
            //            })
                    });
                });
            </script>
        @endsection