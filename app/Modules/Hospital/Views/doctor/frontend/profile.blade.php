@extends('frontend.master')

@section('extra-header')
<link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/doctor/doctor-profile.css') }}">
@endsection
@section('main-content')

    <div class="wrapper" id="site-wrapper" >
        <div class="content-wrapper" style="margin-left:0px;">

            <div class="row">
                <div class="col-md-2 doctor-profile__image">
                    <img src="{{$doctor->profile_picture}}">
                </div>
                <div class="col-md-6 doctor-profile__details">
                    <ul>
                        <li class="doctor-name doctor-capitalize">
                                Dr.<span id="profile-name">{{$doctor->name}}</span>
                        </li>
                        <li class="doctor-rating doctor-m-b-8">
                            <div class="rating">
                                4.5
                                <i class="fa fa-star star-right" aria-hidden="true"></i>
                            </div>
                            <span class="rating-rate">30 ratings</span>
                        </li>
                        <li class="doctor-occupation doctor-m-b-10">
                            <i class="fa fa-stethoscope" aria-hidden="true"></i>
                            <span class="doctor-occupation-name doctor-m-l-15">{{$doctor->occupation}}</span>
                        </li>
                        <li class="doctor-graduation doctor-m-b-10">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            <span class="doctor-m-l-15">{{$doctor->graduation}}</span>
                        </li>
                        <li class="doctor-fee doctor-m-b-10">
                            <i class="fa fa-money" aria-hidden="true"></i>
                            <span class="doctor-m-l-15">Rs. {{$doctor->fee}}Consultation fee</span>
                        </li>
                        <li class="doctor-location doctor-m-b-10"><i class="fa fa-location-arrow" aria-hidden="true"></i>
                            <span class="doctor-m-l-15">
                                <span class="text-bold">{{$doctor->location}}</span><br>
                                <p class="doctor-location-detail doctor-m-l-15">{{$doctor->location_details}}</p>
                            </span></li>
                    </ul>
                </div>

                <div class="col-md-12 doctor-button">
                    <button class="btn btn-primary doctor-capitalize"> book appointment</button>
                </div>
        </div>
            
        </div>

    </div>


@endsection