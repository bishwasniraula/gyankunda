<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class userinfo extends Model
{
    protected $fillable = ['uid',
                    'firstName',
                    'lastName',
                    'mobileNum',
                    'email',
                    'gender',
                    'DoB',
                    'notify_order_by'
                 ];

    protected $primaryKey = 'uid';
}
