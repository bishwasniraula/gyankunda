<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;

class deliveryinfo extends Model
{
    protected $fillable = ['uid',
                     		'shipping_full_name',
							'shipping_zipcode',
							'txtShipAddress',
							'shipping_suite_or_apt',
							'Shipping_mobno',
							'shipping_city',
							'ShipState'
                 ];

    protected $primaryKey = 'uid';
}
