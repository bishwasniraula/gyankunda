<?php

namespace App\Model\user;

use Illuminate\Database\Eloquent\Model;
use Auth;

class billinginfo extends Model
{
    
   protected $fillable = ['uid',
                     		'zipcode',
                     		'address_full',
                     		'billing_phoneno',
                     		'inpBCity',
                     		'Bstate',
                     		'BillCountry'
                 ];

    protected $primaryKey = 'uid';
}
