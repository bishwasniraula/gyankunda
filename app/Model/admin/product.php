<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    

    protected $fillable = [
        'title', 'price', 'selling_price','quantity','status','expiry_date','description','salt_composition','cover_photo','basic_use','side_effect','how_to_use' 
    ];



public function getShortDescriptionAttribute($value){

	return substr($this->description, 0, 50).'...';
}
public function getShortSaltCompositionAttribute($value){

	return substr($this->salt_composition, 0, 20).'...';
}
public function setDescriptionAttribute($value){

	$this->attributes['description'] = strip_tags($value);
}
public function setSaltCompositionAttribute($value){
$this->attributes['salt_composition'] = strip_tags($value);
}

public function setBasicUseAttribute($value){
	$this->attributes['basic_use'] = strip_tags($value);
}

public function setSideEffectAttribute($value){
	$this->attributes['side_effect'] = strip_tags($value);
}

public function setHowToUseAttribute($value){
	$this->attributes['how_to_use'] = strip_tags($value);
}



}
