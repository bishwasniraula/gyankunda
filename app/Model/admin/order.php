<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class order extends Model
{

	protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'cart',
        'payment_method',
        'total_amount',
        'total_discount',
        'total_amount_with_VAT',
        'created_at',
       
    ];



    


}
