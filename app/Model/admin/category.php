<?php

namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $fillable = ['category_name','id'
    					];

   public function setCategoryNameAttribute($value){

   	$this->attributes['category_name'] = ucwords($value);
   		
   }


}
