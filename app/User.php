<?php

namespace App;

use App\Notifications\verifyEmail;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','mobile','password','token','is_active','purpose',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function verified(){

        return $this->token === null;
    }

    public function sendVerificationEmail(){

       $this->notify(new verifyEmail($this)); 
    }
    

    public function setNameAttribute($value){

        $this->attributes['name'] = ucwords($value);
    }

    public function setPurposeAttribute($value){

        $this->attributes['purpose'] = ucwords($value);
    }

 



}
