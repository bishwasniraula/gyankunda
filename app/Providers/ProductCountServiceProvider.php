<?php

namespace App\Providers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Session;

class ProductCountServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view){
            /** \Illuminate\Contracts\View\View $view */


            if(Session::has('cart')){

            $count = count(session()->get('cart')->items);

            $view->with('cart_qty', $count);
            }
            else
              $count = 0;

            $view->with('cart_qty', $count);  
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
