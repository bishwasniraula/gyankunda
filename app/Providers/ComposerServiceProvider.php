<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        // View::composer(
        //     'frontend.layouts.navigationList', 'App\Http\ViewComposers\ProductMenuComposer'
        // );
    }

    /**
     * Register the application services.
     */
    public function register()
    {
    }
}
