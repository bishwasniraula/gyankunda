<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillinginfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billinginfos', function (Blueprint $table) {
        $table->integer('uid',10)->unsigned();
        $table->foreign('uid')->references('id')->on('users')->onDelete('cascade');
       $table ->string('zipcode')->nullable();
       $table ->string('address_full')->nullable();
       $table ->string('billing_phoneno')->nullable();
       $table ->string('inpBCity')->nullable();
       $table ->string('BCity')->nullable();
       $table ->string('BillCountry')->nullable();
       $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billinginfos');
    }
}
