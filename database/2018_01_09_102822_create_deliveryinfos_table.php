<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryinfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveryinfos', function (Blueprint $table){
            $table->integer('uid',10)->unsigned();
            $table->foreign('uid')->references('id')->on('users')->onDelete('cascade');
            $table->string('shipping_full_name')->nullable();
            $table->string('shipping_zipcode')->nullable();
            $table->string('txtShipAddress')->nullable();
            $table->string('shipping_suite_or_apt')->nullable();
            $table->string('Shipping_mobno')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('ShipState')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveryinfos');
    }
}
