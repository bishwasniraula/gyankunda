<?php


// login signup optionpage

Route::get('optionpage', 'RegisterOptionController@index')->name('optionpage');

Auth::routes();

Route::group(['namespace'=>'Frontend'], function(){

Route::get('/', 'MainController@index');

Route::get('/hospital', 'PagesController@hospital')->name('hospital');


Route::get('/labtest', 'PagesController@labtest')->name('labtest');

Route::get('/labtest/labtest-search', 'PagesController@labtestSearch')->name('labtest-search');

Route::get('/labtest/packagecard', 'PagesController@packagecard')->name('packagecard');

Route::get('/doctor', 'PagesController@doctor')->name('doctor');

Route::get('/doctor/doctors-feild', 'PagesController@doctorsFeild')->name('doctors-feild');

Route::get('/doctor/doctor-profile', 'PagesController@doctorProfile')->name('doctor-profile');

Route::get('swasthya/article', 'ArticleController@index')->name('article');

Route::get('swasthya/forum', 'ForumController@index')->name('forum');
Route::get('swasthya/forum/question-answer', 'ForumController@forumQAnswer')->name('forum-answer');

});

// view product by category
Route::group(['namespace'=>'Frontend'], function(){

    Route::get('/medicine-categories', 'PagesController@category')->name('medicine-categories');

    Route::resource('/medicine-category-products', 'MedicineController',['only' => ['index', 'show']]);

    Route::resource('/medicine/product', 'ProductController', ['only' => ['index', 'show']]);
    Route::get('/medicine/product', 'MainController@show')->name('medicine/name');

});







// cart routing

Route::group(['middleware' => 'auth'], function() {

Route::get('/checkout',[
    'uses'  => 'Frontend\ProductController@getCheckout',
    'as'    => 'checkout'
]);


Route::post('/checkout',[
    'uses'  => 'Frontend\ProductController@postCheckout',
    'as'    => 'checkout'
]);

Route::get('/order-summary',[
    'uses'  => 'Frontend\ProductController@orderSummary',
    'as'    => 'summary'
]);

Route::get('/payment',[
    'uses'  => 'Frontend\ProductController@proceedPayment',
    'as'    => 'payment'
]);

});


Route::group(['namespace' => 'Frontend'], function() {

Route::get('/add-to-cart/{id}',[
    'uses'  => 'ProductController@getAddToCart',
    'as'    => 'product.addToCart'
]);

Route::post('/single-add',[
    'uses'  => 'ProductController@getAddQTY',
    'as'    => 'product.getAddQTY'
]);

Route::get('/cart-count',[
        'uses'  => 'ProductController@cartCount',
        'as'    => 'product.countCart'
    ]);

Route::get('/shopping-cart',[
    'uses'  => 'ProductController@getCart',
    'as'    => 'product.shoppingcart'
]);

Route::get('/afterremove-cart',[
     'uses'  => 'ProductController@updateCart',
        'as'    => 'product.updateCart'
    ]);

Route::get('/remove/{id}',[
    'uses'  => 'ProductController@getRemoveItem',
    'as'    => 'product.remove'
]);

Route::get('/add/{id}',[
    'uses'  => 'ProductController@getAddByOne',
    'as'    => 'product.addByOne'
]);




Route::get('/reduce/{id}',[
    'uses'  => 'ProductController@getReduceByOne',
    'as'    => 'product.reduceByOne'
]);

Route::get('/gyankunda',[
    'uses'  => 'ProductController@index',
    'as'    => 'product.index'
]);




Route::get('/increase','ProductController@increaseItem');
});


// user verification

Route::get('/user-varification-notification', 'VerifyController@notification');

Route::get('/verify/{token}', 'VerifyController@verify')->name('verify');

// userprofile

Route::group(['middleware' => 'auth'], function() {

Route::get('userprofile/',[
    'uses'  => 'Frontend\UserProfileController@index',
    'as'    => 'userprofile'
]);

Route::post('/submit','Frontend\UserProfileController@uploadUserPrescription');

Route::post('/update-password','Frontend\UserProfileController@UpdateuserPassword');

Route::get('/useraccount',[
    'uses'  => 'Frontend\UserProfileController@useraccount',
    'as'    => 'useraccount'
]);

Route::get('/orderlist',[
    'uses'  => 'Frontend\UserProfileController@userOrderlist',
    'as'    => 'orderlist'
]);

Route::get('/uplpress',[
    'uses'  => 'Frontend\UserProfileController@uploadPrescription',
    'as'    => 'uploadprescription'
]);
Route::get('/accinfo',[
    'uses'  => 'Frontend\UserProfileController@accountInfo',
    'as'    => 'accinfo'
]);
Route::post('/add-billing-info',[
    'uses'  => 'Frontend\UserProfileController@billingInfo',
    'as'    => 'billingInfo'
]);
Route::post('/add-delevery-info',[
    'uses'  => 'Frontend\UserProfileController@deliveryInfo',
    'as'    => 'deliveryInfo'
]);
Route::post('/add-personal-info',[
    'uses'  => 'Frontend\UserProfileController@personalInfo',
    'as'    => 'personalInfo'
]);
});



// admin routes
    Route::group(['namespace' =>'Admin'],function(){

    Route::get('admin/dashboard', 'HomeController@index')->name('dashboard');

    Route::resource('admin/product', 'ProductController');

    Route::resource('admin/order', 'OrderController',['only'=>['index','show','destroy']]);

    Route::get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin-login', 'Auth\LoginController@login');
    
    Route::get('/admin/change-password', 'Auth\UpdatePasswordController@index')->name('admin/changepasword');

    Route::post('admin/yes-password', 'Auth\UpdatePasswordController@updatePassword')->name('admin.password');
    
    Route::get('/admin/user-status', 'UserStatusController@index')->name('userstatus');

    // Admin ajax call
    Route::get('delete-product','ProductController@deleteProduct');

    Route::get('/delete-category','CategoryController@deleteCategory');

    Route::resource('admin/add-product-category','CategoryController');


});
  










